"""              Extracter.py
            written on 7. May 2019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: Extracts a 2x2 image from a zero field
    INPUT:  u, an array with an imbedded image
         l, left pixel adjustment
         r, right pixel adjustment
         up, upper pixel adjustment
         dn, lower pixel adjustment
   OUTPUT: u, the extracted image
   USAGE:  U=Extracter(u,l,r,up,dn)
 """
 
 
from numpy import ndenumerate, array, abs as npabs, append, maximum, minimum, full, shape 

#from scipy.sparse.csgraph import laplacian   # ????????????????????????
from proxtoolbox.utils.Laplacian import Laplacian



def Extracter(u,l,r,up,dn):
    
    tmp = Laplacian(u,[1,1]) # ???????????????????????
    shape_u = shape(u)
    #print(min(npabs(tmp).flatten()))
    tmp = (maximum(full(shape_u, 1e10-1e9) , minimum( full(shape_u,1e10), npabs(tmp)))-(1e10-1e9))/1e9
    #print(tmp)
    tmpx = npabs(tmp[63,1:128]-tmp[63,0:127])
    west = min(find(tmpx, 0))+l
    east = max(find(tmpx, 0))+r
    tmpy1 = abs(tmp[1:128,64-2]-tmp[0:127,64-2])
    north = min(find(tmpy1, 0))+up
    south = max(find(tmpy1, 0))+dn
    u=u[int(north): int(south), int(west): int(east)]
    
    return u
    
    
def find(Array, ele):
    L = []
    Array = Array.flatten()
    #print(Array)
    for idx, val in ndenumerate(Array):
        if val == ele:
            L.append(idx)
    
    return array(L)



if __name__ == "__main__":
    from numpy import arange
    
    A = 2 * arange(150**2).reshape(150,150)
    
    print(Extracter(A, 0.5, 0.1, 1, -2))
    
    

