#######################################################################
#                                                                    
#                   load_image_to_float_array.py
#                            written by
#                           Russell Luke
#                   Inst. fuer Num. u. Angew. Math
#                       Universitaet Goettingen
#                          Oct. 1, 2024
#
# DESCRIPTION: title says it all, except that it converts to greyscale image arrays
#
# INPUT:
#              image_path
#
# OUTPUT:     flat_array, img_array.shape
#
# USAGE:       float_array, original_shape = load_image_to_float_array(image_path)
# Non-standard function calls: 
#
########################################################################
import numpy as np
from PIL import Image

def load_image_to_float_array(image_path):
    # Open the image using PIL
    with Image.open(image_path) as img:
        # Convert the image to grayscale
        img_gray = img.convert('L')
        
        # Convert the image to a numpy array
        img_array = np.array(img_gray)

        # Normalize the values to float between 0 and 1
        img_float = img_array.astype(np.float32) / 255.0
        
        # Flatten the array
        flat_array = img_float.flatten()
    return flat_array, img_array.shape
