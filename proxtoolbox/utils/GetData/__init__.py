"""
The "utils.GetData"-module contains functions to download the InputData for the example problems
"""

from .GetData import *


__all__ = ["dlProgress", "query_yes_no", "getData"]
