
"""              OnesPad.py
            written on September 2. 1019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: Function for padding an array with ones.
         Automatically resizes the array to the next
         largest diad  by adding zeros symmetrically to the array and again
         symmetrically doubles the size of the array.  Takes one or two
         dimensional arrays.

    Input: m = numpy data array
           

    Output: padded_m = the ones padded numpy array
    Usage:  padded_m = OnesPad(m) """
    
    
from math import log2, ceil
import numpy as np
    
def OnesPad(arr):
    if len(np.shape(arr)) == 2:
        #creat ones array in right shape
        [m, n] = np.shape(arr)
        max_dim = max([m,n])
        pad_shape = 2**(int(log2(max_dim) + 1))
        padded = np.ones((pad_shape, pad_shape))
        #print(pad_shape)
        
        
        #place the input array at the right place into padded
        free_top_bottum = pad_shape - m
        free_left_right = pad_shape - n
        
        free_top = ceil(free_top_bottum / 2)
        free_bottum = int(free_top_bottum / 2)
        
        free_left = ceil(free_left_right / 2)
        free_right = int(free_left_right / 2)
        
        padded[free_top:free_bottum+max_dim, free_left:free_right+max_dim] = arr
        
        
        return padded

    else:
        m = np.shape(arr)[0]
        pad_shape = 2**(int(log2(m) + 1))
        padded = np.ones(pad_shape)
        print(padded)
        padded[pad_shape- m:] = arr
        return padded
    
    
    
    
    
    
    
    
    
if __name__ == "__main__":
    A = np.arange(12).reshape(3, 4)
    print(OnesPad(A))
    A = np.array([1,2, 3])
    #print(A)
    print(OnesPad(A))