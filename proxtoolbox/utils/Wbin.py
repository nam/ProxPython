"""              Wbin.py
            written on 7 Sep by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: Saves data in .npz and nd creates a companion .info file
                  with information about the size and class of the data

    Input:  data: array of numpy matrices  OR a dictionary {"name of array": numpy_array, "second_array": sec_array, .......}
            filename: path to file in which the data should be saved
            info: ????????????????????????????????

    Output: filename.npz : a zip file containing the data
    Usage: Wbin(data, filename, info) """
    
    
from numpy import savez
    
def Wbin(data, filename, info):
    
    if isinstance(data, list):
        savez(filename, *data)
        
        f = open(filename + ".info", "w")
        f.write(info)
        f.close()
        
    if isinstance(data, dict):
        savez(filename, **data)
        f.write(info)
        f.close()
        
            