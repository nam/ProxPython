"""              Reorder.py
            written on 10. May 2019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen
            
             DESCRIPTION:  Reorders a 2x2 image
             INPUT: A, an image to be reordered
             OUTPUT: B, the reordered image
             USAGE: B=Reorder(A)

    
    
 """
 
from numpy import shape, zeros, arange, concatenate, size

def Reorder(B):
    
    # Reorders a matrix
    
    n1 = shape(B)[-2] -1
    n2 = shape(B)[-1] 
    
    
    # if it is a nxmxk matix
    try:
        #A = zeros((n1+1, n2))
        A = zeros((size(B[:, 0]), n2))
        
        #print(B[1,:])
        for j in range(n1+1):
            #A[n1-j, :] = B[j, :]
            A[:, n1-j] = concatenate(B[:, j])
        return A.T
    
    #if it is a n x m matrix
    except:
        A = zeros((n1+1, n2))
        #A = zeros((size(B[:, 0]), n2))
        
        #print(B[1,:])
        for j in range(n1+1):
            A[n1-j, :] = B[j, :]
            #A[:, n1-j] = concatenate(B[:, j])
        return A
        

        






if __name__ == "__main__":
    # does the Reorder.m file miss a "end" statemant at the end of the function ??
    A = arange(1,19).reshape(2,3,3)
    print(Reorder(A))
    
    A = arange(1,10).reshape(3,3)
    print(Reorder(A))
        