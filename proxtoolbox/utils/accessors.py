
from proxtoolbox.utils.cell import Cell, isCell
import numpy as np

def accessors(u, Nx, Ny, product_space_dimension):
    '''
    This utility function gives accessors (get and set functions)
    based on the nature of the iterate `u` and the variables
    `Nx`, `Ny`, and `product_space_dimension`
    Parameters
    ----------
    u : array_like
    Nx: int
    Ny: int
    product_space_dimension: int

    Returns
    -------
    get, set, data_shape:
        `get` and `set` are functions that allow access
        to the data indexed by the product space dimension
        `data_shape` corresponds to the dimensions of the data, not
        including the product space dimension if it exists.
    '''
    # Ny corresponds to first dimension
    # Nx to the second dimension when both are defined
    # Since this is a bit counter-intuitive we define the
    # following variables instead:
    n1 = Ny
    n2 = Nx
    if isCell(u):
        # assumes 2D
        get = lambda u, j: u[j]
        set = set0
        data_shape = u[0].shape
    else:
        if n2 == 1:
            # u is n1 x L array
            get = lambda u, j: u[:,j]
            set = set1
            data_shape = (n1)
        elif n1 == 1:
            # u is L x n2 array
            get = lambda u, j: u[j,:]
            set = set2
            data_shape = (n2)
        else:
            # assumes 2D case
            get = lambda u, j: u[:,:,j]
            set = set3
            data_shape = (n1, n2)
    return get, set, data_shape
     

# the following functions are helper functions used
# by the evaluate() method. They should have been lambda 
# functions, but Python does not support these kind of
# functions that perform an assignment using subscripts
def set0(dest, j, val):
    dest[j] = val
def set1(dest, j, val):
    dest[:,j] = val
def set2(dest, j, val):
    dest[j,:] = val
def set3(dest, j, val):
    dest[:,:,j] = val

