
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.proxoperators.ADM_prox import ADM_Context
from proxtoolbox.utils.functions import *
import numpy as np



class ADM_couplings(ADM_Context):
    """
    Generic interface for prox operators
    """

    def __init__(self, experiment):
        """
        Initialization method for a concrete instance
        
        Parameters
        ----------
        experiment : instance of Experiment class
            Experiment object that will use this prox operaror
        """
        pass  # base class does nothing

    def eval(self, u):
        """
        Applies a prox operator to some input data
        
        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        ndarray or a list of ndarray objects
            Result of the application of the prox operator onto
            the input data
        """
        raise NotImplementedError("This is just an abstract interface")

class Square_Quadratic(ADM_couplings):
    """
    the coupling function for ADM: Q(x,y) = 0.5 * || u[1] - Quadratic(u[0])||^2 
    where u[1] are the auxilliary variables in the image of quadratics
    u[0] are the primal/domain variables, and 
    Quadratic() is a vector of dim(u[1]).  This function is in utils.functions
    """
    def __init__(self, experiment):
        super(ADM_couplings, self).__init__(experiment)
        """
        Initialization method for a concrete instance
        
        Parameters
        ----------
        experiment : instance of Experiment class
            Experiment object that will use this prox operaror
        """
        self.A_tens = experiment.A_tens
        self.b_mat = experiment.b_mat
        self.c_vec = experiment.c_vec

    def eval(self,u):
        Qx = Quadratic(self.A_tens, self.b_mat, self.c_vec, self.u0[0])
        val = 0.5 * sum(map(self.u[1]-Qx)**2)
        return val

