
from proxtoolbox.algorithms.algorithm import Algorithm
from proxtoolbox import proxoperators
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.accessors import accessors
import numpy as np
import copy


from numpy import exp

class AvP2(Algorithm):
    """
    Alternating prox with modification as described 
    in Luke,Sabach&Teboulle "Optimization on Spheres: 
    Models and Proximal Algorithms 
    with Computational Performance Comparisons" 
    D. R. Luke, S. Sabach and M. Teboulle.  2019.
    This is an averaged projections algorithm with a 
    2-step recursion (x^{k-1}, x^{k}, u^{k})

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on September 08, 2016.    
    """
    
    def __init__(self, experiment, iterateMonitor, accelerator):
        super(AvP2, self).__init__(experiment, iterateMonitor, accelerator)
        
        if hasattr(experiment, 'shift_data'):
            self.shift_data = experiment.shift_data
        else:
            L = self.product_space_dimension
            self.shift_data = Cell(L)
            for j in range(L):
                self.shift_data[j] = 0

    def evaluate(self, u):
        """
        Update of the AvP2 algorithm
       
        Parameters
        ----------
        u : ndarray or a cell of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a cell of ndarray objects
            The new iterate (same type as input parameter `u`).

        """
        lmbda = self.computeRelaxation()
        
        u_new = Cell(len(u))
        L = self.product_space_dimension
        get, set, _data_shape = accessors(u[2], self.Nx, self.Ny, L)
        if isCell(u[2]):
            u2_tmp = Cell(len(u[2]))
        else:
            u2_tmp = np.empty(u[2].shape, dtype=u[2].dtype)
        diff_tmp = u[1]-u[0]
        for j in range(L):
            set(u2_tmp, j, get(u[2], j) + self.shift_data[j] + (diff_tmp)*lmbda)
        u_new2 = self.prox1.eval(u2_tmp)
        u_new[1] = get(u_new2, 0)
        diff_tmp = 2*u[1]-u[0]
        for j in range(L):
            set(u2_tmp, j, get(u[2], j) + (diff_tmp - self.shift_data[j])*lmbda)
        u_new[2] = self.prox2.eval(u2_tmp)
        u_new[0] = copy.deepcopy(u[1])  # deep copy is needed since u[1] may be a cell

        return u_new

