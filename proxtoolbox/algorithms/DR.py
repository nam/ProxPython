from proxtoolbox.algorithms.algorithm import Algorithm

import numpy as np

class DR(Algorithm):
    """
    Douglas-Rachford algorithm
    """

    def evaluate(self, u):
        """
        Update for Douglas-Rachford algorithm. 

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The new iterate (same type as input parameter `u`).
        """

        tmp1 = 2*self.proxOperators[0].eval(u) - u
        tmp2 = self.proxOperators[1].eval(tmp1) - tmp1/2
        unew = u/2 + tmp2
        return unew

    def postprocess(self):
        self.u = self.proxOperators[0].eval(self.u)
        output = {'stats': {'iter': self.iter}, 'u': self.u}
        self.iterateMonitor.postprocess(self, output)
        return output
