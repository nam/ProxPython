
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.proxoperators.ADM_prox import ADM_Context
from proxtoolbox.algorithms.ADM_objective import *
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.functions import *
import numpy as np


class ADM_objective(ADM_Context):
    """
    Augmented objective of the ADM algorithm for minimizing the sum of two 
    objectives with abstract coupling between the primal variables and the auxilliary/image
    variables.  Specialized in  subclasses to particular instances. 
    """
    """
    Generic interface for prox operators
    """

    def __init__(self, experiment):
        super(ADM_objective, self).__init__(experiment)
        """
        Initialization method for a concrete instance
        
        Parameters
        ----------
        experiment : instance of Experiment class
            Experiment object that will use this prox operaror
        """
        if hasattr(experiment, 'A_tens'):
            self.A_tens = experiment.A_tens
        else:
            self.A_tens = 0
        if hasattr(experiment, 'b_mat'):
            self.b_mat = experiment.b_mat
        else:
            self.b_mat = 1
        if hasattr(experiment, 'c_vec'):
            self.c_vec = experiment.c_vec
        else:
            self.c_vec = 0
        if hasattr(experiment, 'augmentation_scaling'):
            self.augmentation_scaling = experiment.augmentation_scaling
        else:
            self.augmentation_scaling = 1.0
        if hasattr(experiment, 'primal_scaling'):
            self.primal_scaling = experiment.primal_scaling
        else:
            self.primal_scaling = 1.0
        if hasattr(experiment, 'image_scaling'):
            self.image_scaling = experiment.image_scaling
        else:
            self.image_scaling = 1.0



    def calculateObjective(self, alg):
        """
        Evaluate ADM objective function
            
        Parameters
        ----------
        alg : algorithm instance
            The algorithm that is running
         
        Returns
        -------
            The value of the objective function 
            which will be reported by the `gap' 
        """

        # adaptation of the ADMM_phase_indicator_objective.py 
        objective_with_penalty_and_coupling = 0
        critical_point_discrepancy = 0
        u = alg.u_new

        if (self.domain_objective!='none'):
            objective_with_penalty_and_coupling += self.primal_scaling*eval(self.domain_objective)(u[0])
        objective_with_penalty_and_coupling += self.image_scaling*eval(self.image_objective)(u[1])
        objective_with_penalty_and_coupling += self.augmentation_scaling*eval(self.coupling_function)(self, u) / 2
        critical_point_discrepancy += self.augmentation_scaling*eval('Primal_Dual_alignment')(self, u) 

        return objective_with_penalty_and_coupling, critical_point_discrepancy

def Square_Quadratic(self, u):
        """
        Evaluate ADM coupling function with primal variables mapped by a quadratic
            
         
        Returns
        -------
            The value of the objective function 
            which will be reported by the `gap' 
        """
        Qx = Quadratic(self.A_tens, self.b_mat, self.c_vec, u[0])
        coupling_violation = sum((u[1]-Qx)**2)
        return coupling_violation
def Primal_Dual_alignment(self, u):
        """
        Check whether the regularized solution coincides with a solution to the original problem 
            
         
        Returns
        -------
            The value of || q(u[0])- rho^{-1}sign(q(u[0]))||
            which will be reported by the `gap' 
        """
        Qx = Quadratic(self.A_tens, self.b_mat, self.c_vec, u[0])
        tmp=np.zeros(Qx.shape)
        for j in range(self.Ny):
            if np.abs(Qx[j])>1e-15:
                tmp[j] = u[1][j] - (Qx[j] - np.sign(Qx[j])*self.augmentation_scaling)
            else:
                tmp[j]=0
        gap=np.sqrt(sum(tmp**2))
        return gap
