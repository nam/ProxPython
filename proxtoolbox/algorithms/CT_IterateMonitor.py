
from proxtoolbox.algorithms.iterateMonitor import IterateMonitor
from proxtoolbox.algorithms.feasibilityIterateMonitor import FeasibilityIterateMonitor
from proxtoolbox.utils.cell import Cell, isCell
import numpy as np


class CT_IterateMonitor(IterateMonitor):
#class CT_IterateMonitor(FeasibilityIterateMonitor):
    """
    Algorithm analyzer for monitoring iterates of 
    projection algorithms for CT (ART) experiments 
    Specialization of the IterateMonitor class.
    
    It would be more appropriate to derive from
    FeasibilityIterateMonitor, but this would 
    result in too many calculations

    Was used for plotting. Now does nothing, but this
    could change...
    """

    def __init__(self, experiment):
        super(CT_IterateMonitor, self).__init__(experiment)
        pass

        

