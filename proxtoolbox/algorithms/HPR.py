
from proxtoolbox.algorithms.algorithm import Algorithm
from proxtoolbox.utils.cell import Cell, isCell
from numpy import exp

class HPR(Algorithm):
    """
    User-friendly version of the Heinz-Patrick-Russell algorithm.  
    See Bauschke,Combettes&Luke, Journal of the Optical 
    Society of America A, 20(6):1025-1034 (2003).
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on May 23, 2002, revised Jan.17, 2005.    
    """

    def evaluate(self, u):
        """
        Update for the Heinz-Patrick-Russell algorithm

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The new iterate (same type as input parameter `u`).
        """
       
        lmbd = self.computeRelaxation()        
        if not isCell(u):
            tmp = self.prox2.eval(u)
            tmp2 = (2+lmbd-1)*tmp - u
            u_new = (2*self.prox1.eval(tmp2) - tmp2 + u + (1-lmbd)*tmp)/2
        else:
            tmp = self.prox2.eval(u)
            tmp2 = Cell(len(tmp))
            u_new = Cell(len(tmp))
            for j in range(len(tmp)):
                tmp2[j] = (2+lmbd-1)*tmp[j]- u[j]
            tmp3 = self.prox1.eval(tmp2)
            for j in range(len(tmp)):
                u_new[j] = (2*tmp3[j]-tmp2[j] + u[j] +(1-lmbd)*tmp[j])/2
        return u_new

    def getDescription(self):
        return self.getDescriptionHelper("\\lambda", self.lambda_0, self.lambda_max)