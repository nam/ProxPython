
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.proxoperators.ADMM_prox import ADMM_Context
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.accessors import accessors

import numpy as np
from numpy import pi, zeros, conj
from numpy.fft import fft2, ifft2, fft, ifft
import copy


class ADMM_PhaseLagrangeUpdate(ADMM_Context):
    """
    Explicit step with respect to the Lagrange multipliers 
    in the ADMM algorithm. 
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Oct 1, 2017.    
    """

    def __init__(self, experiment):
        super(ADMM_PhaseLagrangeUpdate, self).__init__(experiment)


    def eval(self, u):
        """
        Explicit step with respect to the Lagrange multipliers 
        in the ADMM algorithm
            
        Parameters
        ----------
        u : cell
            Input data 
         
        Returns
        -------
        xprime : cell
            The resulting step
        """
        v = copy.deepcopy(u[2]) # we need a deep copy here
        m1, n1, p1, _q1 = size_matlab(u[0])
        L = self.product_space_dimension
        get_1, set_1, _data_shape_1 = accessors(u[1], self.Nx, self.Ny, L)

        if m1 > 1 and n1 > 1 and p1 == 1:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)
            for j in range(L):
                if self.farfield:
                    if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                        u_hat = -1j*self.fresnel_nr[j]/(self.Nx*self.Ny*2*pi)*FFT(u[0]-self.illumination[j]) + self.FT_conv_kernel[j]
                    elif self.FT_conv_kernel is not None:
                        u_hat = FFT(self.FT_conv_kernel[j]*u[0]) / (m1*n1)
                    else:
                        u_hat = FFT(u[0]) / (m1*n1)
                else: # near field
                    if self.beam is not None:
                        u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u*self.beam[j]))/self.magn[j]
                    else:
                        u_hat = IFFT(self.FT_conv_kernel[j]*FFT(u))/self.magn[j]
                tmp = u_hat - u[1][j]
                v[j] += tmp
        elif m1 == 1 or n1 == 1:
            if m1 == 1:
                divisor = n1
            else:
                divisor = m1
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)
            for j in range(L):
                if self.farfield:
                    if self.fresnel_nr is not None and self.fresnel_nr[j] > 0:
                        raise NotImplementedError('Error ADMM_PhaseLagrangeUpdate: complicated far field set-ups for 1D signals not implemented')
                    elif self.FT_conv_kernel is not None:
                        u_hat = FFT(self.FT_conv_kernel[j]*u[0]) / divisor
                    else:
                        u_hat = FFT(u[0]) / divisor
                else:
                    raise NotImplementedError('Error ADMM_PhaseLagrangeUpdate: near field set-ups for 1D signals not implemented')
                tmp = u_hat - get_1(u[1], j)
                v_sum_j = get_1(v, j) + tmp
                set_1(v, j, v_sum_j)

        return v

