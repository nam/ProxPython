
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
import numpy as np
from numpy.linalg import norm


class PHeBIE_phase_objective:
    """
    objective function for the PHeBIE algorithm applied to the 
    phase retrieval problem
       
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Feb. 12, 2019.    
    """

    def __init__(self, experiment):
        if hasattr(experiment, 'norm_data'):
            self.norm_data = experiment.norm_data
        else:
            self.norm_data = 1.0


    def calculateObjective(self, alg):
        """
        Evaluate PHeBIE objective function
            
        Parameters
        ----------
        alg : algorithm instance
            The algorithm that is running
         
        Returns
        -------
        objValue : real
            The value of the objective function 
        """
        objValue = 0
        u = alg.u_new
        normM = self.norm_data
        m1, n1, p1, _q1 = size_matlab(u[0])
        if isCell(u[1]):
            for jj in range(len(u[1])):
                objValue += (norm(u[0] - u[1][jj])/normM)**2
        else:
            m2, n2, p2, q2 = size_matlab(u[1])
            if m1 == 1:
                objValue = (norm(np.ones(m2,1) @ u[0] - u[1]))**2
            elif n1 == 1:
                objValue = (norm(u[0] @ np.ones(1,n2)*u[0] - u[1]))**2
            elif p1 == 1:
                for jj in range(p2):
                        objValue += (norm(u[0] - u[1][:,:,jj]))**2
            else: # cells of 4D arrays?!!!
                for jj in range(p1):
                    for k in range(q2):
                        objValue += (norm(u[0][:,:,k] - u[1][:,:,k,jj]))**2

        return objValue



