
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.algorithms.iterateMonitor import IterateMonitor
from proxtoolbox.utils.cell import Cell, isCell
import numpy as np
from numpy import zeros, angle, trace, exp, sqrt, sum, matmul, array, reshape
from numpy.linalg import norm

class ADM_IterateMonitor(IterateMonitor):
    """
    Algorithm analyzer for monitoring iterates of 
    projection algorithms for the ADMM algorithm. 
    Specialization of the IterateMonitor class.
    """

    def __init__(self, experiment):
        super(ADM_IterateMonitor, self).__init__(experiment)
        self.gaps = None
        self.shadows = None
        self.rel_errors = None
        self.product_space_dimension = experiment.product_space_dimension
        if hasattr(experiment, 'norm_data'):
            if experiment.Nx is None:
                self.normM = np.sqrt(experiment.domain_dim)
            else:
                self.normM = np.sqrt(experiment.Nx*experiment.Ny)
        else:
            # won't normalize data to make it independent of array size
            self.normM = 1.0


    def preprocess(self, alg):
        """
        Allocate data structures needed to collect 
        statistics. Called before running the algorithm.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm to be monitored.
        """
        super(ADM_IterateMonitor, self).preprocess(alg)

        # In ADM, the iterate u is a cell of blocks of variables.
        # In the analysis of Bolte Sabach Teboulle and Vaisbourd 2018, 
        # only the primal/domain sequence is monitored.  This is done in the 
        # shadows vector.  The gap monitors the *value* of the ADM objective function:
        # gap = F(x) + Phi(y) + Q(x,y)
        # The changes vector monitors the overall change in the combined blocks of variables

        self.u_monitor = self.u0.copy()

        # set up diagnostic arrays
        if self.diagnostic:
            self.gaps = self.changes.copy()
            self.gaps[0] = 999
            self.shadows = self.changes.copy()
            self.shadows[0] = sqrt(999)
            if self.truth is not None:
                self.rel_errors = self.changes.copy()
                self.rel_errors[0] = sqrt(999)
           
    def updateStatistics(self, alg):
        """
        Update statistics. Called at each iteration
        while the algorithm is running.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm being monitored.
        """

        # 'u' is a cell of  2 blocks of variables, primal and  auxilliary/image  
        # each of these containing ARRAYS.  For ADM, convergence of the
        # algorithm is determined only by the behavior of the 1st
        # block of variables (see Bolte, Sabach, Teboulle and Vaisbourd (2018).  
        # The first block of primal variables is monitored as a 
        # "shadow sequence" in analogy with the shadow sequence of the 
        # Douglas-Rachford iteration. 

        u = alg.u_new
        prev_u = self.u_monitor
        tmp_change = 0
        normM = self.normM
        for j in range(1,2):
            # the first cell is the block (assumed single) of 
            # primal variables - assumed an array.  
            # All other cells are the dual blocks, stored as
            # cells or arrays
            if isCell(u[j]):
                k = len(u[j])
                _m, _n, p, q = size_matlab(u[j])
            else:
                m, n, p, q = size_matlab(u[j])
                k = self.n_product_Prox
            
            for kk in range(k):
                if p == 1 and q == 1:
                    if isCell(u[j]):
                        # the next line was added on the hunch that a global phase
                        # rotation each iteration is making the algorithm look like
                        # it is converging more slowly than it really is
                        tmp_change += (norm(u[j][kk] - prev_u[j][kk])/normM)**2
                    else: # dealing with 1D arrays on the product space
                        if n == k:
                            tmp_change += (norm(u[j][:,kk] - prev_u[j][:,kk])/normM)**2
                        elif m == k:
                            tmp_change += (norm(u[j][kk,:] - prev_u[j][kk,:])/normM)**2
                elif q == 1:
                    if isCell(u[j]): # this means cells of cells of 3D arrays...I hope not!
                        for jj in range(p):
                            # the next line was added on the hunch that a global phase
                            # rotation each iteration is making the algorithm look like
                            # it is converging more slowly than it really is
                            tmp_change += (norm(u[j][kk][:,:,jj] - prev_u[j][kk][:,:,jj])/normM)**2
                    else:
                        # we have a 3D array, the third dimension being the
                        # product space
                        tmp_change += (norm(u[j][:,:,kk] - prev_u[j][:,:,kk])/normM)**2
                else: # cells of 4D arrays?!!!
                    for jj in range(q):
                        for pp in range(p):
                            tmp_change += (norm(u[kk][:,:,pp, jj] - prev_u[kk][:,:,pp, jj])/normM)**2
                
        self.changes[alg.iter] = sqrt(tmp_change)

        if self.diagnostic:
            tmp_shadow = 0
            rel_error = 0
            # looks for the gap and the shadow.  We use gap to hold the function values of the
            # ADM objective.  shadow holds the distance of the iterate to a critical 
            # point of the unaugmented problem.  
            # This is problem/implementation specific and will 
            # be found in the experiment and proxoperators subdirectories
            # where the problem families are defined.
            self.gaps[alg.iter], self.shadows[alg.iter] = self.calculateObjective(alg)


            if self.truth is not None:
                self.rel_errors[alg.iter] = rel_error

        self.u_monitor = u.copy()


    def postprocess(self, alg, output):
        """
        Called after the algorithm stops. Store statistics in
        the given 'output' dictionary

        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that was monitored.
        output : dictionary
            Contains the last iterate and various statistics that
            were collected while the algorithm was running.
                
        Returns
        -------
        output : dictionary into which the following entries are added
            (when diagnostics are required)
        gaps : ndarray
            Squared gap distance normalized by the magnitude
            constraint
        shadows : ndarray
            ??? TODO
        """
        output = super(ADM_IterateMonitor, self).postprocess(alg, output)
        if self.diagnostic:
            stats = output['stats']
            stats['gaps'] = self.gaps[1:alg.iter+1]
            stats['shadows'] = self.shadows[1:alg.iter+1]
            if self.truth is not None:
                stats['rel_errors'] = self.rel_errors[1:alg.iter+1]
        return output