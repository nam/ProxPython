import numpy as np
from proxtoolbox.algorithms.algorithm import Algorithm

class BCD(Algorithm):
    """
    Block coordinate descent methods for solving problems that
    can be devided into disjoint blocks, that is
        min     f(x)
        s.t.    x in X = X1 x X2 x ... x Xn
    So the BCD minimizes blockwise:
        min     f(x)
        s.t.    x in Xk
    for each block k=1,...,n.

    See e.g. D. Bertsekas in "Convex Optimization Algorithms", pp. 369.

    If another algorithm should be used to minimize over one block,
    this "inner algorithm" can be defined in the experiment class
    and the prox operator "algo_in_prox" can be used to call this
    algorithm in each block iteration of the BCD. 
    """

    def __init__(self, experiment, iterateMonitor, accelerator=None):
        super().__init__(experiment, iterateMonitor, accelerator)
        
        self.randomized = experiment.randomizedAlgo
        self.numBlocks = experiment.numBlocks
        self.blockMasks = experiment.blockMasks
        if hasattr(experiment,'inner_algorithm'): # experiment has this attribute, iff prox1 == 'algo_in_prox'
            self.extract = False
        else:
            self.extract = True

    def evaluate(self, u):
        """
        Update for the block descending algorithm.
        This is cycling through all n blocks.
        One cycle can be performed by choosing the blocks
        sequentially or randomly, depending on the parameter
        experiment.randomizedAlgo.

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The new iterate (same type as input parameter `u`).
        """

        if self.randomized:
            # choose blocks randomly, i.e. some blocks will be evaluated multiple times
            unusedBlocks = list(range(self.numBlocks))
            while len(unusedBlocks) != 0:
                blockNo = np.random.randint(self.numBlocks)
                if blockNo in unusedBlocks:
                    unusedBlocks.remove(blockNo)
                    
                if self.extract:
                    u[self.blockMasks[blockNo]] = self.prox1.eval(u[self.blockMasks[blockNo]])
                else:
                    # iff prox1 == 'algo_in_prox'
                    u = self.prox1.eval(u, blockNo, self.blockMasks)

        else:
            # choose blocks sequentially, i.e. each block is evaluated once
            for blockNo in range(self.numBlocks):
                if self.extract:
                    u[self.blockMasks[blockNo]] = self.prox1.eval(u[self.blockMasks[blockNo]])
                else:
                    # iff prox1 == 'algo_in_prox'
                    u = self.prox1.eval(u, blockNo, self.blockMasks)

        return u
    
  