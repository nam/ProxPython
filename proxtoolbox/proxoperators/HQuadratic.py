from proxtoolbox.proxoperators.proxoperator import ProxOperator
from numpy import transpose

class HQuadratic(ProxOperator):
    '''
    DESCRIPTION: computes the Hessian of a vector of quadratics. 
                 Of course, not a prox mapping, but kind of sort of 
                 belongs to this class 
    INPUT:  x            := the variable          
            A_tens       := a tensor (3D array) dimension nxnxm
            b_mat        := a matrix (2D array) mxn
    OUTPUT: HQ_vec a matrix of Hessians of the m quadratics
    ''' 
    def __init__(self, experiment):
        self.A_tens = experiment.A_tens

    def eval(self, X, prox_idx=None):
        HQ_vec = self.A_tens + transpose(self.A_tens,(0,2,1))
        return HQ_vec

