
import numpy as np
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.utils.size import size_matlab
from numpy import zeros, sqrt, sign

__all__ = ['Prox_l0', 'Prox_l1']


class Prox_l1(ProxOperator):
    """
    Subroutine for computing the prox of the l1-norm  -- aka the soft-shrinkage-thresholder -- in Rn
    """

    def eval(u, shrinkage_parameter, prox_idx=None):
        """
        Parameters
        ----------
        u : array_like - Input, array to be projected

        Returns
        -------
        p_Sparsity : array_like, the projection
        """
        p_shrink = np.zeros(u.shape)
        for j in range(len(u)):
            if u[j]>0:
                p_shrink[j] = np.maximum(0, u[j] - shrinkage_parameter)
            elif u[j]<0:
                p_shrink[j] = np.minimum(0, u[j] + shrinkage_parameter)

        return p_shrink

class Prox_l0(ProxOperator):
    """
    Subroutine for computing the prox of the counting function -- aka the hard thresholder -- in Rn
    """

    def eval(u, threshold_parameter, prox_idx=None):
        """
        Prox subroutine for computing the l0-prox/hard-threshold operator

        Parameters
        ----------
        X : array_like - Input, array to be mapped

        Returns
        -------
        p_threshold : array_like, the prox point
        """
        p_threshold = zeros(u.shape)
        for j in range(len(u)):
            if u[j] - sqrt(2*threshold_parameter) > 0:
                p_threshold[j] = u[j]
            elif u[j] + sqrt(2*threshold_parameter) < 0:
                p_threshold[j] = u[j]
        return p_threshold
