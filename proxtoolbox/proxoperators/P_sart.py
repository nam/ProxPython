from proxtoolbox.proxoperators.proxoperator import ProxOperator
from skimage.transform import _radon_transform
from skimage.transform._radon_transform import sart_projection_update
import numpy as np

class P_sart(ProxOperator):
    def __init__(self, experiment):
        super().__init__(experiment)
        if hasattr(experiment, 'b'):
            self.b = experiment.b
        else:
            self.b = None
        if hasattr(experiment, 'dtype'):
            self.dtype = experiment.dtype
        else:
            self.dtype = None
        if hasattr(experiment, 'theta'):
            self.theta = experiment.theta
        else:
            self.theta = None


    def eval(self, u, prox_idx=None):
        if prox_idx == None:
            prox_idx = 0

        if self.b.ndim != 2:
            raise ValueError('image must be two dimensional')

        if self.dtype is None:
            if self.b.dtype.char in 'fd':
                dtype = self.b.dtype
            else:
                warn(
                "Only floating point data type are valid for SART inverse "
                "radon transform. Input data is cast to float. To disable "
                "this warning, please cast image_radon to float."
                )
            self.dtype = np.dtype(float)
        elif np.dtype(self.dtype).char not in 'fd':
            raise ValueError(
                "Only floating point data type are valid for inverse " "radon transform."
            )

        dtype = np.dtype(self.dtype)
        self.b = self.b.astype(dtype, copy=False)

        u_shape = (self.b.shape[0], self.b.shape[0])

        if self.theta is None:
            self.theta = np.linspace(0, 180, self.b.shape[1], endpoint=False, dtype=dtype)
        elif len(self.theta) != self.b.shape[1]:
            raise ValueError(
                f'Shape of theta ({len(self.theta)}) does not match the '
                f'number of projections ({self.b.shape[1]})'
            )
        else:
            self.theta = np.asarray(self.theta, dtype=self.dtype)

        if u is None:
            u = np.zeros(u_shape, dtype=self.dtype)
        elif u.dtype != self.dtype:
            warn(f'object dtype does not match output dtype: ' f'object is cast to {self.dtype}')

        u = np.asarray(u, dtype=self.dtype)

        # The following sart_projection_update() function actually 
        # computes the negative of the gradient of the 
        # squared distance to the affine subspace corresponding 
        # to the image of the radon transform at a single
        # angle.  Moreover, this correspondence only 
        # holds when the discretization of the radon transform 
        # is coarse enough relative to the discretization in the
        # object domain that the individual ``rays" of the radon 
        # transform do not pass through the same pixels in the 
        # object domain, if that makes any sense.  
        # 
        neg_grad_sq_dist = sart_projection_update(
            u,
            self.theta[prox_idx],
            self.b[:, prox_idx],
            )
        # The projection is related to the (negative) gradient 
        # of the squared distance via:
        # P_C = Id - 1/2 nabla d^2_C
        # which leads to:
        y = u + 0.5*neg_grad_sq_dist
        return y
    
