from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.utils.functions import Quadratic
from proxtoolbox.utils.Bregman_potentials import *
import numpy as np
from numpy import cbrt, sqrt

class Prox_l1_comp_Q_Breg(ProxOperator):
    """
    Bregman Prox operator for the NoLips experiment with l1 primal regularization and Quartic coupling
    to the variables in the image of a quadratic form  
    
    """

    def __init__(self, experiment):

    def eval(self, u):
        """
        Computes the Bregman prox of the l1-norm via the formula in 
        Bolte, Sabach, Teboulle, and Vaisbourd (2018) 

        Input
        ----------
        u : cell of 2 vectors, the primal domain variable of dim n
            and the auxilliary image variable of dim m 
        prox_idx : int, optional
            Index of this prox operator
        scaling : scaling in the l1-prox
        
        Returns
        -------
        u_vec : vector of dim n
        """
        Q_xk_vec = Quadratic.eval(self,u[0])
        dQ_xk_mat = dQuadratic.eval(self,u[0])
        image_resid_vec = u[1]- Q_xk_vec
        dQxkyk_vec = -self.augmentation_scaling * matmul(map(transpose, dQ_xk_mat), image_resid_vec)
        dh_xk_vec = self.dBregman_potential.eval(u[0])
        tmp_vec = dh_xk_vec - 1/self.Lip_grad_k *  dQxkyk_vec
        self.shrinkage_parameter = self.primal_scaling / self.Lip_grad_k
        y_vec = Prox_l1.eval(tmp_vec)
        # using map avoids for... loops at each of the inner functions!
        a = sum(map(abs,y_vec)**2) # faster than linalg.norm(y_vec)**2
        # tmp = real root of a*t^3 + t - 1 via Cardano's formula
        tmp = cbrt(0.5/a + sqrt(0.25/(a**2) + (1/27)/(a**3))) + cbrt(0.5/a - sqrt(0.25/(a**2) + (1/27)/(a**3)))
        u_vec = (-1) * tmp * y_vec 
        return u_vec

