from proxtoolbox import proxoperators
from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj
from proxtoolbox.utils.functions import Quadratic


class Prox_l1_at_Q(ProxOperator):
    """
    Prox operator for the NoLips experiment with l1-penalized Quadratic constriants 
    
    """

    def eval(self, u):
        """
        Computes the prox of the l1-norm in the image of another mapping Q:dim(u)-> m. 

        Input
        ----------
        u : cell of vectors, the primal/domain variable dim n, and the 
            image/dual variable dim m...only actually uses the primal variable
        Q : a mapping u-> y of dimension m
        shrinkage_parameter : scaling in the l1-prox
        
        Returns
        -------
        y : vector of dim Q(u)
        """
        self.shrinkage_parameter = self.image_scaling
        y_prime = Quadratic.eval(self,u)
        y = ProxOperator.Prox_l1.eval(y_prime, self.shrinkage_parameter)
        return y

