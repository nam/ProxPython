"""
Written by Matthijs Jansen, November 2020.

Contains proxoperator to apply to multidimensional photoelectron spectroscopy data, in order to reconstruct a set
of molecular orbitals as well as background contributions
"""

from numpy import sum, where, moveaxis
from numpy.core._multiarray_umath import sqrt, array

from proxtoolbox.proxoperators import ProxOperator, P_M, P_Sparsity_real

__all__ = ['P_nondispersive']


class P_nondispersive(ProxOperator):
    """
    Constrain molecular orbitals to be non-dispersive, i.e. u(k) should not depend on energy
    """
    def __init__(self, experiment):
        super(P_nondispersive, self).__init__(experiment)
        self.energy_axis = experiment.energy_axis
        self.state_axis = experiment.state_axis
        self.momentum_axes = experiment.momentum_axes
        self.ns = experiment.u0.shape[self.state_axis]
        self.ne = experiment.u0.shape[self.energy_axis]

    def eval(self, u, prox_idx=None):
        if self.state_axis !=0 and self.energy_axis != 1:
            u = moveaxis(u, [self.state_axis, self.energy_axis], [0, 1])
        out = u.copy()
        profiles = sum(u, axis=self.energy_axis)  # Integrate over the energy axis
        profiles /= sqrt(sum(abs(profiles)**2, axis=self.momentum_axes))
        norms = sqrt(sum(abs(u)**2, axis=self.momentum_axes))
        for s in range(self.ns):
            for e in range(self.ne):
                out[s, e] = profiles[e] * norms[s, e]

        if self.state_axis !=0 and self.energy_axis != 1:
            out = moveaxis(out, [0, 1], [self.state_axis, self.energy_axis])
        return out
