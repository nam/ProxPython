from proxtoolbox.proxoperators.proxoperator import ProxOperator

import numpy as np
from scipy.sparse.linalg import svds


__all__ = ['MC_P_M']

class MC_P_M(ProxOperator):
    """
    Class for matrix completion experiment with alternating projections.

    Written by Maximilian Seeber, 
    Universität Göttingen, 2022.

    """
    def __init__(self, experiment):
        super().__init__(experiment)
        self.r = experiment.r

    def eval(self, u):
        X = np.copy(u)
        U, sigma, V = svds(X, k=self.r)
        X = U @ np.diag(sigma) @ V
        return X