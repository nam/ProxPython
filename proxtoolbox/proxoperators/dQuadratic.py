from numpy import matmul, reshape, transpose
from proxtoolbox.proxoperators.proxoperator import ProxOperator


class dQuadratic(ProxOperator):
    '''
    DESCRIPTION: computes the gradient of a vector of quadratics. 
                 Of course, not a prox mapping, but kind of sort of 
                 belongs to this class 
    INPUT:  x            := the variable          
            A_tens       := a tensor (3D array) dimension nxnxm
            b_mat        := a matrix (2D array) mxn
    OUTPUT: dQ_vec a matrix of gradients of the m quadratics
    ''' 
    def __init__(self, experiment):
        self.A_tens = experiment.A_tens
        self.b_mat = experiment.b_mat

    def eval(self, X, prox_idx=None):
        dQ_vec = reshape(matmul(self.A_tens, X)+matmul(transpose(self.A_tens,(0,2,1)), X), self.b_mat.shape) + self.b_mat
        return dQ_vec

