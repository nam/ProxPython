
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils import fft, ifft
from proxtoolbox.utils.accessors import accessors
import numpy as np
from numpy import sqrt, conj, tile, mean, exp, angle, trace, reshape
from numpy.linalg import norm
from numpy.fft import fft2, ifft2

class P_CDP_cyclic(ProxOperator):
    """
    Prox operator that projects onto Fourier magnitude constraints
    with nonunitary, but invertible masks. 

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Sept. 27, 2016.    
    """

    def __init__(self, experiment):
        self.use_one_dim_FFT = experiment.Ny == 1 or experiment.Nx == 1
        self.masks = experiment.masks
        self.rt_data = experiment.rt_data
        self.product_space_dimension = experiment.product_space_dimension
        self.Nx = experiment.Nx
        self.Ny = experiment.Ny

    def eval(self, u, prox_index = None):
        """
        Projection onto Fourier magnitude constraints
        with nonunitary, but invertible masks        

        Parameters
        ----------
        u : Cell or ndarray
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_new : Cell or ndarray
            The projection
        """
        if prox_index is not None:
            j = prox_index
        else:
            j = 0
        L = self.product_space_dimension
        get, _set, _data_shape = accessors(self.rt_data, self.Nx, self.Ny, L)
        if self.Nx == 1 or self.Ny == 1:
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)
        else:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)

        U = FFT(get(self.masks, j)*u)
        U0 = magproj(get(self.rt_data, j), U) 
        u_new = IFFT(U0)/get(self.masks,j)

        return u_new


