
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils import fft, ifft
from proxtoolbox.utils.accessors import accessors
import numpy as np
from numpy import sqrt, conj, tile, mean, exp, angle, trace, reshape
from numpy.linalg import norm
from numpy.fft import fft2, ifft2

class P_CDP(ProxOperator):
    """
    Prox operator that projects onto Fourier magnitude constraints
    with nonunitary, but invertible masks. 

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Sept. 27, 2016.    
    """

    def __init__(self, experiment):
        self.use_one_dim_FFT = experiment.Ny == 1 or experiment.Nx == 1
        self.masks = experiment.masks
        self.rt_data = experiment.rt_data
        self.product_space_dimension = experiment.product_space_dimension
        self.Nx = experiment.Nx
        self.Ny = experiment.Ny

    def eval(self, u, prox_index = None):
        """
        Projection onto Fourier magnitude constraints
        with nonunitary, but invertible masks        

        Parameters
        ----------
        u : Cell or ndarray
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_new : Cell or ndarray
            The projection
        """
        L = self.product_space_dimension
        get, set, _data_shape = accessors(u, self.Nx, self.Ny, L)
        if isCell(u):
            u_new = Cell(L)
        else:
            u_new = np.empty(u.shape, dtype=np.complex128)

        for j in range(L):
            u_j = get(u, j)
            masks_j = get(self.masks, j)
            rt_data_j = get(self.rt_data, j)
            m, n, _p, _q = size_matlab(u_j)
            if m > 1 and n > 1:
                FFT = lambda u: fft2(u)
                IFFT = lambda u: ifft2(u)
            else:
                FFT = lambda u: fft(u)
                IFFT = lambda u: ifft(u)

            U = FFT(masks_j*u_j)
            U0 = magproj(rt_data_j, U)  # project U onto constraint rt_data
            u_new_j = IFFT(U0)/masks_j
            set(u_new, j, u_new_j)

        return u_new


