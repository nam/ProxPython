
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox import proxoperators
from proxtoolbox.utils.cell import Cell, isCell
import numpy as np
from numpy import zeros, ones, zeros_like

# Prox operators used by CT experiments (ART)
"""Inverse radon transform.

    Reconstruct an image from the radon transform.  
    This collection of classes has two different approaches, 
    the first uses an explicit and not very sophisticated 
    matrix representation of the discretized radon transform. 
    The second approach uses the implementation of the 
    Simultaneous Algebraic Reconstruction Technique (SART) algorithm
    in scikit-image (https://scikit-image.org/).

    Notes
    -----
    Algebraic Reconstruction Techniques are based on formulating the tomography
    reconstruction problem as a set of linear equations. Along each ray,
    the projected value is the sum of all the values of the cross section along
    the ray. A typical feature of SART (and a few other variants of algebraic
    techniques) is that it samples the cross section at equidistant points
    along the ray, using linear interpolation between the pixel values of the
    cross section. The resulting set of linear equations are then solved using
    a slightly modified Kaczmarz method.

    When using SART, a single iteration is usually sufficient to obtain a good
    reconstruction. Further iterations will tend to enhance high-frequency
    information, but will also often increase the noise.

    References
    ----------
    .. [1] AC Kak, M Slaney, "Principles of Computerized Tomographic
           Imaging", IEEE Press 1988.
    .. [2] AH Andersen, AC Kak, "Simultaneous algebraic reconstruction
           technique (SART): a superior implementation of the ART algorithm",
           Ultrasonic Imaging 6 pp 81--94 (1984)
    .. [3] S Kaczmarz, "Angenäherte auflösung von systemen linearer
           gleichungen", Bulletin International de l’Academie Polonaise des
           Sciences et des Lettres 35 pp 355--357 (1937)
    .. [4] Kohler, T. "A projection access scheme for iterative
           reconstruction based on the golden section." Nuclear Science
           Symposium Conference Record, 2004 IEEE. Vol. 6. IEEE, 2004.
    .. [5] Kaczmarz' method, Wikipedia,
           https://en.wikipedia.org/wiki/Kaczmarz_method

"""


class CT_ProxOperator(ProxOperator):
    """
    Base class for CT prox operators
    """
    def __init__(self, experiment):
        self.A = experiment.A
        self.b = experiment.b
        self.aaT = experiment.aaT
        self.m = experiment.m
        self.n = experiment.n
        self.alg = experiment.algorithm # needed to obtain iteration count

class P_hyperplane(CT_ProxOperator):
    """
    Projection onto hyperplane 
    """

    def __init__(self, experiment):
        super(P_hyperplane, self).__init__(experiment)




    def eval(self, u, prox_idx=None):
        """
        Projects a point u onto a hyperplane given by the prox_idx-th row
        of matrix A and b[prox_idx]

        Parameters
        ----------
        u : array-like
            point to be projected (vector)
        prox_idx : int
            Index of this prox operator
        
        Returns
        -------
        y : array-like
            The projection (vector).
        """
        if prox_idx == None:
            prox_idx = 0

        # we want matrices not vectors
        A_idx_mat = np.reshape(self.A[prox_idx,:], (1, self.A.shape[1]))
        A_idx_mat_t = A_idx_mat.T

        proj_mult = np.eye(self.n) - A_idx_mat_t*A_idx_mat/self.aaT[prox_idx]
        
        A_idx_t_vec = A_idx_mat_t.flatten() # we now want a vector not a matrix
        y = proj_mult @ u + A_idx_t_vec/self.aaT[prox_idx]*self.b[prox_idx]

        return y


# The next two prox operators are no longer used
# they would require a specific formulation beyond
# 'product space' and 'cyclic'.

class P_sequential_hyperplane_odd(CT_ProxOperator):
    """
    Hyperplane projection subroutine of the ART method  
    for tomographic reconstruction of a density profile. 

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on May 29, 2012.
    """

    def __init__(self, experiment):
        super(P_sequential_hyperplane_odd, self).__init__(experiment)

    def eval(self, u, prox_idx = None):
        """
        Evaluate this prox operator

        Parameters
        ----------
        u : array-like
            point to be projected (vector)
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        v : array-like
            The projection (vector).
        """
        iterCount = self.alg.iter
        K = np.mod(2*iterCount+1, self.m)
        a = np.reshape(self.A[K,:], (1, self.A.shape[1]))  # we want a matrix, not a vector
        aaT = (a @ a.T) + 1e-20
        b = self.b[K] / aaT
        v = a @ u / aaT
        v = u + a.T @ (b - v)
        return v


class P_sequential_hyperplane_even(CT_ProxOperator):
    """
    Hyperplane projection subroutine of the ART method  
    for tomographic reconstruction of a density profile. 

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on May 29, 2012.
    """

    def __init__(self, experiment):
        super(P_sequential_hyperplane_even, self).__init__(experiment)

    def eval(self, u, prox_idx = None):
        """
        Evaluate this prox operator

        Parameters
        ----------
        u : array-like
            point to be projected (vector)
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        v : array-like
            The projection (vector).
        """
        iterCount = self.alg.iter
        K = np.mod(2*iterCount, self.m)
        a = self.A[K, :]
        a = a.reshape((1, a.size))  # numpy doesn't differentiate between row and colum vectors for 1D
        aaT = (a @ a.T) + 1e-20
        b = self.b[K] / aaT
        v = a @ u / aaT
        v = u + a.T @ (b - v)
        return v

