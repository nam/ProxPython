
import numpy as np
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.utils.size import size_matlab
from numpy import zeros, sqrt, sign

__all__ = ['Prox_l0', 'Prox_l1', 'Prox_linfty', 'Proj_l1', 'Proj_l2', 'Proj_linfty']


class Prox_l1(ProxOperator):
    """
    Subroutine for computing the prox of the l1-norm  -- aka the soft-shrinkage-thresholder -- in Rn
    """

    def eval(u, shrinkage_parameter, prox_idx=None):
        """
        Parameters
        ----------
        u : array_like - Input, array to be projected

        Returns
        -------
        p_Sparsity : array_like, the projection
        """
        p_shrink = np.zeros(u.shape)
        for j in range(len(u)):
            if u[j]>0:
                p_shrink[j] = np.maximum(0, u[j] - shrinkage_parameter)
            elif u[j]<0:
                p_shrink[j] = np.minimum(0, u[j] + shrinkage_parameter)

        return p_shrink

class Prox_linfty(ProxOperator):
    """
    Subroutine for computing the prox of the lambda*l_infty-norm  in Rn
    """

    def eval(u, prox_parameter, prox_idx=None):
        """
        Parameters
        ----------
        u : array_like - Input, array to be projected

        Returns
        -------
        p_linfty : array_like, the projection
        """
        tmp=u / prox_parameter
        return u - prox_parameter * Proj_l1.eval(tmp,1)

class Prox_l0(ProxOperator):
    """
    Subroutine for computing the prox of the counting function -- aka the hard thresholder -- in Rn
    """

    def eval(u, threshold_parameter, prox_idx=None):
        """
        Prox subroutine for computing the l0-prox/hard-threshold operator

        Parameters
        ----------
        X : array_like - Input, array to be mapped

        Returns
        -------
        p_threshold : array_like, the prox point
        """
        p_threshold = zeros(u.shape)
        for j in range(len(u)):
            if u[j] - sqrt(2*threshold_parameter) > 0:
                p_threshold[j] = u[j]
            elif u[j] + sqrt(2*threshold_parameter) < 0:
                p_threshold[j] = u[j]
        return p_threshold

class Proj_l1(ProxOperator):
    """
    Projection onto the scaled l1 ball
    
    Grabbed from https://sigpy.readthedocs.io/en/latest/_modules/sigpy/thresh.html#l1_proj
    """
    def eval(u, eps, prox_idx=None):
        """Projection onto L1 ball.

        Args:
            eps (float, or array): L1 ball scaling.
            u (array)

        Returns:
            array: Result.

        References:
            J. Duchi, S. Shalev-Shwartz, and Y. Singer, "Efficient projections onto
            the l1-ball for learning in high dimensions" 2008.

        """
        shape = u.shape
        input = u.ravel()

        if np.linalg.norm(input, 1) < eps:
            return input
        else:
            size = len(input)
            s = np.sort(np.abs(input))[::-1]
            st = (np.cumsum(s) - eps) / (np.arange(size) + 1)
            idx = np.flatnonzero((s - st) > 0).max()
            # throws and error when this is empty
            return Prox_l1.eval(input.reshape(shape), st[idx])

class Proj_l2(ProxOperator):
    """
    Projection onto the scaled l2 ball
    """
    def eval(u, radius, prox_idx=None):
        """Projection onto L2 ball.

        Args:
            radius (float): L2 ball scaling.
            input (array)

        Returns:
            array: Result.

        """   

        norm_u = np.linalg.norm(u, 2) 
        if norm_u < radius:
            return u
        else: 
            return (radius/norm_u)*u

class Proj_linfty(ProxOperator):
    """
    Projection onto the scaled l_infinity ball
    """
    def eval(u, radius, prox_idx=None):
        """Projection onto Linfty ball.

        Args:
            radius (float): L_infinity ball scaling.
            u (array)

        Returns:
            array: Result.

        """   

        return np.clip(u, - radius, radius)
