from proxtoolbox import proxoperators
from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj
from proxtoolbox.utils.cell import Cell, isCell
import numpy as np
from numpy import conj, ones, zeros, exp, \
    log, tile, real, pi
from numpy.fft import fft2, ifft2, fft, ifft


class Approx_Pphase_JWST_Wirt(ProxOperator):
    """
    Prox operator implementing projection onto Fourier
    magnitude constraints in the far field with 
    Poisson noise for the JWST data set.
    Customized for the Wirtinger flow code of 
    Candes et al to use their data formats.  The comparison
    of the Wirtinger flow approach to better and older
    approaches has appeared in Luke, Sabach&Teboulle, 
    "Proximal Algorithms for Phase Retrieval: a comparison"
    (2018)
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) around Jan 3, 2018.
    """

    def __init__(self, experiment):
        self.FT_conv_kernel = experiment.FT_conv_kernel
        self.Nx = experiment.Nx
        self.Ny = experiment.Ny
        self.data = experiment.data
        self.data_sq = experiment.data_sq
        self.data_zeros = experiment.data_zeros
        self.data_ball = experiment.data_ball
        self.TOL2 = experiment.TOL2
        self.product_space_dimension = experiment.product_space_dimension

    def eval(self, u, prox_idx=None):
        """
        Projection onto Fourier magnitude constraints in the near field
        or far field with Poisson noise.
            
        Parameters
        ----------
        u : array-like
            Function in the physical domain to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_new : array_like
            The projection in the physical (time) domain in the
            same format as u.
        """

        if isCell(u):
            u_new = Cell(len(u))
            for j in range(self.product_space_dimension):
                u_new[j] = self.evalHelper(u[j], j)
        else:
            raise NotImplementedError('Error Approx_Pphase_JWST_Wirt: only Cells are supported at the moment')

        return u_new


    def evalHelper(self, u, idx):
        m = u.shape[0]
        if u.ndim > 1:
            n = u.shape[1]
        else:
            n = 1
        if m > 1 and n > 1:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)
        else:
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)

        u_hat = FFT(self.FT_conv_kernel[idx]*u)/(self.Nx*self.Ny)
        u_hat_sq = real(u_hat * conj(u_hat))

        # Update data_sq to prevent division by zero.
        # Note that data_sq_zeros, as defined below,
        # can be different from self.data_zeros[j]] 
        # (e.g., JWST experiment).
        data_sq = self.data_sq[idx].copy()
        data_sq_zeros = np.where(data_sq == 0)
        data_sq[data_sq_zeros] = 1

        tmp = u_hat_sq / data_sq
        if self.data_zeros is not None:
            tmp[self.data_zeros] = 1
            u_hat_sq[self.data_zeros] = 0
        I_u_hat = tmp == 0
        tmp[I_u_hat] = 1
        tmp = log(tmp)
        h_u_hat = real(sum(sum(u_hat_sq*tmp + self.data_sq[idx] - u_hat_sq)))

        if h_u_hat >= self.data_ball + self.TOL2:
            p_Mhat = magproj(self.data[idx], u_hat)
            u_new = (conj(self.FT_conv_kernel[idx])*IFFT(p_Mhat)) * (self.Nx*self.Ny)
        else:
            u_new = u
        return u_new

