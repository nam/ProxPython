
import numpy as np 
from numpy import linalg
from proxtoolbox.proxoperators.proxoperator import ProxOperator

__all__ = ['l2', 'dl2', 'h_4', 'dh_4']


class l2(ProxOperator):
    '''
        the 1/2 * l2-norm-squared
        Works on vectors of length len(X)
        Returns the l2-norm
    ''' 
    def eval(X):    
        # using map avoids for... loops at each of the inner functions!
        val = 0.5 * sum(map(abs,X)**2) 
        return val

class dl2(ProxOperator):
    '''
        the derivative of 1/2 * l2-norm-squared
        Works on vectors of length len(X)
        Returns X
    ''' 
    def eval(X):
        # does nothing!
        return X


# I have removed the h_4_2 operator to avoid making the 
# mistake of acutally using this in a NoLips experiment:  
# computing the prox of the objective function  F + phi
# already implicity uses the Bregman potential phi + 1/2||.||^2

class h_4(ProxOperator):
    '''
        DESCRIPTION: computes 1/4||x||^4_2 
        INPUT:  x            := the variable          
        OUTPUT: h a scalar
    ''' 
    def eval(X, alpha = 0.25):
        # using map avoids for... loops at each of the inner functions!
        tmp = sum(map(abs,X)**2)
        h = alpha * tmp**2 
        return h

class dh_4(ProxOperator):
    '''
        DESCRIPTION: computes the gradient of 1/4||x||^4_2 
        INPUT:  x            := the variable          
        OUTPUT: dh a vecotr size x
    ''' 
    def eval(X, alpha = 0.25):
        tmp = linalg.norm(X)
        dh_vec = (alpha * 4 * tmp**2) * X
        return dh_vec
    



