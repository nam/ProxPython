import numpy as np
from proxtoolbox.proxoperators.proxoperator import ProxOperator


__all__ = ['P_anti_symmetry','P_symmetry','P_symmetry_xy','P_symmetry_realdata']


class P_symmetry(ProxOperator):

    """
    projection onto SYM set, simulated data
    """
    def eval(self,u,prox_idx=None):
        """
        Applies the proxoperator P_symmetry

        Parameters
        ----------
        u : array_like - function in the real domain to be projected

        Returns
        -------
        array_like - x: the projection on symmetry and antisymmetry domain.
        """
        u1 = (u - np.flip(u,1))/2 # Oy
        u2 = (u1 - np.flip(u1,2))/2 # Oz
        u3 = (u2 + np.flip(u2,0))/2 # Ox
        return u3

class P_symmetry_xy(ProxOperator):

    """
    projection onto SYM set, simulated data
    """
    def eval(self,u,prox_idx=None):
        """
        Applies the proxoperator P_symmetry

        Parameters
        ----------
        u : array_like - function in the real domain to be projected

        Returns
        -------
        array_like - x: the projection on symmetry and antisymmetry domain.
        """
        u1 = (u - np.flip(u,1))/2 # Oy
        u2 = (u1 + np.flip(u1,0))/2 # Ox
        return u2

class P_symmetry_realdata(ProxOperator):
    """
    projection onto SYM set, experimental data
    """

    def __init__(self, experiment):
        """
        Initialization
        """
        
        super().__init__(experiment)
        self.antiX = experiment.antiX
        self.antiY = experiment.antiY


    def eval(self,u,prox_idx=None):
        """
        Applies the proxoperator P_symmetry

        Parameters
        ----------
        u : array_like - function in the real domain to be projected

        Returns
        -------
        array_like - x: the projection on symmetry and antisymmetry domain.
        """
        if self.antiX:
            u1 = (u - np.flip(u,0))/2
        else:
            u1 = (u + np.flip(u,0))/2
        if self.antiY:
            u2 = (u1 - np.flip(u1,1))/2
        else:
            u2 = (u1 + np.flip(u1,1))/2
        u3 = (u2 - np.flip(u2,2))/2
        return u3

class P_anti_symmetry(ProxOperator):
    """
    projection onto SYM set, experimental data
    """


    def eval(self,u,prox_idx=None):
        """
        Applies the proxoperator P_symmetry

        Parameters
        ----------
        u : array_like - function in the real domain to be projected

        Returns
        -------
        array_like - x: the projection on symmetry and antisymmetry domain.
        """
        
        u1 = (u - np.flip(u,1))/2 # Ox
        u2 = (u1 - np.flip(u1,0))/2 # Oy
        return u2