from proxtoolbox.proxoperators.proxoperator import ProxOperator

import numpy as np

class MC_ALS(ProxOperator):
    """
    Class for matrix completion experiment with alternating least squares.

    Written by Maximilian Seeber, 
    Universität Göttingen, 2022.
    """

    def __init__(self, experiment):
        super().__init__(experiment)
        self.n_1 = experiment.n_1
        self.n_2 = experiment.n_2
        self.r = experiment.r
        self.lambd = experiment.lambd
        self.mask = experiment.mask
        self.M = experiment.M
        self.C_u = [np.diag(row) for row in self.mask]
        self.C_v = [np.diag(col) for col in self.mask.T]

    def eval(self, u):
        U, V = u

        for i in range(self.n_1):
            U[:,i] = np.linalg.solve(V @ self.C_u[i] @ V.T +
                self.lambd * np.eye(self.r), V @ self.C_u[i] @ self.M[i,:])

        for j in range(self.n_2):
            V[:,j] = np.linalg.solve(U @ self.C_v[j] @ U.T +
                self.lambd * np.eye(self.r), U @ self.C_v[j] @ self.M[:,j])


        return U, V