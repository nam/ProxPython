from proxtoolbox.experiments.experiment import Experiment
from proxtoolbox import proxoperators
from proxtoolbox.utils.cell import Cell, isCell


#for downloading data
import proxtoolbox.utils.GetData as GetData
from proxtoolbox.utils.GetData import datadir
# for loading and processing images
from proxtoolbox.utils.load_image_to_float_array import load_image_to_float_array 
from proxtoolbox.utils.ZeroPad import ZeroPad 
from proxtoolbox.utils.Resize import Resize 

# for the radon transform and projection
from skimage.transform import radon

import numpy as np
from scipy.io import loadmat
from numpy.linalg import norm
from math import sqrt

# graphics
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure
from proxtoolbox.utils.graphics import addColorbar


class Gauss_Experiment(Experiment):
    """
    Gauss linear equations experiment class
    """

    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'Gauss',
            'object': 'real',
            'rescale': True,
            'Nx':256,
            'Ny':256,
            'image_scaling': 0.5,
            'MAXIT': 20,
            'TOL': 1e-4,
            'lambda_0': 0.75,
            'lambda_max': 0.75,
            'lambda_switch': 13,
            'data_ball': 1e-15,
            'diagnostic': True,
            'iterate_monitor_name': 'IterateMonitor',
            'verbose': 0,
            'graphics': 1,
            'anim': False,
            'debug': True
        }
        return defaultParams

    def __init__(self,
                 **kwargs):
        """
        """
        # call parent's __init__ method
        super(Gauss_Experiment, self).__init__(**kwargs)

        # do here any data member initialization
        self.inner_dimension = None
        self.outer_dimension = None
        self.block_step = None
        self.A = None
        self.theta=None
        self.b = None
        self.m = None
        self.n = None

    def loadData(self):
        """
        Load Gauss dataset. Create the initial iterate.
        """
        #make sure input data can be found, otherwise download it
        GetData.getData('Gauss')
        
        # load data
        if not self.silent:
            print('Loading data file Carl_Friedrich_Gauss.jpg ')
        # Open the image using PIL
        image_path = datadir/'Gauss'/'Carl_Friedrich_Gauss.jpg'
        float_array, original_shape = load_image_to_float_array(image_path)
        # img_array is the true object that will be 
        # mapped to a point in the image space of the 
        # linear mapping A:
        img_array = float_array.reshape(original_shape)

        # Zero pad and resize the image as needed:  
        # zero pad the array to the nearest square dyad (for FFT)
        img_array = ZeroPad(img_array)
        self.truth=img_array
        # keep track of the nonzero indexes in case
        # one wants to apply a support constraint
        self.support_idx = np.nonzero(img_array)
        # Flatten the array
        img_vec = img_array.flatten()

        # The linear mapping is the discrete radon transform
        # implemented by the call `radon(img_array, theta=self.theta)'
        # We undersample the radon transform on purpose.  This will lead
        # to artifacts in the reconstruction, but more interesting 
        # is the nonuniqueness of solutions to the corresponding
        # linear system. 
        self.A='radon'

        self.theta = np.linspace(0.0, 180.0, round(max(img_array.shape)*self.image_scaling), endpoint=False)
        self.b = radon(img_array, theta=self.theta)
        self.Ny = len(img_vec)
        self.inner_dimension = self.b.shape[0]
        self.outer_dimension = self.b.shape[1]
        self.Nx = 1
        self.Nz = 1
        self.block_step = self.inner_dimension

        self.sets = self.outer_dimension
        if self.formulation == 'product space':
            self.product_space_dimension = self.sets
            self.u0 = Cell(self.product_space_dimension)
            for j in range(self.product_space_dimension):
                self.u0[j] = np.zeros(img_array.shape)
        else:
            self.u0 = np.zeros(img_array.shape)
            self.product_space_dimension = 1

        self.m = self.b.shape[0]*self.b.shape[1]
        self.n = self.Ny
        # the next is a generic scaling
        # that removes the dependence of the 
        # norms from the problem dimension. 
        # More specific normalizations are up to the user. 
        self.norm_data = np.sqrt(self.Ny)


    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(Gauss_Experiment, self).setupProxOperators()  # call parent's method

        self.proxOperators = []
        self.productProxOperators = []

        if self.formulation == 'product space':
            self.nProx = 2
            self.proxOperators.append('P_diag')
            self.proxOperators.append('Prox_product_space')
            self.n_product_Prox = self.product_space_dimension
            for _j in range(self.n_product_Prox):
                self.productProxOperators.append('P_sart')
            if hasattr(self, 'constraint'):
                if self.constraint == 'sparse transform':
                    self.n_product_Prox +=1
                    self.productProxOperators.append('P_Sparse_transform')
                elif self.constraint == 'nonnegative and support':
                    self.n_product_Prox +=1
                    self.productProxOperators.append('P_SP')
        else:
            self.nProx = self.sets
            self.product_space_dimension = 1
            for _j in range(self.nProx):
                self.proxOperators.append('P_sart')
            if hasattr(self, 'constraint'):
                if self.constraint == 'sparse transform':
                    self.nProx +=1
                    self.proxOperators.append('P_Sparse_transform')
                elif self.constraint == 'nonnegative and support':
                    self.nProx +=1
                    self.proxOperators.append('P_SP')


    def setFattening(self):
        """
        Optional method for fattening/regularizing sets
        Called by initialization() method after 
        instanciating prox operators but before instanciating
        the algorithm
        """

        # Estimate the gap in the relevant metric
        computeGap = False # deactivated for now as it takes too long to compute
        if computeGap: 
            # simple for now...    
            if self.formulation == 'product space':
                proxOps = self.productProxOperators
                u0 = self.u0[0]
            else:
                proxOps = self.proxOperators
                u0 = self.u0
            prox = proxOps[0](self)
            u1 = prox.eval(u0, 0)
            u2 = u1
            tmp_gap = 0
            for j in range(1, len(proxOps)):
                prox = proxOps[j](self)
                u2 = prox.eval(u1, j)
                tmp_gap += (norm(u1 - u2)/self.norm_data)**2

        else:
            # use cached value
            tmp_gap = 131.26250306395025  # u2 = prox.eval(u2, j)
            # tmp_gap = 0.7668342258350885  # u2 = prox.eval(u1, j)
        gap_0 = sqrt(tmp_gap)

        # sets the set fattening to be a percentage of the
        # initial gap to the unfattened set with 
        # respect to the relevant metric (KL or L2), 
        # that percentage given by
        # input.data_ball input by the user.
        self.data_ball = self.data_ball*gap_0
        # the second tolerance relative to the oder of 
        # magnitude of the metric
        self.TOL2 = self.data_ball*1e-15

    def show(self):

        u_m = self.output['u_monitor']
        if isCell(u_m):
            u = u_m[0]
            u2 = u2 = u_m[len(u_m)-1]
        else:
            u = self.output['u']
            u2 = u_m

        f, ((ax1, ax2), (ax3, ax4)) = subplots(2, 2,
                                               figsize=(self.figure_width, self.figure_height),
                                               dpi=self.figure_dpi)
        self.createImageSubFigure(f, ax1, u, 'solution')
        self.createImageSubFigure(f, ax2, u-self.truth, 'error')

        changes = self.output['stats']['changes']
        time = self.output['stats']['time']
        time_str = "{:.{}f}".format(time, 5) # 5 is precision
        xLabel = "Iterations (time = " + time_str + " s)"
        algo_desc = self.algorithm.getDescription()
        title = "Algorithm " + algo_desc
        ax3.plot(changes)
        ax3.set_yscale('log')
        ax3.set_xlabel(xLabel)
        ax3.set_ylabel('log of iterate difference')
        ax3.set_title(title)

        if self.diagnostic and 'gaps' in self.output['stats']:
            gaps = self.output['stats']['gaps']
            ax4.semilogy(gaps)
            ax4.set_xlabel(xLabel)
            ax4.set_ylabel('Log of the gap distance')
        else:
            ax4.remove()
        show()

    def animate(self, alg):
        """
        Display animation. This method is called
        after each iteration of the algorithm `alg` if
        data member `anim` is set to True.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that is running.
        """
        # the output is a picture in a column vector
        if alg.u_new is not None:
            u = alg.u_new
        else:  # this the case when called before algorithm runs
            u = alg.u
        title = "Iteration " + str(alg.iter)
        self.animateFigure(u, title)


    def createImageSubFigure(self, f, ax, u, title = None):
        im = ax.imshow(u, cmap='gray')
        addColorbar(im)
        if title is not None:
            ax.set_title(title)

