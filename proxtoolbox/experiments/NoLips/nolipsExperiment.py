
from proxtoolbox.experiments.experiment import Experiment
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils.graphics import addColorbar
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure
import numpy as np


class NoLipsExperiment(Experiment):
    '''
    Base class for NoLips experiments. Concrete NoLips
    experiments should derive from this class
    '''
    @staticmethod
    def getDefaultParameters():
        raise NotImplementedError("Static method getDefaultParameters() must \
                                  be implemented in any concrete class derived from \
                                  Experiment class")

 
    def __init__(self, **kwargs):
        """
        """
        # call parent's __init__ method
        super(NoLipsExperiment, self).__init__(**kwargs)
        # do here any data member initialization
        self.input_ignore_attrs = ['input_ignore_attrs', 'output_ignore_keys',
                                   'proxOperators',
                                   'output', 'data_zeros',
                                   'figure_width', 'figure_height', 'figure_dpi']
        self.output_ignore_keys = ['u1', 'u2', 'plots']


    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(NoLipsExperiment, self).setupProxOperators()  # call parent's method

        # Algorithm specific initialization
        # The following code should be independent from the actual experiment
        # Now we adjust data structures and prox operators according to the algorithm
        if self.algorithm_name == 'ADM':
            self.iterate_monitor_name = 'ADM_IterateMonitor'
            if self.optimality_monitor is None:
                self.optimality_monitor = 'ADM_objective'
            if self.coupling_function is None:
                self.coupling_function = 'ADM_couplings'
        if self.algorithm_name == 'ADMM':
            # There are two main prox mappings, one for each of the first two
            # blocks. These prox mappings may be further decomposed, but that
            # is not handled at this level.
            # Set up Prox0, prox operator used in the primal prox block
            if not hasattr(self, 'Prox0'):
                self.Prox0 = self.proxOperators[0]  # given by previous call to parent

            self.iterate_monitor_name = 'ADMM_IterateMonitor'
            if self.optimality_monitor is None:
                self.optimality_monitor = 'ADMM_objective'
            self.lagrange_mult_update = 'ADMM_LagrangeUpdate'

    @property
    def input_params(self):
        """Return a dictionary describing the input parameters.
        Not included are subclasses and multi-dimensional lists."""
        vars_dict = vars(self)
        output = vars_dict.copy()
        for var in vars_dict:
            obj = vars_dict[var]
            # Exclude some variables:
            if var in self.input_ignore_attrs:
                output.pop(var)
                continue

            # Only include variables that are not an instance of a class or large numpy arrays
            if hasattr(obj, '__dict__') is True:
                output.pop(var)
                continue
            if (isinstance(obj, (list, np.ndarray)) and len(obj) > 0 and
                    isinstance(obj[0], (list, np.ndarray)) and len(obj[0]) > 1):
                output.pop(var)
                continue
        return output

    @property
    def output_for_saving(self):
        """ Create a dictionary of the output which can be saved. """
        output = {}
        for key in self.output:
            if key in self.output_ignore_keys:
                pass
            elif key == 'u_monitor':
                # replace list with a dict
                length = len(self.output[key])
                output[key] = {('%d' % i): self.output[key][i] for i in range(length)}
            else:
                output[key] = self.output[key]
        return output
        
    def show(self):
        """
        Generate graphical output from the solution...this was cut and pasted 
        from Phase...may not make sense for NoLips
        """
        u_m = self.output['u_monitor']
        # The goal here is to extract the data that we want to plot
        # One issue is that u_monitor may represent different things.
        # It would be nice to have a description of what u_monitor
        # is to avoid the following guessing process:
        if isCell(u_m):
            u = u_m[0]
            u2 = u_m[len(u_m)-1]
        else:
            u = self.output['u']
            u2 = u_m
        u_plot = self.getPlotData(u)
        u2_plot = self.getPlotData(u2)

        changes = self.output['stats']['changes']
        if 'time' in self.output['stats']:
            time = self.output['stats']['time']
        else:
            time = 999
            
        algo_desc = self.algorithm.getDescription()
        title = "Algorithm: " + algo_desc

        
        # figure 900
        if u_plot.ndim == 1:
            f = self.createTwoGraphFigure(u_plot, titles)
        else:
            f = self.createTwoImageFigure(u_plot, titles)
        f.suptitle(title)
        plt.subplots_adjust(hspace = 0.3) # adjust vertical space (height) between subplots (default = 0.2)
        plt.subplots_adjust(wspace = 0.4) # adjust horizontal space (width) between subplots (default = 0.2)

 
        time_str = "{:.{}f}".format(time, 5) # 5 is precision
        label = "Iterations (time = " + time_str + " s)"

        #figure 901
        f = plt.figure(figsize = (self.figure_width, self.figure_height))
        plt.semilogy(changes)
        plt.xlabel(label)
        plt.ylabel('Log of change in iterates')
        f.suptitle(title)

        #figure 902
        if 'gaps' in self.output['stats']:
            gaps = self.output['stats']['gaps']
            f = plt.figure(figsize = (self.figure_width, self.figure_height))
            plt.semilogy(gaps)
            plt.xlabel(label)
            plt.ylabel('Log of the gap distance')
            f.suptitle(title)

        if 'fvals' in self.output['stats']:
            fvals = self.output['stats']['fvals']
            f = plt.figure(figsize = (self.figure_width, self.figure_height))
            plt.semilogy(fvals)
            plt.xlabel(label)
            plt.ylabel('Log of the function values')
            f.suptitle(title)

        show()

    def getPlotData(self, u):
        """
        Helper function to extract data that we can plot
 
        Parameters
        ----------
        u : Cell or ndarray
            Iterate or monitored data
        """
        if isCell(u):
            u_plot = u[0]
        else:
            u_plot = u
            if self.product_space_dimension != 1 and u.ndim > 1:
                # we may need to refine this based on the info we have
                m, n, _p, _q = size_matlab(u)
                if m == self.product_space_dimension or m == 1:
                    u_plot = u[0,:]
                elif n == self.product_space_dimension or n == 1:
                    u_plot = u[:,0]
                elif u.ndim > 2:
                    u_plot = u[:,:,0]
        return u_plot

    def animate(self, alg):
        """
        Display animation. This method is called
        after each iteration of the algorithm `alg` if
        data member `anim` is set to True.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that is running.
        """
        if alg.u_new is not None:
            u = alg.u_new
        else:  # this the case when called before algorithm runs
            u = alg.u
        if isCell(u):
            u = u[0]
            if isCell(u):
                u = u[0]
    
        m, n, _p, _q = size_matlab(u)
        image = None
        if self.Nx == 1 or self.Ny == 1:
            # This is not an image
            # TODO Not implemented
            raise NotImplementedError('Animation of a plot is not implemented yet')
        else:
            # This is an image
            if m == 1 or n == 1:
                # image is the shape of a column vector
                N = int(np.sqrt(u.shape[0]))
                u = np.reshape(u, (N,N), order='F')
                image = np.real(u)
                title = "Iteration " + str(alg.iter)

        self.animateFigure(image, title)

    def createGraphSubFigure(self, ax, u, title = None):
        ax.plot(u)
        if title is not None:
            ax.set_title(title)


# TO DO:  initialize the algorithm and put in the function f and phi.  Not sure yet how to do this since these could vary. Should probably be done
# at this level since these are in principle independent of the particular 
# quadratic forms, but would want these to be parameters with defaults that the user could change.   
 

