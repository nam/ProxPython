import SetProxPythonPath
from proxtoolbox.experiments.Gauss.Gauss_Experiment import Gauss_Experiment

# The class `Gauss_Experiment' has a number of parameters that 
# one can choose.  
# `algorithm` is self-explanatory. 
# `formulation` tells ProxToolbox whether the model is 
#    on the `product space` or not; at the moment `cyclic` 
#    means `not product space`.  
# `image_scaling` determines the number of x-ray projection angles
#    as a factor of the resolution of the object input.  `image_scaling<1`
#    means that the number of angles is less than the number of parallel 
#    x-ray beams by the factor image_scaling. 
# `MAXIT' and `TOL` are self-explanatory
#    
Gauss = Gauss_Experiment(algorithm='CP', formulation ='cyclic', 
                   image_scaling=0.1, constraint='nonnegative and support', 
                   MAXIT=200, TOL=1e-4
                   )
Gauss.run()
# Gauss.animate(Gauss.algorithm)
Gauss.show()