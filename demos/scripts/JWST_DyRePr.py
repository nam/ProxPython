
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='DyRePr', TOL=5e-9, anim=True)
JWST.run()
JWST.show()
