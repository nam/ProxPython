import SetProxPythonPath
from proxtoolbox.experiments.phase.Sparse_real_symm_Experiment import Sparse_real_symm_Experiment
import json

Sparse2 = Sparse_real_symm_Experiment(algorithm='CDRl', save_output=True, MAXIT=5000, verbose=1, graphics=1, anim=True, lambda_max = 1.0, TOL=5e-6)
Sparse2.run()
Sparse2.show()

with open('../OutputData/2024_05_23/Sparse_real_symmm_CDRl.output.json', 'r') as file:
    data = json.load(file)
