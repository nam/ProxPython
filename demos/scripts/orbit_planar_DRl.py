import SetProxPythonPath
from proxtoolbox.experiments.orbitaltomography.planar_molecule import PlanarMolecule

exp_pm = PlanarMolecule(algorithm='DRl',
                           constraint='sparse real',
                           sparsity_parameter=40)
# exp_pm.plotInputData()
exp_pm.run()
exp_pm.show()
