import SetProxPythonPath
from proxtoolbox.experiments.BM.SumOfLeastSquares_Experiment import SumOfLeastSquares_Experiment

BM = SumOfLeastSquares_Experiment(algorithm='BCD',
                     inner_algorithm='GPM',
                     Nx=8, Ny=12, Nz=50,
                     randomizedAlgo=True, MAXIT=100)
BM.run()
BM.show()