
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='AvP', accelerator_name='GenericAccelerator')
sourceExp.run()
sourceExp.show()
