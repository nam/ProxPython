import SetProxPythonPath
from proxtoolbox.experiments.orbitaltomography.orthogonal_orbits import OrthogonalOrbitals

exp_pm = OrthogonalOrbitals(algorithm='CP',
                           constraint='sparse real')
# exp_pm.plotInputData()
exp_pm.run()
exp_pm.show()
