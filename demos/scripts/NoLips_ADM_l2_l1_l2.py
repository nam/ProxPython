
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_l1_l1_h42 = randomExperiment(algorithm='ADM', domain_objective='l2', image_objective='l1', Bregman_potential='l2', lambda_0=.05, lambda_max=.05, Lip_grad_max=2, augmentation_scaling=.005, image_dim=51, MAXIT=10000)
ADM_l1_l1_h42.run()
ADM_l1_l1_h42.show()

# Notes:  Converges linearly with nonconvex and convex coupling.
# But convergence is faster and ``nicer" with the convex coupling