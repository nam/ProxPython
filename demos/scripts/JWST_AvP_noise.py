
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='AvP', MAXIT=2000, TOL=5e-15, noise=True)
JWST.run()
JWST.show()
