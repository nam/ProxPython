
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_0_linfty_h42 = randomExperiment(algorithm='ADM', domain_objective= 'none', image_objective= 'linfty', coupling_function = 'Square_Quadratic_convex', coupling_approximation='linear', Bregman_potential='h_4', lambda_0=.05, lambda_max=.05, Lip_grad_max=15, augmentation_scaling=.008, domain_dim=30, image_dim=40, MAXIT=500000)
ADM_0_linfty_h42.run()
ADM_0_linfty_h42.show()

# Notes:  this works, with linear convergence and convex coupling, albeit very slow - 319657 iterations 
# Nonconvex coupling also seems to yield local linear convergence, but much slower (over 500000 iterations to get to 10^-6).