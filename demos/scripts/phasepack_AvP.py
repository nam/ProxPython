
import SetProxPythonPath
from proxtoolbox.experiments.phase.Phasepack_Experiment import Phasepack_Experiment

phasepack = Phasepack_Experiment(algorithm='AvP', formulation='cyclic', MAXIT=2000)
phasepack.run()
phasepack.show()
