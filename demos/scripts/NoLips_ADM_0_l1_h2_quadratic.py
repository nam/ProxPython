
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_0_l1_h2_quadratic = randomExperiment(algorithm='ADM', domain_objective= 'none', image_objective= 'l1', coupling_function = 'Square_Quadratic_convex', coupling_approximation='quadratic', Bregman_potential='l2', lambda_0=.05, lambda_max=.05, Lip_grad_max=5, augmentation_scaling=1, domain_dim=50, image_dim=51, MAXIT=100000)
ADM_0_l1_h2_quadratic.run()
ADM_0_l1_h2_quadratic.show()
