
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

# Bregman_potential='h_4' results in the total Bregman potential of 
# 1/4 ||.||^4 + 1/2||.||^2.  This is because computing the prox of the 
# F_phi function implicitly includes the second part of that sum. 
ADM_l2_linfty_h4 = randomExperiment(algorithm='ADM', domain_objective='l2', image_objective= 'linfty', Bregman_potential='h_4',lambda_0=.5, lambda_max=.5, Lip_grad_max=1, augmentation_scaling=.001, image_dim=51, MAXIT=100000)
ADM_l2_linfty_h4.run()
ADM_l2_linfty_h4.show()

# Notes:  performance is better with a convex coupling and linear approximation, 
# but not dramatically so (2 iterations faster)
# This one is slower than Bregman l2 by about 28 iterations.