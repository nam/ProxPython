
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment


CDP = CDP_Experiment(algorithm='CDRl', Nx=256, formulation='cyclic',
                     lambda_0=1.0, lambda_max=1.0, MAXIT=1000, TOL=1e-8)
CDP.run()
CDP.show()
