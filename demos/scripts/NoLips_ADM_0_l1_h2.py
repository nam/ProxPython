
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_0_linfty_h2 = randomExperiment(algorithm='ADM', domain_objective= 'none', image_objective= 'l1', coupling_function = 'Square_Quadratic_convex', coupling_approximation='linear', Bregman_potential='l2', lambda_0=.05, lambda_max=.05, Lip_grad_max=5, augmentation_scaling=.02, domain_dim=50, image_dim=51, MAXIT=100000)
ADM_0_linfty_h2.run()
ADM_0_linfty_h2.show()
