
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='AvP', accelerator_name='GenericAccelerator', sensors=10, noise=True)
sourceExp.run()
sourceExp.show()
