
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='DRAP', lambda_0=0.25, lambda_max=0.25, sensors=10)
sourceExp.run()
sourceExp.show()
