import pdb
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='DRl', lambda_0=0.5, lambda_max=0.5, anim=True)
JWST.run()
JWST.show()
