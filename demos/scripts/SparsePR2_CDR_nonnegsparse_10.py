
import SetProxPythonPath
from proxtoolbox.experiments.phase.Sparse2_Experiment import Sparse2_Experiment

Sparse2 = Sparse2_Experiment(algorithm='CDRl', lambda_0=1.0, lambda_max=1.0, TOL=5e-4)
Sparse2.run()
Sparse2.show()
