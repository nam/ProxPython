 /home/russell/git/ProxPython/demos/NoLips_ADM_0_linfty_h2_quadratic.py 
***********************************
*    Running a convex instance    *
***********************************
Running ADM on randomExperiment...
Took 118445 iterations and 249.85944199562073 seconds.

Takes longer to find the region of local convergence, but once it finds it, 
generally convergence is faster than:

 /home/russell/git/ProxPython/demos/NoLips_ADM_0_linfty_h2_linear.py 
***********************************
*    Running a convex instance    *
***********************************
Running ADM on randomExperiment...
Took 81642 iterations and 50.65134406089783 seconds.

/home/russell/git/ProxPython/demos/NoLips_ADM_0_linfty_h42_linear.py 
***********************************
*    Running a convex instance    *
***********************************
Running ADM on randomExperiment...
Took 96355 iterations and 64.33213472366333 seconds.

