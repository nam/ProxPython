
import SetProxPythonPath
from proxtoolbox.experiments.phase.SIRIUS_CDI_Experiment import SIRIUS_CDI_Experiment

CDI = SIRIUS_CDI_Experiment(algorithm = 'DRl', Nx = 256, Ny = 256, 
            lambda_0 = 0.9, lambda_max = 0.5)
CDI.run()
CDI.show()
