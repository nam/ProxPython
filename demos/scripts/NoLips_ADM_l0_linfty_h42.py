
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_l0_l1_h42 = randomExperiment(algorithm='ADM', domain_objective= 'l0', image_objective= 'linfty', Bregman_potential='h_4_2', lambda_0=.05, lambda_max=.05, Lip_grad_max=5, augmentation_scaling=0.2, image_dim=51, MAXIT=10000)
ADM_l0_l1_h42.run()
ADM_l0_l1_h42.show()
