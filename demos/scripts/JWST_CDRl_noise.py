
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='CDRl', formulation='cyclic', 
                       lambda_0=0.25, lambda_max=0.25, 
                       noise=True, rotate=True)
JWST.run()
JWST.show()
