
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDI_Experiment import CDI_Experiment

CDI = CDI_Experiment(algorithm = 'CP', MAXIT = 5000)
CDI.run()
CDI.show()
