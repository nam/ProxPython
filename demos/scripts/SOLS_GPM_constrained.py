import SetProxPythonPath
from proxtoolbox.experiments.BM.SumOfLeastSquares_Experiment import SumOfLeastSquares_Experiment

BM = SumOfLeastSquares_Experiment(algorithm='GPM',
                     Nx=8, Ny=12, Nz=50, constBound=20,
                     MAXIT=2000)
BM.run()
BM.show()