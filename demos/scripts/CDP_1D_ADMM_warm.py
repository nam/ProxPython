
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment

# Note: This demo does not converge (it blows up, same with ProxMatlab)

CDP = CDP_Experiment(algorithm='ADMM', constraint='support only',
                     lambda_0=10, lambda_max=10, warmup_iter=50)
CDP.run()
CDP.show()
