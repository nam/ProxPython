
import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_0_linfty_h2_linear = randomExperiment(algorithm='ADM', domain_objective= 'none', image_objective= 'linfty', coupling_function='Square_Quadratic_convex', coupling_approximation='linear', Bregman_potential='l2', lambda_0=.05, lambda_max=.05, Lip_grad_max=15, augmentation_scaling=.008, domain_dim=30, image_dim=40, MAXIT=1000000)
ADM_0_linfty_h2_linear.run()
ADM_0_linfty_h2_linear.show()

# Notes:  augmentatin_scaling .01, very slow linear convergence - linear regime extends over 175000 iterations!   
# augmentation_scaling .05 1600 iterations to convergence, wierd behavior locally
# augmentation_scaling .07 does not converge. 