{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "dc9f3abe",
   "metadata": {},
   "source": [
    "This notebook demonstrates the application of projection algorithms for linear systems of equations.  The concrete linear operator handled here is the discrete radon transform.  The goal is to reconstruct the \"object\"  (a portrait of Carl Friedrich Gauss) from the \"image\" of the object under the radon transform (the sinogram).  The first few cells are meant just to show you the data and the problem that we will solve in the ProxToolbox.  There are a few utilities that we steal from the ProxToolbox, but these are \"stand alone\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4b16de0",
   "metadata": {},
   "source": [
    "First import the necessary packages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "77100bac",
   "metadata": {},
   "outputs": [],
   "source": [
    "import SetProxPythonPath\n",
    "from proxtoolbox.utils.load_image_to_float_array import load_image_to_float_array \n",
    "from proxtoolbox.utils.ZeroPad import ZeroPad \n",
    "from proxtoolbox.utils.Resize import Resize \n",
    "\n",
    "import numpy as np\n",
    "from PIL import Image\n",
    "import matplotlib.pyplot as plt\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ef954e4-8af0-49fe-a008-557949c1ef89",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Usage\n",
    "image_path = '../../InputData/Gauss/Carl_Friedrich_Gauss.jpg'\n",
    "float_array, original_shape = load_image_to_float_array(image_path)\n",
    "\n",
    "img_array = float_array.reshape(original_shape)\n",
    "\n",
    "# Zero pad and resize the image as needed:  \n",
    "# zero pad the array to the nearest square dyad (for FFT)\n",
    "img_array = ZeroPad(img_array)\n",
    "\n",
    "# Flatten the array\n",
    "img_vec = img_array.flatten()\n",
    "\n",
    "print(f\"Image converted to array of {len(img_vec)} float values\")\n",
    "\n",
    "plt.imshow(img_array, cmap='gray')\n",
    "plt.axis('off')\n",
    "plt.show()\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "459e999e",
   "metadata": {},
   "source": [
    "You need to have installed the scikit-image package (pip install scikit-image) to run the code below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3961874c",
   "metadata": {},
   "outputs": [],
   "source": [
    "from skimage.transform import radon"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d7daff6a-43c8-499c-9719-feb84853015a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# `image_scaling` determines the number of x-ray projection angles\n",
    "#  as a factor of the resolution of the object input.  \n",
    "# `image_scaling<1` means that the number of angles is less than \n",
    "# the number of parallel x-ray beams by the factor image_scaling. \n",
    "# When the number of parallel x-ray beams matches the number of \n",
    "# pixels, `image_scaling<1` undersamples the radon transform \n",
    "# and will lead to artifacts in the reconstruction.  This is \n",
    "# mathematically more interesting since there are \n",
    "# many possible solutions to the corresponding linear system. \n",
    "image_scaling = 1\n",
    "theta = np.linspace(0.0, 180.0, round(max(img_array.shape)*image_scaling), endpoint=False)\n",
    "sinogram = radon(img_array, theta=theta)\n",
    "print(sinogram.shape)\n",
    "fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4.5))\n",
    "\n",
    "ax1.set_title(\"Original\")\n",
    "ax1.imshow(img_array, cmap=plt.cm.Greys_r)\n",
    "\n",
    "dx, dy = 0.5 * 180.0 / max(img_array.shape), 0.5 / sinogram.shape[0]\n",
    "ax2.set_title(\"Radon transform\\n(Sinogram)\")\n",
    "ax2.set_xlabel(\"Projection angle (deg)\")\n",
    "ax2.set_ylabel(\"Projection position (pixels)\")\n",
    "ax2.imshow(\n",
    "    sinogram,\n",
    "    cmap=plt.cm.Greys_r,\n",
    "    extent=(-dx, 180.0 + dx, -dy, sinogram.shape[0] + dy),\n",
    "    aspect='auto',\n",
    ")\n",
    "\n",
    "fig.tight_layout()\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1680fdc9",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(sinogram, cmap='gray')\n",
    "plt.axis('off')\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c3cd0ca",
   "metadata": {},
   "source": [
    "For reconstructing the portrait of Gauss from the sinogram (in the image of the discrete radon transform, which is a linear operator),  the code below uses a call to ``sart_projection_update\" provided in the scikit-image package. \n",
    "This function is used to build the projector \"P_sart\" in the \"proxoperators\" folder of the ProxToolbox."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9110faf0",
   "metadata": {},
   "outputs": [],
   "source": [
    "from skimage.transform.radon_transform import order_angles_golden_ratio\n",
    "from skimage.transform._radon_transform import sart_projection_update"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8692556f-783e-42f7-a3db-9a6b7505a4a4",
   "metadata": {},
   "outputs": [],
   "source": [
    "imkwargs = dict(vmin=-0.2, vmax=0.2)\n",
    "dtype = sinogram.dtype\n",
    "\n",
    "img_recon_old = np.zeros_like(img_array, dtype=dtype)    \n",
    "img_recon_new = np.zeros_like(img_recon_old, dtype=dtype)\n",
    "\n",
    "iter=0\n",
    "MAXIT=200\n",
    "change=np.ones(MAXIT+1)\n",
    "TOL=1e-4\n",
    "relaxation=0.5\n",
    "while ((change[iter]>TOL) and (iter<MAXIT)):\n",
    "    iter +=1\n",
    "    img_recon_old=img_recon_new.copy()\n",
    "    img_tmp = img_recon_old.copy()\n",
    "    for angle_index in order_angles_golden_ratio(theta):\n",
    "        neg_grad_sq_dist = sart_projection_update(\n",
    "            img_tmp,\n",
    "            theta[angle_index],\n",
    "            sinogram[:, angle_index],\n",
    "        )\n",
    "        img_tmp += relaxation*neg_grad_sq_dist\n",
    "    img_recon_new = img_tmp\n",
    "    # img_recon_new = iradon_sart(sinogram, theta=theta, image=img_recon_old.copy())\n",
    "    error = img_recon_old - img_recon_new\n",
    "    change[iter] = np.linalg.norm(error)\n",
    "    print(iter)\n",
    "    print(change[iter])\n",
    "\n",
    "print(\n",
    "    f'SART step change: ' f'{change[iter]:.15f}'\n",
    ")\n",
    "print(\n",
    "    f'SART rms reconstruction error: ' f'{np.sqrt(np.mean(error**2)):.15f}'\n",
    ")\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13dabf30",
   "metadata": {},
   "source": [
    "Next we view the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f5f9799-d120-457b-8b55-b19e2ae662e9",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "fig, axes = plt.subplots(1, 2, figsize=(8, 8.5), sharex=True, sharey=True)\n",
    "ax = axes.ravel()\n",
    "\n",
    "ax[0].set_title(\"Reconstruction\\nSART\")\n",
    "ax[0].imshow(img_recon_old, cmap=plt.cm.Greys_r)\n",
    "\n",
    "ax[1].set_title(\"Reconstruction error\\nSART\")\n",
    "ax[1].imshow(img_recon_new-img_recon_old, cmap=plt.cm.Greys_r)\n",
    "plt.figure(figsize=(10, 6))\n",
    "x = np.linspace(0, iter, iter-1)\n",
    "plt.semilogy(x, change[1:iter])\n",
    "plt.title('Reconstruction error\\nSART')\n",
    "plt.xlabel('iteration')\n",
    "plt.ylabel('norm of change')\n",
    "plt.grid(True)\n",
    "plt.show()\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9051176b",
   "metadata": {},
   "source": [
    "Now we implement this within the ProxToolbox environment. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a324a969",
   "metadata": {},
   "source": [
    "The class \"Gauss_Experiment\" in the next cell has a number of parameters that one can choose. \n",
    "\n",
    "- \"algorithm\" is self-explanatory.  The first cell runs the \n",
    "   cyclic projections algorithm \"CP\" found in the \"algorithms\" folder.  This just runs cyclic projections with the projector \n",
    "   \"P_sart\" (found in the \"proxoperators\" folder) applied to each angular rotation.  If the discretization of Gauss' portrait is too coarse relative to  the discretization of the parallel x-ray beam, then P_sart will\n",
    "   not truely be a projection, but the explanation of this\n",
    "   is too lengthy to get into here. \n",
    "- \"formulation\" tells ProxToolbox whether the model is \n",
    "   on the \"product space\" or not; at the moment \"cyclic\" \n",
    "   means \"not product space\".  \n",
    "- \"image_scaling\" determines the number of x-ray projection angles\n",
    "   as a factor of the resolution of the object input.  image_scaling<1 means that the number of angles is less than the number of parallel x-ray beams by the factor image_scaling. \n",
    "- \"MAXIT\" and \"TOL\" are self-explanatory\n",
    "\n",
    "   "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7137b67f",
   "metadata": {},
   "source": [
    "First we import the proxtoolbox experiment Gauss_Experiment"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "356d3c20",
   "metadata": {},
   "outputs": [],
   "source": [
    "import SetProxPythonPath\n",
    "from proxtoolbox.experiments.Gauss.Gauss_Experiment import Gauss_Experiment\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b7f8179",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "Gauss = Gauss_Experiment(algorithm='CP', formulation ='cyclic', \n",
    "                   image_scaling=0.1, \n",
    "                   MAXIT=200, TOL=1e-4\n",
    "                   )\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bb9e39ce",
   "metadata": {},
   "outputs": [],
   "source": [
    "Gauss.run()\n",
    "# Gauss.animate(Gauss.algorithm)\n",
    "Gauss.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48406877",
   "metadata": {},
   "source": [
    "The next example shows what happens when we add a nonnegativity and support constraint set to the feasibility model.  The algorithm does not change;  we have only added a \"qualitative\" support constraint to the model.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8304731b",
   "metadata": {},
   "outputs": [],
   "source": [
    "Gauss = Gauss_Experiment(algorithm='CP', formulation ='cyclic', \n",
    "                   constraint='nonnegative and support', \n",
    "                   image_scaling=0.1, MAXIT=200, TOL=1e-4\n",
    "                   )\n",
    "Gauss.run()\n",
    "# Gauss.animate(Gauss.algorithm)\n",
    "Gauss.show() "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "794bbffa",
   "metadata": {},
   "source": [
    "Next we compare the cyclic projections algorithm above with the averaged projections algorithm.  To switch algorithms in the Gauss_Experiment class, just change the parameter value \"algorithm\" to point to the \"AvP.py\" algorithm in the \"algorithms\" folder.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54aaff8f",
   "metadata": {},
   "outputs": [],
   "source": [
    "Gauss = Gauss_Experiment(algorithm='AvP', formulation ='cyclic', \n",
    "                   image_scaling=0.1, MAXIT=200, TOL=1e-4\n",
    "                   )\n",
    "Gauss.run()\n",
    "# Gauss.animate(Gauss.algorithm)\n",
    "Gauss.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84817567",
   "metadata": {},
   "source": [
    "Averaged projections is equivalent to cyclic/alternating projections on the \"product space\".  The next cells demonstrate this. (Though there seems to be a scaling factor problem with the iterate monitor.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8af13dc5",
   "metadata": {},
   "outputs": [],
   "source": [
    "Gauss = Gauss_Experiment(algorithm='AvP', formulation ='product space', \n",
    "                   image_scaling=0.1, MAXIT=200, TOL=1e-4\n",
    "                   )\n",
    "Gauss.run()\n",
    "# Gauss.animate(Gauss.algorithm)\n",
    "Gauss.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "102919ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "Gauss = Gauss_Experiment(algorithm='CP', formulation ='product space', \n",
    "                   image_scaling=0.1, MAXIT=200, TOL=1e-4\n",
    "                   )\n",
    "Gauss.run()\n",
    "# Gauss.animate(Gauss.algorithm)\n",
    "Gauss.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27ba82f5",
   "metadata": {},
   "source": [
    "It is easy to experiment with different projection-based algorithms.  The cell below applies the relaxed Douglas-Rachford algorithm, \"DRl.py\" also found in the \"algorithms\" folder.  The parameters ``lambda_0\" and ``Lambda_max\" are initial and ending values of the relaxation parameter $\\lambda$ in the DR $\\lambda$ algorithm.  (The algorithm lets this parameter change from $\\lambda_0$ to $\\lambda_{max}$ dynamically as a function of the iteration.  This is practical, but theoretically not interesting.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9e30d815",
   "metadata": {},
   "outputs": [],
   "source": [
    "Gauss = Gauss_Experiment(algorithm='DRl', formulation ='product space', \n",
    "                   lambda_0=0.5, lambda_max=0.5, image_scaling=0.1, MAXIT=200, TOL=1e-4\n",
    "                   )\n",
    "Gauss.run()\n",
    "# Gauss.animate(Gauss.algorithm)\n",
    "Gauss.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a9c2eef",
   "metadata": {},
   "source": [
    "The next cell tries to run an algorithm, stochastic cyclic projections,  that has not been added to the \"algorithms\" folder.  Your task is to write this algorithm and add it to the ProxToolbox."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10a0fabf",
   "metadata": {},
   "outputs": [],
   "source": [
    "Gauss = Gauss_Experiment(algorithm='StochCP', formulation ='cyclic', \n",
    "                   image_scaling=0.1, MAXIT=200, TOL=1e-15\n",
    "                   )\n",
    "Gauss.run()\n",
    "# Gauss.animate(Gauss.algorithm)\n",
    "Gauss.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50510d54",
   "metadata": {},
   "source": [
    "The algorithm \"QNAvP\" is a quasi-Newton acceleration of the averaged projections algorithm and uses the \"samsara\" nonlinear optimization package (pip install samsara). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3be40766",
   "metadata": {},
   "outputs": [],
   "source": [
    "Gauss = Gauss_Experiment(algorithm='QNAvP', formulation ='cyclic', \n",
    "                   image_scaling=0.1, MAXIT=200, TOL=1e-15\n",
    "                   )\n",
    "Gauss.run()\n",
    "# Gauss.animate(Gauss.algorithm)\n",
    "Gauss.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "proxtoolbox_env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
