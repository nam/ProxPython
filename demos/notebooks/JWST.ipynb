{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9727165b",
   "metadata": {},
   "source": [
    "                           JWST Demonstration\n",
    "\n",
    "This notebook demonstrates the application of projection algorithms to an instance of phase retrival.  The concrete phase retrieval problem handled here is wavefront reconstruction for a prototype of the James Webb Space Telescope, as described in ​ \n",
    "\n",
    "​Optical wavefront reconstruction: Theory and numerical methods​.  \n",
    "Luke, D. R. ; Burke, J. V. & Lyon, R. G.​ (2002) \n",
    "SIAM Review, 44(2).​ \n",
    "DOI: https://doi.org/10.1137/S003614450139075  \n",
    "\n",
    "The goal is to reconstruct the complex-valued wavefront at the aperture of the telescope from obervations of a star that is a good approximation to a point source under different defocus settings.  The measurements are  the amplitude of the wave after it has propagated through the telescope to the imaging plane of the telescope. The model for the propagation of the wave is an aberrated Fraunhofer (aka Fourier) transform of the wave at the aperture of the telescope.  The unknown quantity in this problem is the aberration.  The aberration accounts for misalignments in the telecope that lead to less-than-perfect focus.  Once the aberration is known, a deformable mirror is used to correct for the imperfections in the wavefront.  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a58f1dd",
   "metadata": {},
   "source": [
    "There are four constraint sets in this problem: 1. the constraint that the complex-valued wavefront has unit amplitude across the aperture support; 2-4 the intensity measurements of the ``pointspread\" of the telescope at 3 different (known) focus settings.  The first demonstration below shows how simple cyclic projections performs on this problem.   "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97686ab1",
   "metadata": {},
   "source": [
    "First, import the packages. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f3b78b96",
   "metadata": {},
   "outputs": [],
   "source": [
    "import SetProxPythonPath\n",
    "from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "663371b9",
   "metadata": {},
   "source": [
    "The class \"JWST_Experiment\" in the next cell has a number of parameters that one can choose. \n",
    "\n",
    "- \"algorithm\" is self-explanatory.  The first cell runs the \n",
    "   cyclic projections algorithm \"CP\" found in the \"algorithms\" folder.  This just runs cyclic projections with the projector \n",
    "   \"Approx_Pphase_FreFra_Poisson\" (found in the \"proxoperators\" folder) applied to each data measurement at different settings (in particular defocus) of the instrument.  We use an approximate formulation of the projection onto the phase data to avoid instabilities due to division.  This is described in detail in the article cited above. \n",
    "- \"formulation\" tells ProxToolbox whether the model is \n",
    "   on the \"product space\" or not; at the moment \"cyclic\" \n",
    "   means \"not product space\".  \n",
    "- \"MAXIT\" and \"TOL\" are self-explanatory\n",
    "- \"rotate\" rotates the iterates to a fixed rotation.  This is needed for cyclic projections. \n",
    "- \"noise\" adds noise to the data (more realistic, but the noise in this experiment is much bigger than in practice)\n",
    "- \"anim\" pushes every second iterate to a graphics display.  Does not work in the notebook, but does work as a script run from the commandline prompt.  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b7f8179",
   "metadata": {},
   "outputs": [],
   "source": [
    "JWST = JWST_Experiment(algorithm='CP', formulation='cyclic', MAXIT=1000,\n",
    "                       TOL=5e-15, rotate=False, noise=True, anim=True)\n",
    "JWST.show_data()\n",
    "JWST.run()\n",
    "JWST.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c7456e91",
   "metadata": {},
   "source": [
    "The next demonstration shows how averaged projections performs. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1a14c331",
   "metadata": {},
   "outputs": [],
   "source": [
    "JWST = JWST_Experiment(algorithm='AvP', formulation='cyclic', MAXIT=1000,\n",
    "                       TOL=5e-15, rotate=False, noise=True, anim=True)\n",
    "JWST.run()\n",
    "JWST.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebb26031",
   "metadata": {},
   "source": [
    "Averaged projections is just steepest descents, without step length optimization, for minimizing the sum of squared distances to the constraint sets.  With this interpretation it is easy to apply limited memory BFGS quasi-Newton method, \"QNAvP\".  This calls the \"samsara\" package (pip install samsara).   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e3f13ac2",
   "metadata": {},
   "outputs": [],
   "source": [
    "JWST = JWST_Experiment(algorithm='QNAvP', formulation='cyclic', MAXIT=1000,\n",
    "                       TOL=5e-15, rotate=False, noise=True, anim=True)\n",
    "JWST.run()\n",
    "JWST.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2b668047",
   "metadata": {},
   "source": [
    "This problem is an inconsistent feasibility problem, so the Douglas-Rachford method demonstrated below will not converge. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9cf3474a",
   "metadata": {},
   "outputs": [],
   "source": [
    "JWST = JWST_Experiment(algorithm='DR', formulation='product space', MAXIT=1000,\n",
    "                       TOL=5e-15, rotate=False, noise=True, anim=True)\n",
    "JWST.run()\n",
    "JWST.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d52dca6c",
   "metadata": {},
   "source": [
    "To stablilize the  Douglas-Rachford method, a simple relaxation, DR $\\lambda$, converges for $\\lambda\\in (0,1]$ small enough. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a79e2ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "JWST = JWST_Experiment(algorithm='DRl', formulation='product space', \n",
    "                       lambda_0=0.5, lambda_max=0.5, MAXIT=1000,\n",
    "                       TOL=5e-15, rotate=False, noise=True, anim=False)\n",
    "JWST.run()\n",
    "JWST.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f7da216",
   "metadata": {},
   "source": [
    "Douglas-Rachford and DR $\\lambda$ only make sense in the context of feasibility with just two sets.  The demonstrations above formulated this problem as a two-set feasibilitiy problem in the product space, where one of the sets is the \"diagonal\" of the product space and the other set is the collection of the 4 constraints arranged in the product space.  A cyclic version of these methods can be employed without using the product space trick. The demonstration below implements the cyclic DR $\\lambda$ method studied in \n",
    "\n",
    "​Optimization on Spheres: Models and Proximal Algorithms with Computational Performance Comparisons​. \n",
    "Luke, D. R. ; Sabach, S. & Teboulle, M.​ (2019) \n",
    "SIAM Journal on Mathematics of Data Science, 1(3) pp. 408​-445​.​\n",
    "DOI: https://doi.org/10.1137/18M1193025  \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "707c60c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "JWST = JWST_Experiment(algorithm='CDRl', formulation='cyclic', \n",
    "                       lambda_0=0.25, lambda_max=0.25, MAXIT=1000,\n",
    "                       TOL=5e-15, rotate=True, noise=True, anim=False)\n",
    "JWST.run()\n",
    "JWST.show()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76649a74",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "proxtoolbox_env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
