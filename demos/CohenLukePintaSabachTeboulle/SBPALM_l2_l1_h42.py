# Driver for the numerical example shown in Figure 1 of 
# ``A Semi-Bregman Proximal Alternating Method for a Class of
# Nonconvex Problems: Local and Global Convergence Analysis"
# Cohen, Luke, Pinta, Sabach and Teboulle
# Code written and verified by Russell Luke, 2023

import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_l1_l1_h42 = randomExperiment(algorithm='ADM', domain_objective='l2', image_objective= 'l1', Bregman_potential='h_4',lambda_0=.5, lambda_max=.5, Lip_grad_max=1, augmentation_scaling=.001, image_dim=51, MAXIT=100000)
ADM_l1_l1_h42.run()
ADM_l1_l1_h42.show()

