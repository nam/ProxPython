# Numerical Experiments for ``A Semi-Bregman Proximal Alternating Method for a Class of Nonconvex Problems: Local and Global Convergence Analysis"

[[_TOC_]]

This folder contains the numerical experiments presented in 
E. Cohen, D. R. Luke, T. Pinta, S. Sabach, and M. Teboulle,
``A Semi-Bregman Proximal Alternating Method for a Class of
Nonconvex Problems: Local and Global Convergence Analysis".  Journal 
of Global Optimization, 2023.  

## Acknowledgements

**Main Author**: Russell Luke, University of Göttingen


**Funding**:

* German DFG grant LU 170211-1


# Getting started

Before you do anything, you need to load the following python packages:
        * numpy
        * matplotlib
        * iamgeio
        * h5py
        * urllib3 
        * numba 
        * scikitimage 
        * scikitlearn


