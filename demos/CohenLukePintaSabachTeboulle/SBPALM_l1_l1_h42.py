# Driver for the numerical example shown in Figure 2 of 
# ``A Semi-Bregman Proximal Alternating Method for a Class of
# Nonconvex Problems: Local and Global Convergence Analysis"
# Cohen, Luke, Pinta, Sabach and Teboulle
# Code written and verified by Russell Luke, 2023

import SetProxPythonPath
from proxtoolbox.experiments.NoLips.randomExperiment import randomExperiment

ADM_l1_l1_h42 = randomExperiment(algorithm='ADM', domain_objective='l1', image_objective='l1', coupling_function= 'Square_Quadratic_convex', Bregman_potential='h_4', lambda_0=.2, lambda_max=.2, Lip_grad_max=.5, augmentation_scaling=.02, image_dim=51, MAXIT=10000)
ADM_l1_l1_h42.run()
ADM_l1_l1_h42.show()

