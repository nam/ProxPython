using Distributed, ClusterManagers, LinearAlgebra, Statistics, Rotations, ProgressMeter, Random, JLD2, StaticArrays, Dates, FileIO, DelimitedFiles, ArgParse, Glob, StatsBase, ThreadsX

import NPhotons as nph

const starttime = now()

function printlog(x...)
    println(now(), " ", join(x, " ")...)
    flush(stdout)
end

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table! s begin
        "opath"
            required = true
        "fpath"
            required = true
        "outpath"
            required = true
        "--stride", "-s"
            arg_type = Int
            default = 1000
        "--ntries", "-n"
            arg_type = Int
            default = 1000
        "--avrange", "-r"
            arg_type = Float64
            default = 0.0
        "--averagen", "-a"
            arg_type = Int
            default = 10
        
    end

    return parse_args(s)
end

const psws = nph.lebedevGrid(precision = 23)

function main()
    args = parse_commandline()

    println(args["opath"])
    println(args["fpath"])
    println(args["outpath"])

    @load args["opath"] objs
    @load "out/"*args["fpath"] state
    @load args["fpath"] kwidth
    fs, ws = state.configuration

    if args["avrange"] != 0.0
        avstart = length(state.history) * (1 - args["avrange"])
        avinds = round.(Int, range(avstart, length(state.history), length = args["averagen"]))
        fs = mean(state.history[avinds])
    end

    testobjs = nph.smoothen.(objs[1:args["stride"]:end], kwidth);
    testobjs = testobjs[1:(length(testobjs) ÷ length(fs))*length(fs)]
    
    rngs = [MersenneTwister() for i in 1:Threads.nthreads()]
    
    prog = Progress(length(fs) * length(testobjs))
    vals = ThreadsX.map(Iterators.product(fs, testobjs)) do (f, o)
        next!(prog)
        alf = nph.aligned(f, o, ntries = args["ntries"], rng = rngs[Threads.threadid()])
        nph.fscResolution(alf, o, psws..., rstep = 0.01)
    end

    refobjs = testobjs[getindex.(argmin(vals, dims = 2)[:], 2)];
    alfs = nph.aligned.(fs, refobjs, ntries = 10000);

    @save args["outpath"] alfs ws refobjs vals testobjs
end

main()