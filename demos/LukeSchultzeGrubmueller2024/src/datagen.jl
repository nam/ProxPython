ENV["GKS_ENCODING"] = "utf-8"

using LinearAlgebra, Statistics, Rotations, ProgressMeter, Random, JLD2, BenchmarkTools, StaticArrays, Dates, FileIO, Colors, NearestNeighbors, Distances, Distributions, FileWatching, Combinatorics, Dates, StatsBase, DelimitedFiles, DataStructures, ArgParse, ThreadsX

using CUDA
CUDA.allowscalar(false)

using NPhotons
const nph = NPhotons;

function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "pdb"
            required = true
        "--nimages", "-n"
            arg_type = Int
            default = 10000
        "--nfiles", "-f"
            arg_type = Int
            default = 1
        "--showprogress", "-p"
            action = :store_true
    end
    return parse_args(s)
end
const args = parse_commandline()
const showprogress = args["showprogress"]

# mkpath(args["pdb"])

parampath = "$(args["pdb"])/params.jld2"
if isfile(parampath)
    @load parampath objs N wavelength
end

starttime = now()

const gens = [nph.imageGenerator(N, o, wavelength = wavelength, rng = MersenneTwister()) for o in objs]

for i in 1:args["nfiles"]
    prog = Progress(args["nimages"])

    function mapfunc(i)
        showprogress && next!(prog)
        rand(gens)()
    end

    images = if Threads.nthreads() == 1
        map(mapfunc, 1:args["nimages"])
    else
        ThreadsX.map(mapfunc, 1:args["nimages"])
    end

    path = nph.enumpath("$(args["pdb"])/$(args["pdb"])_$(args["nimages"])_λ=$(wavelength).jld2")
    nph.saveImages(path, images)

    eta = round(Int, (now() - starttime).value * (args["nfiles"] / i - 1) / 60000)
    println("Saved to $(path).  ETA: $(eta ÷ 60):$(lpad(eta % 60, 2, '0'))")
end