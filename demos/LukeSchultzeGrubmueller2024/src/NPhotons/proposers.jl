struct NormalProposer

end

function neighbor(proposer::NormalProposer, f, d)
    f .+ d .* randn.(eltype(f))
end

function logprop(proposer::NormalProposer, f_to, f_from, d)\
    mapreduce(+, f_to, f_from) do x, y
        norm(x - y)^2 / (2d^2)
    end
end


struct SmoothProposer
    σ::Float64

    function SmoothProposer(σ = 1.0)
        new(σ)
    end
end

function neighbor(proposer::SmoothProposer, f, d)
    M = exp.(-1/(2*proposer.σ^2) .* pairwise(Euclidean(), f.positions).^2)
    M .*= cbrt.(sum(M, dims = 2))
    vs = randn(SVector{3, Float64}, length(f))
    newf = deepcopy(f)
    newf.positions .+= d * norm(vs) .* normalize!(M * vs)
    newf.positions .+= 0.1d .* randn.(SVector{3, Float64})
    newf.positions .-= Ref(mean(newf.positions))
    newf
end

function logprop(proposer::SmoothProposer, f_to, f_from, d)
    0.0
end


struct StructureProposer{T}
    gnormals::T

    function StructureProposer(g, σ = 0.0)
        gnormals = [MvNormal(g.positions[i], sqrt(g.widths[i]^2 + σ^2)) for i in 1:length(g)]
        new{typeof(gnormals)}(gnormals)
    end
end

function StructureProposer(w::Real, σ = 0.0)
    StructureProposer(AtomVolume(1, w))
end

function neighbor(proposer::StructureProposer, f, d)
    fnormals = [proposer.gnormals[ceil(Int, length(proposer.gnormals) / length(f) * i)] for i in 1:length(f)]
    normals = [normalprod(fnormals[i], MvNormal(f.positions[i], d)) for i in 1:length(f)]

    newf = deepcopy(f)
    newf.positions .= [rand(n) for n in normals]
    newf.positions .-= Ref(mean(newf.positions))
    
    newf
end

function logprop(proposer::StructureProposer, f_to, f_from, d)
    fnormals = [proposer.gnormals[ceil(Int, length(proposer.gnormals) / length(f_from) * i)] for i in 1:length(f_from)]
    normals = [normalprod(fnormals[i], MvNormal(f_from.positions[i], d)) for i in 1:length(f_from)]

    sum(log.(pdf.(normals, f_to.positions)))
end

struct CombinedProposer{T}
    proposers::T
end

CombinedProposer(proposers...) = CombinedProposer(proposers)

function neighbor(proposer::CombinedProposer, x, d)
    y = x
    for p in proposer.proposers
        y = neighbor(p, y, d)
    end
    y
end

function logprop(proposer::CombinedProposer, x, y, d)
    sum(logprop(p, x, y, d) for p in proposer.proposers)
end


struct MultiProposer{T}
    proposers::T
end

function neighbor(proposer::MultiProposer, fs, d)
    [neighbor(p, f, d) for (p, f) in zip(proposer.proposers, fs)]
end

function logprop(proposer::MultiProposer, fs, gs, d)
    sum(logprop(p, f, g, d) for (p, f, g) in zip(proposer.proposers, fs, gs))
end


struct GibbsProposer{T}
    proposer::T
    i::Int
end

function neighbor(proposer::GibbsProposer, f, d)
    @set f[proposer.i] = neighbor(proposer.proposer, f[proposer.i], d)
end

function logprop(proposer::GibbsProposer, f, g, d)
    logprop(proposer.proposer, f[proposer.i], g[proposer.i], d)
end


struct WeightProposer{T}
    prior::T
end

# function neighbor(proposer::WeightProposer, ws, d)
#     # d = min(d, 2std(proposer.prior))
#     wprior(ws) = prod(pdf.(proposer.prior, ws))
#     for i in 1:100000
#         news = ws .+ d .* (x = randn(length(ws)); x .- mean(x))
#         if all(0 .< news .< 1) && rand() < wprior(news) / wprior(ws)
#             ws = news
#             break
#         end
#     end
#     normalize!(ws, 1)
#     ws = clamp.(ws, 0, 1)
#     ws
# end

function metropolisgamma(x, d)
    while true
        y = x * exp(randn() * d)
        if rand() < exp(-y) / exp(-x) * y / x
            return y
        end
    end
end

function neighbor(proposer::WeightProposer, ws, d)
    wprior(ws) = prod(pdf.(proposer.prior, ws ./ sum(ws)))

    news = metropolisgamma.(ws, d)
    # news = [rand(Gamma(1 / d^2, w * d^2)) for w in ws]
    if rand() < wprior(news) / wprior(ws)
        ws = news
    end
    ws
end

function logprop(proposer::WeightProposer, fws, gws, d)
    wprior(ws) = prod(pdf.(proposer.prior,  ws ./ sum(ws)))
    log(wprior(fws)) - log(wprior(gws))
end

struct WidthProposer
    d::Float64
end

function neighbor(proposer::WidthProposer, fs::Array, d)
    newfs = deepcopy(fs)
    rd = randn() .* proposer.d
    for newf in newfs
        newf.widths .+= rd
        newf.widths .= clamp.(newf.widths, 1e-2, Inf)
    end
    newfs
end

function neighbor(proposer::WidthProposer, f, d)
    newf = deepcopy(f)
    rd = randn() .* proposer.d
    newf.widths .+= rd
    newf.widths .= clamp.(newf.widths, 1e-2, Inf)
    newf
end


function logprop(proposer::WidthProposer, f, g, d)
    0.0
end

const WiP = WidthProposer
const WeP = WeightProposer
const GP = GibbsProposer
const MP = MultiProposer
const SP = StructureProposer
const NP = NormalProposer
const CP = CombinedProposer
