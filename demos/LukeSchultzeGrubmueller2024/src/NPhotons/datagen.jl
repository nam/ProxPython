function imageGenerator(intensity::Real, f, fastf; rng = Random.GLOBAL_RNG, upperbound = r -> Inf, wavelength = 2.5, maxr = Inf, incoming = Normal(1, 0), pd = k -> 1)
    envf = envelope(f)
    ewaldradius = 2pi / wavelength
    ewaldarea = 4pi * ewaldradius^2

    maxrvalue = Float64(f(SA[0.0,0.0,0.0]))

    function generateImage(; R = nothing, N = nothing)
        if isnothing(R)
            R = randomRotation(rng)
        end

        image = SVector{3, Float64}[]
        
        for j in 1:rand(rng, Poisson(ifelse(isnothing(N), intensity, N) * maxrvalue * ewaldarea * rand(incoming)))
            rvalue = rand(rng) * maxrvalue

            p = ewaldradius .* (normalize(randn(rng, SVector{3, Float32})) .- SA[0, 0, 1])
            r = norm(p)
            
            if r > maxr || rvalue > envf(r) || rvalue > upperbound(r)
                continue
            end

            value = fastf(R * p)
            
            if value > upperbound(r)
                throw(ArgumentError("$value > $(upperbound(r))"))
            end

            if rvalue < value * pd(p)
                push!(image, p)
            end
        end
        return image
    end

    generateImage
end

function imageGenerator(intensity::Real, f; kwargs...)
    imageGenerator(intensity, f, f; kwargs...)
end

function generateImages(N::Int, intensity::Real, f; kwargs...)
    generateImages(N, intensity, f, f; kwargs...)
end

function generateImages(N::Int, intensity::Real, f, fastf; kwargs...)
    generateImages(N, intensity, [f], [fastf], [1.0]; kwargs...)
end

function generateImages(nimages::Int, intensity::Real, fs::AbstractArray, ws::AbstractArray; kwargs...)
    generateImages(nimages, intensity, fs, fs, ws; kwargs...)
end

function generateImages(nimages::Int, intensity::Real, fsws::Tuple; kwargs...)
    generateImages(nimages, intensity, fsws...; kwargs...)
end

function generateImages(nimages::Int, intensity::Real, fs::AbstractArray, fastfs::AbstractArray, ws::AbstractArray; showprogress = true, kwargs...)
    gens = imageGenerator.(intensity, fs, fastfs; kwargs...)

    prog = Progress(nimages)
    map(1:nimages) do i
        showprogress && next!(prog)
        sample(gens, Weights(ws))()
    end
end


function generateImagesThreaded(N::Int, intensity::Real, f; kwargs...)
    generateImagesThreaded(N, intensity, f, f; kwargs...)
end

function generateImagesThreaded(N::Int, intensity::Real, f, fastf; showprogress = true, kwargs...)
    generators = [imageGenerator(intensity, f, fastf; rng = MersenneTwister(), kwargs...) for i in 1:Threads.nthreads()]
    images = Vector{Vector{SVector{3, Float64}}}(undef, N)

    prog = Progress(N)
    Threads.@threads for i in 1:N
        id = Threads.threadid()
        images[i] = generators[id]()
        showprogress && next!(prog)
    end

    return images
end




function radialAverage(f::AtomVolume, r)
    result = 0.0
    for (p1, h1, σ1) in zip(f.positions, f.heights, f.widths)
        for (p2, h2, σ2) in zip(f.positions, f.heights, f.widths)
            result += h1 * h2 * sinc(r * norm(p1 - p2) / pi) * exp(-(σ1^2 + σ2^2) * r^2 / 2)
        end
    end
    result
end

function radialAverage(V::MixtureVolume, r)
    sum(radialAverage(f, r) for f in V.fs)
end

function scatteringProbability(V::MixtureVolume; kwargs...)
    sum(scatteringProbability(f; kwargs...) for f in V.fs)
end


function smoothingKernel(σ)
    kernel = AtomVolume([SA[0.0,0.0,0.0]], [1.0], [σ], 1.0)
    r -> kernel(SA[r,0.0,0.0])
end
avKernel(σ) = smoothingKernel(σ)

function smoothingFilter(σ)
    kernel = smoothingKernel(σ)
    image -> [p for p in image if kernel(norm(p)) > rand()]
end

function smoothenImages(images, σ)
    smoothingFilter(σ).(images)
end

function smoothenImage(images, σ)
    smoothingFilter(σ)(images)
end



function selectImages(images, condition; nselect = Inf, radius = Inf, total = 0)
    selected = Vector{SVector{3, Float64}}[]

    if total == 0
        total = try
            length(images)
        catch e
            10^6 * length(images.it)
        end
    end
    
    prog = Progress(total; showspeed=true)
    showvalues(selected, ntested) = () -> [(:selected, "$(digitsep(length(selected))) = $(round(100length(selected)/ntested, sigdigits=3))%"), (:total, digitsep(ntested))]
   
    for (ntested, image) in enumerate(images)
        image = filter(x -> norm(x) < radius, image)
        !isempty(image) && condition(image) && push!(selected, image)
        if length(selected) >= nselect 
            prog.n = ntested
            ProgressMeter.finish!(prog, showvalues = showvalues(selected, ntested)) 
            break    
        end
        ProgressMeter.next!(prog, showvalues = showvalues(selected, ntested))
    end

    selected
end

function selectImages(images, rmask::Array, nmask::Array; kwargs...)
    function condition(image)
        all(sum(r1 < norm(x) < r2 for x in image) >= n for (n, r1, r2) in zip(nmask, rmask, [rmask[2:end]; Inf]))
    end
    selectImages(images, condition; kwargs...)
end

function selectImages(files::Vector{String}, condition; kwargs...)
    selectImages(Iterators.flatten(imap(loadImages, files)), condition; kwargs...)
end




struct CircleDistance <: Metric
end

function Distances.evaluate(d::CircleDistance, a, b)
    10000 * abs(a[1] - b[1]) + abs(mod(a[2] - b[2] + pi, 2pi) - pi)
end

function roundImagesPolar(points, images; showprogress = false)
    angles = [atan(p[2], p[1]) for p in points]
    radii = norm.(points)

    distances = zeros(size(points)...)

    indexImages = Array{Array{Int,1}}(undef, length(images))

    prog = Progress(length(images), 1)
    for i in 1:length(images)
        indexImage = Int[]
        for (r, a) in images[i]
            distances .= 10000 .* abs.(r .- radii) .+ min.(abs.(a .- angles), abs.(a .- angles .- 2pi))
            push!(indexImage, argmin(distances))
        end
        indexImages[i] = indexImage
        
        showprogress && next!(prog)
    end
    indexImages
end

function roundImagesCircular(points, images, npercircle; showprogress = false)
    angles = [atan(p[2], p[1]) for p in points]
    radii = [mean(norm.(points[i:i+npercircle-1])) for i in 1:npercircle:length(points)]

    distances = zeros(size(points)...)

    indexImages = Array{Array{Int,1}}(undef, length(images))

    prog = Progress(length(images), 1)
    for i in 1:length(images)
        indexImage = Int[]
        for (r, a) in images[i]
            start = npercircle * (argmin(abs.(r .- radii)) - 1)
            as = view(angles, start+1:start+npercircle)
            index = argmin(min.(abs.(a .- as), abs.(a .- as .- 2pi)))
            push!(indexImage, start + index)
        end
        indexImages[i] = indexImage
        
        showprogress && next!(prog)
    end
    indexImages
end

function roundImages(pixels, images)
    tree = KDTree(reduce(hcat, pixels))
    [nn(tree, image)[1] for image in images]
end



function toImageMatrix(indexImages::Array{<:Array{<:Integer, 1}, 1})
    imageMatrix = zeros(eltype(eltype(indexImages)), length(indexImages), maximum(length.(indexImages)) + 1)
    for (i, image) in enumerate(indexImages)
        imageMatrix[i,1] = length(image)
        imageMatrix[i,2:length(image)+1] .= image
    end
    imageMatrix
end

# function toImageMatrix(serializedImages::Array{<:Integer})
#     maxl = 0
#     nimg = 0
#     lastsep = 1
#     for i in eachindex(serializedImages)
#         if serializedImages[i] < 0
#             maxl = max(maxl, i - lastsep - 1)
#             lastsep = i
#             nimg += 1
#         end
#     end

#     imageMatrix = zeros(eltype(serializedImages), nimg, maxl + 1)
#     row = 1
#     col = 2
#     for i in eachindex(serializedImages)
#         if serializedImages[i] < 0
#             imageMatrix[row, 1] = col - 2
#             row += 1
#             col = 2
#             continue
#         end
#         imageMatrix[row, col] = serializedImages[i]
#         col += 1
#     end
#     imageMatrix
# end

# function serializeImages(indexImages)
#     result = copy(indexImages[1])
#     for i in 2:length(indexImages)
#         push!(result, -1)
#         append!(result, indexImages[i])
#     end
#     result
# end

# function deserializeImages(serializedImages)
#     result = [eltype(serializedImages)[]]
#     for i in eachindex(serializedImages)
#         if serializedImages[i] < 0
#             push!(result, eltype(serializedImages)[])
#         else
#             push!(result[end], serializedImages[i])
#         end
#     end
#     result
# end
