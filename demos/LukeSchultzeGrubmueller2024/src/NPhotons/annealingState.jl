import Base.show


mutable struct AnnealingState{T}
    energies::Array{Float64,1}
    nsteps::Int
    stepsize::Float64
    slambda::Float64
    temperature::Float64
    tlambda::Float64
    configuration::Union{Nothing, T}
    history::Vector{T}
    histstep::Int

    function AnnealingState(;energies = [], nsteps = 0, stepsize = 1.0, slambda = 1.01, temperature = 1.0, tlambda = 2^(-1 / 1e3), configuration = nothing, histstep = 10)
        T = ifelse(!isnothing(configuration), typeof(configuration), Any)
        new{T}(energies, nsteps, stepsize, slambda, temperature, tlambda, configuration, [], histstep)
    end
end

function lastEnergy(state::AnnealingState)
    !isempty(state.energies) ? state.energies[end] : Inf
end

function step!(state::AnnealingState, E, configuration = nothing; logpropdiff = 0.0)
    currentE = Inf
    if !isempty(state.energies)
        currentE = state.energies[end]
    end
    deltaE = E - currentE

    state.temperature *= state.tlambda
    state.nsteps += 1
    state.stepsize = clamp(state.stepsize, 1e-10, 10)
    
    if state.nsteps % state.histstep == 0
        push!(state.history, state.configuration)
    end

    if deltaE < 0 || exp(-(deltaE + logpropdiff) / state.temperature) > rand()
        state.stepsize *= state.slambda^3.27
        state.configuration = configuration
        push!(state.energies, E)
        
        return true
    else
        state.stepsize /= state.slambda
        push!(state.energies, currentE)
        return false
    end
end

function anneal!(state::AnnealingState, energy, neighbor; logprop = (f, g, d) -> 0.0, output = s -> true, nsteps = 100, showprogress = true, targetTemp = 0.0, minTemp = 0.0)
    if targetTemp > 0
        nsteps = round(Int, log(state.tlambda, targetTemp / state.temperature))
    end

    prog = Progress(nsteps; showspeed = true)
    start = state.nsteps

    for i in 1:nsteps
        candidate = neighbor(state.configuration, state.stepsize)
        forward = logprop(candidate, state.configuration, state.stepsize)
        backward = logprop(state.configuration, candidate, state.stepsize)
        step!(state, energy(candidate), candidate, logpropdiff = forward - backward)

        state.temperature = clamp(state.temperature, minTemp, Inf)
        
        if showprogress 
            next!(prog, showvalues = [(:stepsize, state.stepsize), (:temperature, state.temperature), (:nsteps, "$(state.nsteps)/$(start + nsteps)"), (:accepted, acceptanceRatio(state)), (:energy, lastEnergy(state))])
        end
        
        if !output(state)
            return false
        end
    end
    return true
end


function show(io::IO, state::AnnealingState)
    print(io, "AnnealingState(nsteps = $(state.nsteps), stepsize = $(state.stepsize), temperature = $(state.temperature))")
end

function acceptanceRatio(state::AnnealingState; n = 1000)
    if length(state.energies) < 2 return NaN end
    if n == 0
        n = length(state.energies) - 1
    end
    n = min(length(state.energies) - 1, n)
    sum(state.energies[end - i] != state.energies[end - i - 1] for i in 0:n - 1) / n
end

function remainingSteps(state; targetTemp = 1.0)
    round(Int, log(state.tlambda, targetTemp / state.temperature))
end

function estimateStartingTemperature(energy, start, neighbor, d; nstates = 100, etol = 1e-3, showprogress = false)
    from = Float64[]
    to = Float64[]

    estart = energy(start)
    f = start
    prog = Progress(100*nstates)
    for i in 1:nstates
        for j in 1:100
            f = neighbor(f, d)
            showprogress && next!(prog)
        end
        e1, e2 = minmax(energy(f), energy(neighbor(f, d)))
        push!(from, e1 - estart)
        push!(to, e2 - estart)
    end

    T = std([from; to])
    for i in 1:1000
        chi = sum(exp.(.-to ./ T)) / sum(exp.(.-from ./ T))
        if isnan(chi)
            T *= 10
            continue
        end
        T *= (log(chi) / log(0.5))^0.5
        abs(chi - 0.5) < etol && break
    end
    T
end





# function anneal!(state::AnnealingState, energy, neighbor, surrogate; logprop = (f) -> 0.0, output = s -> true, nsteps = 100, showprogress = true, targetTemp = 0.0, minTemp = 0.0, ssteps = 10, logintvl = 500)
#     if targetTemp > 0
#         nsteps = round(Int, log(state.tlambda, targetTemp / state.temperature))
#     end

#     prog = Progress(nsteps)

#     accepted = 0
#     rejected = 0
#     acceptedSurrogate = 0
#     rejectedSurrogate = 0
#     history = Bool[]

#     for i in 1:nsteps
#         logpropdiff = 0.0

#         configuration = state.configuration
#         currentAE = surrogate(state.configuration)

#         reject = true
#         for i in 1:ssteps
#             candidate = neighbor(configuration, state.stepsize)
#             forward = logprop(candidate, configuration, state.stepsize)
#             backward = logprop(configuration, candidate, state.stepsize)

#             candidateAE = surrogate(candidate)
#             gforward = min(0, currentAE - candidateAE + forward - backward)
#             gbackward = min(0, candidateAE - currentAE + backward - forward)

#             if rand() < exp(gforward / state.temperature)
#                 # logpropdiff += candidateAE - currentAE
#                 logpropdiff += forward + gforward - backward - gbackward
#                 configuration = candidate
#                 currentAE = candidateAE
#                 reject = false
#                 state.stepsize *= state.slambda
#                 acceptedSurrogate += 1
#             else
#                 # reject = true
#                 rejectedSurrogate += 1
#                 state.stepsize /= state.slambda
#                 # break
#             end
#         end

#         if !reject
#             if step!(state, energy(configuration), configuration, logpropdiff = logpropdiff)
#                 accepted += 1
#                 push!(history, true)
#                 state.stepsize *= state.slambda
#             else
#                 rejected += 1
#                 push!(history, false)
#                 state.stepsize /= state.slambda
#             end
#         else
#             step!(state, Inf, configuration, logpropdiff = logpropdiff)
#         end

#         if i % logintvl == 0
#             println("$(accepted / logintvl), $(rejected / logintvl), $((accepted + rejected) / logintvl), $(acceptedSurrogate / (acceptedSurrogate + rejectedSurrogate)), $(state.stepsize)")
#             accepted = rejected = acceptedSurrogate = rejectedSurrogate = 0
#         end

#         state.temperature = clamp(state.temperature, minTemp, Inf)
#         showprogress && next!(prog)
        
#         if !output(state)
#             return false
#         end
#     end
#     return history
#     return true
# end