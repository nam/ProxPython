import BioStructures

function loadPDBstruc(pdb)
    if occursin(".pdb", pdb)
        return read(pdb, BioStructures.PDB)
    end
    BioStructures.downloadpdb(pdb, dir="pdb/")
    struc = read("pdb/" * pdb * ".pdb", BioStructures.PDB)
end

const atomfactorlist = Dict(
    "C" => (0.7, 6.0),
    "N" => (0.65, 7.0),
    "S" => (1.0, 16.0),
    "O" => (0.6, 8.0),
    "default" => (0.7, 6.0),
    "P" => (0.65, 15.0),
    "H" => (0.3, 1.0),
    "D" => (0.3, 1.0)
)

function element(name)
    string(filter(x -> occursin(x, "CNSOPHD"), name)[1])
end

function atomic_number(name)
    atomfactorlist[element(name)][2]
end

function atomic_radius(name)
    atomfactorlist[element(name)][1]
end


function loadPDB(pdb, selector = BioStructures.standardselector)
    struc = loadPDBstruc(pdb)

    atomfactorlist = Dict(
        "C" => (0.7, 6.0),
        "N" => (0.65, 7.0),
        "S" => (1.0, 16.0),
        "O" => (0.6, 8.0),
        "default" => (0.7, 6.0),
        "P" => (0.65, 15.0),
        "H" => (0.3, 1.0),
        "D" => (0.3, 1.0)
    )
    
    atoms = BioStructures.collectatoms(struc, selector)
    positions = Array{Float64,1}[]
    radii = Float64[]
    masses = Float64[]
    for atom in atoms
        push!(positions, BioStructures.coords(atom))
        push!(radii, atomfactorlist[BioStructures.element(atom)][1])
        push!(masses, atomfactorlist[BioStructures.element(atom)][2])
    end
    # positions .-= Ref(mean(positions));
    return SVector{3, Float64}.(positions), radii, masses
end
