@ECHO OFF

REM Command file for building tutorial latex file from .rst

sphinx-build -b latex Tutorial _build/tutorial

REM could be improved by using pdflatex to generate pdf from latex file

REM previous attempt with rst2pdf extension was not convincing
REM (the generated pdf file was not visually appealing)
REM for reference:
REM sphinx-build -b pdf Tutorial _build/tutorial