

Extending the ProxToolbox
-------------------------

The focus of this section is to describe how to extend the ProxToolbox by
adding new experiments and algorithms.

.. toctree::
   :maxdepth: 1
  
   addingExperiment.rst
   addingAlgorithm.rst
