.. _tutorial:

********
Tutorial
********

This tutorial shows how to use and how to extend the ProxToolbox to
create new experiments and algorithms.

The experiment object models a problem and its associated 
dataset. It is through this object that the various algorithms implemented
in the ProxToolbox are used.

The experiment described in this tutorial is the CDI experiment for which
the dataset is an image set which was produced at the Institute for X-ray
Physics at the University of Göttingen.


.. toctree::
   :maxdepth: 1
  
   howtouse
   howtodevelop
   

