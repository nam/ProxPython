This folder contains a Julia package for molecular electronic structure determination from single molecule X-ray scattering using a stochastic maximum likelihood approach. The code was used to generate the numerical experiments reported in ``Stochastic Algorithms for Large-Scale Composite Optimization: the Case of Single-Shot X-FEL Imaging"
D. R. Luke, S. Schultze, H. Grubmüller https://arxiv.org/abs/2401.13454.

This is not meant for out-of-the-box usage yet, but a short demonstration is included (`scripts/LSG_demo.ipynb`, and `scripts/LSG_demo.jl`). Please note that these contain code specific to our compute cluster and might not work in your environment. 

Requirements
=============

* A CUDA-capable gpu (compute capability 3.5 or higher) with support for CUDA 11.0 or higher
* Julia
* The packages listed in `dependency_versions.txt`

Installation
=============

Install [Julia](https://julialang.org) and [IJulia](https://github.com/JuliaLang/IJulia.jl) according to the instructions. All our computations were done using Julia 1.9 (but newer versions should work as well).
Then either clone this repository
```
    git clone https://gitlab.gwdg.de/nam/ProxPython.git
```
or otherwise download the source code (see options under the blue ``Code" button in the top right of the main git repository screen).

Add the path to the source code to the JULIA_LOAD_PATH, by adding 
```
  push!(LOAD_PATH, "path/to/ProxPython/Julia/XFEL")
```
to `.julia/config/startup.jl` (create the file if it does not exist). Then, in Julia,  install the dependencies by executing
```
  ]add path/to/ProxPython-repository#master:Julia/XFEL/NPhotons
```
(this may take a few minutes). You can also do this manually by installing the packages found in `NPhotons/Project.toml`. Everything should work with the newest package versions, but you can find the package versions we used in `dependency_versions.txt`.

Demo
=============

First, in Julia, install some additional dependencies:
```
  ]add LinearAlgebra, Statistics, Rotations, ProgressMeter, Random, JLD2, BenchmarkTools, StaticArrays, Dates, FileIO, Distributions, StatsBase, SpecialFunctions, StatsFuns, Glob, IterTools, Suppressor, ColorSchemes, Accessors, Interpolations, Hungarian, GLMakie, CairoMakie, Colors, CUDA, HDF5, TickTock, Revise

```
To reduce compilation times in the notebook, run `precompile` afterwards (though this should happen automatically).  To run in commandline mode, from your system comandline prompt in the ProxPython/Julia/XFEL/scripts/ directory type `julia LSG_demo.jl`;  for a Jupyter notebook, open the script `LSG_demo.ipynb` in Jupyter, and execute the cells in order. These are the only demos that should work.  The other .jl scripts are experimental.


[Copyright (c) 2023 Russell Luke and Steffen Schultze](https://gitlab.gwdg.de/nam/ProxPython/-/blob/master/LICENSE?ref_type=heads)
