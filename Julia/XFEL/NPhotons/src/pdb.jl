function getpdb(pdb)
    if isfile(pdb)
        return read(pdb, BioStructures.PDB)
    end
    pdb = uppercase(pdb)
    if occursin(".pdb", pdb)
        return read(pdb, BioStructures.PDB)
    end
    try
        path = joinpath("pdb", pdb * ".pdb")
        if !isfile(path)
            BioStructures.downloadpdb(pdb, dir="pdb/")
        end
        return read(path, BioStructures.PDB)
    catch e
        println("PDB not found, trying mmCIF")
    end
    path = joinpath("pdb", pdb * ".cif")
    if !isfile(path)
        BioStructures.downloadpdb(pdb, dir="pdb/", format = BioStructures.MMCIF)
    end
    return read(path, BioStructures.MMCIF)
end

const atomfactorlist = Dict(
    "C" => (0.7, 6.0),
    "N" => (0.65, 7.0),
    "S" => (1.0, 16.0),
    "O" => (0.6, 8.0),
    "default" => (0.7, 6.0),
    "P" => (0.65, 15.0),
    "H" => (0.3, 1.0),
    "D" => (0.3, 1.0)
)

function element(name)
    string(filter(x -> occursin(x, "CNSOPHD"), name)[1])
end

function atomic_number(name)
    atomfactorlist[element(name)][2]
end

function atomic_radius(name)
    atomfactorlist[element(name)][1]
end


function loadPDB(pdb, select = :heavy; normalized = true)
    selector = Dict(
        :heavy => BioStructures.heavyatomselector, 
        :calpha => BioStructures.calphaselector,
        :all => BioStructures.allselector
    )[select]
    
    struc = getpdb(pdb)

    atomfactorlist = Dict(
        "C" => (0.7, 6.0),
        "N" => (0.65, 7.0),
        "S" => (1.0, 16.0),
        "O" => (0.6, 8.0),
        "default" => (0.7, 6.0),
        "P" => (0.65, 15.0),
        "H" => (0.3, 1.0),
        "D" => (0.3, 1.0)
    )
    # println(struc)
    atoms = BioStructures.collectatoms(struc, selector)
    positions = SVector{3,Float64}[]
    widths = Float64[]
    heights = Float64[]
    for atom in atoms
        push!(positions, BioStructures.coords(atom))
        el = BioStructures.element(atom)
        if el == ""
            el = BioStructures.atomname(atom)[1:1]
        end
        af = get(atomfactorlist, el, (0.7, 6.0))
        push!(widths, af[1])
        push!(heights, af[2])
    end
    positions .-= Ref(mean(positions));
    o = AtomVolume(positions, heights, widths, maximum(norm, positions, init = 0.0) + maximum(widths, init = 0.0))
    if normalized
        o = normalize(o)
    end
    o
end
