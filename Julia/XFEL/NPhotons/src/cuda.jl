@inline function warpreduce(op, x, o, u = 0)
    while o > u
        x = op(x, shfl_down_sync(0xFFFFFFFF, x, o));
        o ÷= 2
    end
    x
end

@inline warpsum(x, o, u = 0) = warpreduce(+, x, o, u)
@inline warpprod(x, o, u = 0) = warpreduce(*, x, o, u)

function kernel_gradient_1!(R, G, X, dX, P, I, ioff, ::Val{maxo}, ::Val{stride}) where {maxo, stride}
    tidx = threadIdx().x; tidy = threadIdx().y; tidz = threadIdx().z
    bidx = blockIdx().x;  bidy = blockIdx().y;  bidz = blockIdx().z
    dimx = blockDim().x;  dimy = blockDim().y;  dimz = blockDim().z
    c = clock(UInt32)
    
    i = (bidy - 1) * dimy + tidy + ioff
    j = (bidz - 1) * dimz + tidz
    jsx = (j - 1) * size(X, 1)

    if i <= size(I, 1)
        l = ldg(I, i)

        # x = @MVector fill(P[j], maxo ÷ stride + 1)
        x = MVector{maxo ÷ stride + 1, Float32}(undef)
        for o in 0:maxo÷stride
            x[o + 1] = P[l + 1, j, min(size(P, 3), o * stride + tidx)]
        end

        y = @MVector fill(0.0f0, maxo ÷ stride + 1)

        for k = 2:l+1
            for o = 0:maxo÷stride
                ind = ldg(I, (k - 1) * size(I, 1) + i) + o * stride + tidx - 1
                x[o + 1] *= ldg(X, jsx + ind)
                y[o + 1] += ldg(dX, jsx + ind)
            end
        end

        z = warpsum(dot(x, y), stride ÷ 2)
        if tidx == 1
            CUDA.@atomic G[i, j] += z
        end
 
        y = warpsum(sum(x), stride ÷ 2)
        if tidx == 1
            CUDA.@atomic R[i, j] += y
        end
    end

    return
end

function kernel_gradient!(R, G, X, dX, P, I, ioff, roff, ::Val{maxo}, ::Val{stride}) where {maxo, stride}
    tidx = threadIdx().x; tidy = threadIdx().y; tidz = threadIdx().z
    bidx = blockIdx().x;  bidy = blockIdx().y;  bidz = blockIdx().z
    dimx = blockDim().x;  dimy = blockDim().y;  dimz = blockDim().z
    
    i = (bidy - 1) * dimy + tidy + ioff
    j = (bidz - 1) * dimz + tidz + roff

    X = CUDA.Const(X); I = CUDA.Const(I); dX = CUDA.Const(dX); P = CUDA.Const(P)

    @inbounds if i <= size(I, 1)
        l = I[i, 1]

        # c = 0
        x = MVector{maxo ÷ stride + 1, Float32}(undef)
        for o in 0:maxo÷stride
            x[o + 1] = P[l + 1, j, min(size(P, 3), o * stride + tidx)]
        end

        for k = 2:l+1
            # c += 1
            for o = 0:maxo÷stride
                ind = I[i, k] + o * stride + tidx - 1
                x[o + 1] *= X[ind, j]
            end
        end

        for h in 1:size(dX, 3)
            y = @MVector zeros(Float32, maxo ÷ stride + 1)
            for k = 2:l+1
                # c += 1
                for o = 0:maxo÷stride
                    ind = I[i, k] + o * stride + tidx - 1
                    y[o + 1] += dX[ind, j, h]
                end
            end

            z = warpsum(dot(x, y), stride ÷ 2)
            if tidx == 1
                CUDA.@atomic G[i, j - roff, h] += z
            end
        end

        y = warpsum(sum(x), stride ÷ 2)
        # y = warpsum(c, stride ÷ 2)
        if tidx == 1
            CUDA.@atomic R[i, j - roff] += y
        end
    end

    return
end

function applyKernelGradient!(R, G, X::CuArray, dX, P, I, maxoffset, rrange)
    dimx = min(8, maxoffset + 1)
    dimy = 128 ÷ dimx

    for ir in partitionrange(1 : ceil(Int, size(I, 1) / dimy), maxl = 65535)
        blocks = (1, length(ir), length(rrange))
        ioff = (minimum(ir) - 1) * dimy
        roff = minimum(rrange) - 1
        @cuda name="grad" threads=(dimx,dimy,1) blocks=blocks kernel_gradient!(R, G, X, dX, P, I, ioff, roff, Val(maxoffset), Val(dimx))
    end
end


function computelogpGradient!(X, X2, dX, dX2, meanX, XP, dXP, P, R, G, pixelWeights, rotationWeights, images, intensity, npercircle, imagelengths, area; inner = true, mode = :poisson)
    npix = size(X, 1)
    nrot = size(X, 2)
    nimg = size(images, 1)
    lmax = size(images, 2) - 1
    ng = size(G, 3)

    mul!(XP, X', pixelWeights)
    rXP = reshape(XP, 1, size(XP)...)

    P .= .- intensity .* rXP
    maxlogP = maximum(P, dims = (2,3))
    P .= exp.(P .- maxlogP)

    reshape(X2, 2npercircle, :)[1:npercircle, :] .= reshape(X, npercircle, :)
    reshape(X2, 2npercircle, :)[npercircle+1:end, :] .= reshape(X, npercircle, :)
    reshape(dX2, 2npercircle, :)[1:npercircle, :] .= reshape(dX, npercircle, :)
    reshape(dX2, 2npercircle, :)[npercircle+1:end, :] .= reshape(dX, npercircle, :)
    dX2 ./= X2

    for h in 1:size(dX, 3)
        mul!(view(dXP, :, :, h), view(dX, :, :, h)', pixelWeights)
    end

    meanX .= 1
    meanX[2:end, 1:1] .= mean(X2, dims = 2)
    reshaped = reshape(view(meanX, 2:size(meanX, 1), 1), npercircle, :)
    reshaped .= mean(reshaped, dims = 1)
    X2 ./= meanX[2:end]

    result = zeros(nimg,1)
    resultG = zeros(nimg, 1, ng)
    
    for rr in partitionrange(1:nrot, stride = size(R, 2))
        R .= 0; G .= 0
        applyKernelGradient!(R, G, X2, dX2, P, images, ifelse(inner, npercircle-1, 0), rr)

        @views G[:, 1:length(rr), :] .-= intensity .* permutedims(dXP[rr, :, :], (2, 1, 3)) .* R[:, 1:length(rr)]

        view(G, :, 1:length(rr), :) .*= view(rotationWeights, :, rr)
        view(R, :, 1:length(rr)) .*= view(rotationWeights, :, rr)

        result .+= Array(sum(R, dims=2))
        resultG .+= Array(sum(G, dims=2))
    end

    resultG ./= result

    result = log.(result ./ ifelse(inner, npercircle, 1))
    result .+= Array(sum(Float64.(log.(meanX[images[:, 2:end] .+ 1])), dims = 2))
    result .+= getindex.(Ref(reshape(Array(maxlogP), :)), imagelengths .+ 1)

    # reshape(result,:), sum(resultG, dims = 1)[:]
    sum(resultG, dims = 1)[:]
end

function gradientClosure(images, rotations, pixels, pixelWeights, npercircle, rotationWeights, N; polarization = 1, area = 1.0, ncalls = 10, rmask = [], nmask = [], usecuda = true, sorted = true, inner = true, mode = :poisson, ndiffs = 1)
    images = if images isa Vector{<:SVector{3, <:Real}}
        deserializeImages(images)
    else
        images
    end

    npix = length(pixels); nrot = length(rotations); 
    nimg = length(images); lmax = maximum(length.(images), init = 0)

    if sorted
        images = sort(images, by = length)
    end
    indexImages = if eltype(images[1]) == Int
        images
    elseif length(eltype(eltype(images))) == 2
        roundImagesCircular(pixels, images, npercircle)
    else
        roundImages(pixels, images)
    end
    indexImages = [image .+ npercircle .* ((image .- 1) .÷ npercircle) for image in indexImages]
    imageMatrix = toImageMatrix(indexImages)
    imagelengths = length.(images)


    I = UInt16.(imageMatrix)
    meanX = ones(2npix + 1, 1)
    X2 = zeros(2npix, nrot)
    dX2 = zeros(2npix, nrot, ndiffs)
    XP = zeros(nrot, size(pixelWeights, 2))
    dXP = zeros(nrot, size(pixelWeights, 2), ndiffs)
    P = zeros(lmax+1, nrot, size(pixelWeights, 2))
    R = zeros(nimg, ceil(Int, nrot / ncalls))
    G = zeros(nimg, ceil(Int, nrot / ncalls), ndiffs)
    pixelWeights = pixelWeights
    rotationWeights = reshape(copy(rotationWeights),1,:)

    cuI = cu(I); cumeanX = cu(meanX); cuX2 = cu(X2); cuXP = cu(XP); cuP = cu(P); cuR = cu(R);
    cupixelWeights = cu(pixelWeights); curotationWeights = cu(rotationWeights); cuG = cu(G)
    cudX2 = cu(dX2); cudXP = cu(dXP); 

    logps(X, dX) = computelogpGradient!(X, cuX2, dX, cudX2, cumeanX, cuXP, cudXP, cuP, cuR, cuG, cupixelWeights, curotationWeights, cuI, N, npercircle, imagelengths, area, inner = inner, mode = mode)
end





function kernel_cpu!(R, X, P, I, ::Val{maxo}, ::Val{stride}) where {maxo, stride}
    for i in axes(I, 1), j in axes(P, 2)
        l = I[i,1]
        y = zero(eltype(R))

        for tidx in 1:stride
            x = fill(P[l+1,j], maxo ÷ stride + 1)
            for k = 2:l+1
                for o in 0:maxo÷stride
                    x[o + 1] *= X[I[i,k] + o * stride + tidx - 1, j]
                end
            end
            y += sum(x)
        end

        R[i, j] += y
    end

    return
end


function kernel!(R, X, P, I, A, B, ioff, roff, ::Val{maxo}, ::Val{stride}) where {maxo, stride}
    tidx = threadIdx().x; tidy = threadIdx().y; tidz = threadIdx().z
    bidx = blockIdx().x;  bidy = blockIdx().y;  bidz = blockIdx().z
    dimx = blockDim().x;  dimy = blockDim().y;  dimz = blockDim().z
    i = (bidy - 1) * dimy + tidy + ioff
    j = (bidz - 1) * dimz + tidz + roff

    X = CUDA.Const(X); I = CUDA.Const(I); P = CUDA.Const(P)

    @inbounds if i <= size(I, 1)
        l = I[i, 1]

        x = MVector{maxo ÷ stride + 1, Float32}(undef)
        for o in 0:maxo÷stride
            x[o + 1] = P[l + 1, j, min(size(P, 3), o * stride + tidx)]
        end

        for k = 2:l+1
            p = I[i, k]
            for o in 0:maxo÷stride
                ind = p + o * stride + tidx - 1
                if A isa Real
                    x[o + 1] *= X[ind, j]
                else
                    x[o + 1] *= ldg(A, p) * X[ind, j] + ldg(B, p)
                end
            end
        end
        
        y = warpsum(sum(x), stride ÷ 2)

        if tidx == 1
            CUDA.@atomic R[i, j - roff] += y
        end
    end

    return
end

function applyKernel!(R, X::CuArray, P, I, maxoffset, rrange, A = 1f0, B = 0f0)
    dimx = min(8, maxoffset + 1)
    dimy = 512 ÷ dimx
    
    for ir in partitionrange(1 : ceil(Int, size(I, 1) / dimy), maxl = 65535)
        blocks = (1, length(ir), length(rrange))
        ioff = (minimum(ir) - 1) * dimy
        roff = minimum(rrange) - 1
        @cuda name="logp" threads=(dimx,dimy,1) blocks=blocks kernel!(R, X, P, I, A, B, ioff, roff, Val(maxoffset), Val(dimx))
        # open("kernel32g.cu", "w") do io
            # @device_code_sass io=io @cuda name="logp" threads=(dimx,dimy,1) blocks=blocks kernel!(R, X, P, I, A, B, ioff, roff, Val(maxoffset), Val(dimx))
        # end
    end
end

# function kernel!(R, X, P, I, A, B, ioff, roff, ::Val{maxo}, ::Val{stride}) where {maxo, stride}
#     tidx = threadIdx().x; tidy = threadIdx().y; tidz = threadIdx().z
#     bidx = blockIdx().x;  bidy = blockIdx().y;  bidz = blockIdx().z
#     dimx = blockDim().x;  dimy = blockDim().y;  dimz = blockDim().z
#     i = (bidy - 1) * dimy + tidy + ioff
#     j = (bidz - 1) * dimz + tidz + roff

#     X = CUDA.Const(X); I = CUDA.Const(I); P = CUDA.Const(P)

#     @inbounds if i <= size(I, 1)
#         l = I[i, 1]

#         x = MVector{maxo ÷ stride + 1, Float32}(undef)
#         for o in 0:maxo÷stride
#             x[o + 1] = P[l + 1, j, min(size(P, 3), o * stride + tidx)]
#         end
        
#         for k0 = 2:4:l+1
#             p = I[i, k0 + (tidx - 1) ÷ 8]

#             for o in 0:maxo÷stride
#                 ind = p + o * stride + (tidx - 1) % 8
#                 x[o + 1] *= warpprod(X[ind, j], 16, 4)
#             end
#         end
        
#         y = warpsum(sum(x), 4)

#         if tidx == 1
#             CUDA.@atomic R[i, j - roff] += y
#         end
#     end

#     return
# end

# function applyKernel!(R, X::CuArray, P, I, maxoffset, rrange, A = 1f0, B = 0f0)
#     dimx = min(32, maxoffset + 1)
#     dimy = 512 ÷ dimx
    
#     for ir in partitionrange(1 : ceil(Int, size(I, 1) / dimy), maxl = 65535)
#         blocks = (1, length(ir), length(rrange))
#         ioff = (minimum(ir) - 1) * dimy
#         roff = minimum(rrange) - 1
#         @cuda name="logp" threads=(dimx,dimy,1) blocks=blocks kernel!(R, X, P, I, A, B, ioff, roff, Val(maxoffset), Val(8))
#         # open("kernel32g.cu", "w") do io
#             # @device_code_sass io=io @cuda name="logp" threads=(dimx,dimy,1) blocks=blocks kernel!(R, X, P, I, A, B, ioff, roff, Val(maxoffset), Val(dimx))
#         # end
#         # println(sum(R))
#     end
# end

function applyKernel!(R, X, P, I, maxoffset)
    kernel_cpu!(R, X, P, I, Val(maxoffset), Val(8))
end



function computelogp!(X, X2, meanX, XP, P, R, A, nB, B, pixelWeights, rotationWeights, images, intensity, npercircle, imagelengths, area; inner = true, mode = :poisson)
    npix = size(X, 1)
    nrot = size(X, 2)
    nimg = size(images, 1)
    lmax = size(images, 2) - 1

    mul!(XP, X', pixelWeights)
    XP .+= nB
    rXP = reshape(XP, 1, size(XP)...)
    if mode == :poisson
        P .= .- intensity .* rXP
    elseif mode == :normalized
        P .= log.(rXP) .* (0:-1:-lmax)
    elseif mode == :gamma
        α = 4; β = 4 / intensity
        P .= log.(rXP .+ β) .* ((0:-1:-lmax) .- α)
    elseif mode == :integral
        P .= log.(rXP) .* (-1:-1:-lmax-1)
    elseif mode == :finite_integral
        P .= log.(rXP) .* (-1:-1:-lmax-1)
        P .+= cu(gammalogcdf.(1:lmax+1, 1, intensity .* Array(rXP)))
    end

    maxlogP = maximum(P, dims = (2,3))
    P .= exp.(P .- maxlogP)

    reshape(X2, 2npercircle, :)[1:npercircle, :] .= reshape(X, npercircle, :)
    reshape(X2, 2npercircle, :)[npercircle+1:end, :] .= reshape(X, npercircle, :)

    # divide by mean to prevent Float32 underflow
    meanX .= 1
    Bm = similar(view(meanX, 2:length(meanX)))
    reshape(Bm, 2npercircle, :)[1:npercircle, :] .= B isa Real ? B : reshape(B, npercircle, :)
    reshape(Bm, 2npercircle, :)[npercircle+1:end, :] .= B isa Real ? B : reshape(B, npercircle, :)
    meanX[2:end, 1:1] .= 0.9 .* mean(X2, dims = 2) .+ Bm
    reshaped = reshape(view(meanX, 2:size(meanX, 1), 1), npercircle, :)
    reshaped .= mean(reshaped, dims = 1)
    X2 ./= meanX[2:end]
    Bm ./= meanX[2:end]

    result = zeros(nimg,1)
    
    for rr in partitionrange(1:nrot, stride = size(R, 2))
        R .= 0
        applyKernel!(R, X2, P, images, ifelse(inner, npercircle-1, 0), rr, A, Bm)
        view(R, :, 1:length(rr)) .*= view(rotationWeights, :, rr)
        result .+= Array(sum(R, dims=2))
    end

    result = log.(result ./ ifelse(inner, npercircle, 1))# .- logfactorial.(imagelengths)
    
    # correct for error made when preventing underflow
    result .+= Array(sum(Float64.(log.(meanX[images[:, 2:end] .+ 1])), dims = 2))
    result .+= getindex.(Ref(reshape(Array(maxlogP), :)), imagelengths .+ 1)

    reshape(result,:)
end

function computeClosure(images, rotations, pixels, pixelWeights, npercircle, rotationWeights, N; polarization = 1, area = 1.0, ncalls = 10, rmask = [], nmask = [], usecuda = true, sorted = true, inner = true, mode = :poisson)
    images = if images isa Vector{<:SVector{3, <:Real}}
        deserializeImages(images)
    else
        images
    end

    npix = length(pixels); nrot = length(rotations); 
    nimg = length(images); lmax = maximum(length.(images), init = 0)

    if sorted
        images = sort(images, by = length)
    end
    indexImages = if eltype(images[1]) == Int
        images
    elseif length(eltype(eltype(images))) == 2
        roundImagesCircular(pixels, images, npercircle)
    else
        roundImages(pixels, images)
    end
    indexImages = [image .+ npercircle .* ((image .- 1) .÷ npercircle) for image in indexImages]
    imageMatrix = toImageMatrix(indexImages)
    imagelengths = length.(images)

    I = UInt16.(imageMatrix)
    meanX = ones(2npix + 1, 1)
    X2 = zeros(2npix, nrot)
    XP = zeros(nrot, size(pixelWeights, 2))
    P = zeros(lmax+1, nrot, size(pixelWeights, 2))
    R = zeros(nimg, ceil(Int, nrot / ncalls))
    pixelWeights = pixelWeights
    rotationWeights = reshape(copy(rotationWeights),1,:)
    A = if polarization isa Real
        polarization
    else
        rp = reshape(polarization, npercircle , :); vcat(rp, rp)[:]
    end

    if usecuda
        cuI = cu(I); cumeanX = cu(meanX); cuX2 = cu(X2); cuXP = cu(XP); cuP = cu(P); cuR = cu(R);
        cupixelWeights = cu(pixelWeights); curotationWeights = cu(rotationWeights); cuA = cu(A)
        logps(X, nB = 0, B = 0) = computelogp!(X, cuX2, cumeanX, cuXP, cuP, cuR, cuA, nB, cu(B), cupixelWeights, curotationWeights, cuI, N, npercircle, imagelengths, area, inner = inner, mode = mode)
    else
        logps_cpu(X, nB = 0, B = 0) = computelogp!(X, X2, meanX, XP, P, R, A, nB, B, pixelWeights, rotationWeights, I, N, npercircle, imagelengths, area, inner = inner, mode = mode)
    end
end

function computeClosure(images, data, N; kwargs...)
    computeClosure(images, data.rotations, data.pixels, data.pixelWeights, data.npercircle, data.rotationWeights, N; kwargs...)
end




function setupEval(rotations, pixels; usecuda = true)
    X = zeros(length(pixels), length(rotations))
    points = SVector{3, Float32}.([R * x for x in pixels, R in rotations])

    if usecuda
        cuX = cu(X)
        cupoints = cu(points)
        function ev(f)
            cuX .= cu(f).(cupoints)
            # evaluate!(cuX, cuda(f), cupoints)
            cuX
        end
    else
        function ev_cpu(f)
            X .= f.(points)
            # evaluate!(X, f, points)
            X
        end
    end
end


function setupCorrection(rmask, nmask, pixels, pixelWeights, rotationWeights, npercircle, N; polarization = 1, mode = :poisson, usecuda = true)
    if isempty(rmask)
        return (X, nB = 0, B = 0) -> 0.0
    end

    XP = zeros(length(rotationWeights), size(pixelWeights, 2))
    upper = [rmask[2:end]; Inf]
    masks = [pixelWeights .* maskfacs(rmask[i], upper[i], pixels) for i in 1:length(nmask)]

    if usecuda
        cuXP = cu(XP)
        cumasks = cu.(masks)
        function computeCorrection(X, nB = 0, B = 0)
            ps = [(mul!(cuXP, X', m); Float64.(Array(cuXP)) .+ sum(cm[:,1] ./ polarization .* B)) for (cm, m) in zip(masks, cumasks)]
            if mode == :poisson
                log(max(0.0, sum(rotationWeights .* tailMultinomial.(collect.(zip(ps...)), Ref(nmask .- 1), N))))
            elseif mode == :gamma
                α = 4; β = 4 / N
                x = log(max(0.0, sum(rotationWeights .* reduce(.*, 1 .- cdfGammaPoisson.(nmask[i] - 1, α, β ./ ps[i]) for i in eachindex(nmask))))) - log(npercircle)
            else
                0.0
            end
        end
    else
        function computeCorrection_cpu(X, nB = 0, B = 0)
            ps = [(mul!(XP, X', m); Float64.(Array(XP)) .+ sum(m[:,1] ./ polarization .* B)) for m in masks]
            if mode == :poisson
                log(max(0.0, sum(rotationWeights .* tailMultinomial.(collect.(zip(ps...)), Ref(nmask .- 1), N))))
            elseif mode == :gamma
                α = 4; β = 4 / N
                log(max(0.0, sum(rotationWeights .* reduce(.*, 1 .- cdfGammaPoisson.(nmask[i] - 1, α, β ./ ps[i]) for i in eachindex(nmask))))) - log(npercircle)
            else
                0.0
            end
        end
    end
end


function logpsClosure(images, rotations, pixels, pixelWeights, npercircle, rotationWeights, N; ncalls = 10, usecuda = true, inner = true, mode = :poisson, polarization = 1, area = 1)
    eval = setupEval(rotations, pixels, usecuda = usecuda)
    computelogps! = computeClosure(images, rotations, pixels, pixelWeights, npercircle, rotationWeights, N; ncalls = ncalls, usecuda = usecuda, sorted = false, inner = inner, mode = mode, polarization = polarization, area = area)

    function logps(f; b = 0)
        B = b isa Real ? b : b.(pixels)
        nB = sum(pixelWeights[:,1] ./ polarization .* B)
        computelogps!(eval(f), nB, B)
    end
end

function logpClosure(images, rotations, pixels, pixelWeights, npercircle, rotationWeights, N; ncalls = 10, rmask = [], nmask = [], usecuda = true, sorted = true, inner = true, mode = :poisson, polarization = 1, area = 1)
    eval = setupEval(rotations, pixels, usecuda = usecuda)
    computelogps! = computeClosure(images, rotations, pixels, pixelWeights, npercircle, rotationWeights, N; ncalls = ncalls, usecuda = usecuda, sorted = sorted, inner = inner, mode = mode, polarization = polarization, area = area)
    computecorrection = setupCorrection(rmask, nmask, pixels, pixelWeights, rotationWeights, npercircle, N, mode = mode, usecuda = usecuda, polarization = polarization)

    function logp(fs::AbstractArray, ws::Nothing; b = 0)
        ps = Vector{Float64}[]
        cs = Float64[]
        
        bs = b isa Array ? b : [b]
        bnbs = map(bs) do b
            B = b isa Real ? b : b.(pixels)
            nB = sum(pixelWeights[:,1] ./ polarization .* B)
            B, nB
        end

        for (f, (B, nB)) in zip(fs, Iterators.cycle(bnbs))
            X = eval(f)
            push!(cs, computecorrection(X, nB, B))
            push!(ps, computelogps!(X, nB, B))
        end

        rps = reduce(hcat, ps)
        ws -> sum(conflse(rps, ws ./ sum(ws))) - length(ps[1]) * logsumexp(cs, ws ./ sum(ws))
    end

    logp(fs::AbstractArray, ws::AbstractArray; kwargs...) = logp(fs, nothing; kwargs...)(ws)
    logp(f; kwargs...) = logp([f]; kwargs...)
    logp(fs::AbstractArray; kwargs...) = logp(fs, normalize(ones(length(fs), 1)); kwargs...)
    logp(fsws::Tuple; kwargs...) = logp(fsws...; kwargs...)
    
    logp
end

function logpClosure(images, params, N; kwargs...)
    logpClosure(images, params.rotations, params.pixels, params.pixelWeights, params.npercircle, params.rotationWeights, N; kwargs...)
end

function logpsClosure(images, params, N; kwargs...)
    logpsClosure(images, params.rotations, params.pixels, params.pixelWeights, params.npercircle, params.rotationWeights, N; kwargs...)
end

function convertArguments(images, N; qmax = 0.0, precision = 23, qstep = 0.1, npercircle = 32, wavelength = 2.5, isotropic = true, polarization = 1, detection = 1, kwargs...)
    if qmax == 0
        qmax = round(maximum(Base.Fix1(maximum, norm), images), digits = 1)
    end
    params = integrationParameters(precision = precision, qstep = qstep, qmax = qmax, npercircle = npercircle, wavelength = wavelength)

    A = polarization isa Real ? polarization : polarization.(params.pixels)
    D = if detection isa Real
        detection
    else
        ps, ws = weightedEwaldGrid(qstep = qstep/10, qmax = qmax, npercircle = 10npercircle, wavelength = wavelength)
        ds = zeros(length(params.pixels))
        counts = zeros(Int, length(params.pixels))
        for (i, p) in zip(roundImages(params.pixels, [ps])[1], ps)
            ds[i] += detection(p)
            counts[i] += 1
        end
        ds ./= max.(1, counts)
        ds
    end
    area = sum(D .* params.pixelWeights)

    if A isa Array || D isa Array || !isotropic
        npixelWeights = zeros(length(params.pixelWeights), npercircle)
        for i in 0:npercircle-1
            circshift!(view(reshape(npixelWeights, npercircle, :, npercircle),:,:,i+1), reshape(params.pixelWeights, npercircle, :), (i, 0))
        end
        params = (rotations = params.rotations, pixels = params.pixels, pixelWeights = npixelWeights, npercircle = params.npercircle, rotationWeights = params.rotationWeights)
    end
    if A isa Array
        for i in 0:npercircle-1
            view(reshape(params.pixelWeights, npercircle, :, npercircle),:,:,i+1) .*= circshift(reshape(A, npercircle, :), (i, 0))
        end
    end
    if D isa Array
        for i in 0:npercircle-1
            view(reshape(params.pixelWeights, npercircle, :, npercircle),:,:,i+1) .*= circshift(reshape(D, npercircle, :), (i, 0))
        end
    end

    args = (images, params, N)
    kwargs = (; polarization = A, area = area, kwargs...)
    args, kwargs
end

function logpClosure(images, N; kwargs...)
    args, kwargs = convertArguments(images, N; kwargs...)
    logpClosure(args...; kwargs...)
end

function logpsClosure(images, N; kwargs...)
    args, kwargs = convertArguments(images, N; kwargs...)
    logpsClosure(args...; kwargs...)
end

function logpClosureConf(args...; kwargs...)
    logpClosure(args...; kwargs...)
end

function integrationParameters(; precision = 23, qstep = 0.1, qmax, npercircle = 32, wavelength = 2.5)
    pixels, pixelWeights = weightedEwaldGrid(qstep = qstep, qmax = qmax, npercircle = npercircle, wavelength = wavelength)
    rotations, rotationWeights = rotationGrid(precision = precision)
    integrationData = (; rotations, pixels, npercircle, pixelWeights, rotationWeights)
end


function maskfacs(rmask, pixels)
    r = 0.5norm(pixels[end])^2 / pixels[end][3]
    angles = approxUnique(2 .* asin.(norm.(pixels) ./ (2r)))

    rmask > maximum(norm.(pixels)) && return(zeros(size(pixels)...))
    amask = 2asin(rmask / (2r))
    
    borders = (angles[2:end] .+ angles[1:end-1]) ./ 2
    outer = [borders; angles[end] + angles[end] - borders[end]]
    inner = [0.0; borders]
    
    facs = 1 .- clamp.((cos.(inner) .- cos(amask)) ./ (cos.(inner) - cos.(outer)), 0, 1)
    
    return repeat(facs, inner = length(pixels) ÷ length(facs))
end

# function maskfacs(rmask, pixels)
#     radii = approxUnique(norm.(pixels))
#     rmask > maximum(radii) && return(zeros(size(pixels)...))
    
#     borders = (radii[2:end] .+ radii[1:end-1]) ./ 2
#     upper = [borders; Inf]
#     lower = [0.0; borders]
    
#     facs = 1 .- clamp.((rmask.^2 .- lower.^2) ./ (upper.^2 .- lower.^2), 0, 1)
#     return repeat(facs, inner = length(pixels) ÷ length(facs))
# end

function maskfacs(rmask1, rmask2, pixels)
    maskfacs(rmask1, pixels) .- maskfacs(rmask2, pixels)
end

























# function kernel_pullback!(R, dR, X, dX, P, dP, I, ::Val{maxo}, ::Val{stride}) where {maxo, stride}
#     tidx = threadIdx().x; tidy = threadIdx().y; tidz = threadIdx().z
#     bidx = blockIdx().x;  bidy = blockIdx().y;  bidz = blockIdx().z
#     dimx = blockDim().x;  dimy = blockDim().y;  dimz = blockDim().z
#     c = clock(UInt32)
    
#     i = (bidy - 1) * dimy + tidy
#     j = (bidz - 1) * dimz + tidz
#     jsx = (j - 1) * size(X, 1)

#     shmem = CuDynamicSharedArray(Float32, size(dX, 1))
#     @inbounds for ind = tidx + dimx * (tidy - 1) : dimx * dimy : size(dX, 1)
#         shmem[ind] = 0
#     end
    
#     CUDA.sync_threads()

#     @inbounds if i <= size(I, 1)
#         l = ldg(I, i)

#         x = @MVector fill(dR[i, j] * P[j], maxo ÷ stride + 1)
#         for k = 2:l+1
#             for o = 0:maxo÷stride
#                 ind = ldg(I, (k - 1) * size(I, 1) + i) + o * stride + tidx - 1
#                 x[o + 1] *= ldg(X, jsx + ind)
#             end
#         end

#         for k = 2:l+1
#             for o = 0:maxo÷stride
#                 ind = ldg(I, (k - 1) * size(I, 1) + i) + o * stride + tidx - 1
#                 CUDA.@atomic shmem[ind] += x[o + 1] / ldg(X, jsx + ind)
                
#                 # CUDA.@atomic dX[jsx + ind] += x[o + 1] / ldg(X, jsx + ind)
#             end
#         end

#         y = sum(x) / P[j]
        
#         offset = stride ÷ 2
#         while offset > 0
#             y += shfl_down_sync(0xFFFFFFFF, y, offset);
#             offset ÷= 2
#         end

#         if tidx == 1
#             CUDA.@atomic dP[j] += y
#         end
#     end

#     CUDA.sync_threads()

#     @inbounds for ind = tidx + dimx * (tidy - 1) : dimx * dimy : size(dX, 1)
#         CUDA.@atomic dX[jsx + ind] += shmem[ind]
#     end

#     return
# end

# function applyKernelPullback!(R, dR, X, dX, P, dP, I, maxoffset)
#     dimx = 1
#     dimy = 512 ÷ dimx
#     blocks = (1, ceil(Int, size(I, 1) / dimy), size(X, 2))
#     shmem = size(X, 1) * sizeof(Float32)
#     # println(dimx)
#     # println(dimy)
#     # println(blocks)
#     @cuda threads=(dimx,dimy,1) blocks=blocks shmem=shmem kernel_pullback!(R, dR, X, dX, P, dP, I, Val(maxoffset), Val(dimx));
# end


# function computegradient!(dlogresult, X, dX, X2, dX2, meanX, dmeanX, XP, dXP, P, dP, R, dR, pixelWeights, rotationWeights, images, intensity, npercircle)
#     npix = size(X, 1)
#     nrot = size(X, 2)
#     nimg = size(images, 1)
#     lmax = size(images, 2) - 1
    
    
#     mul!(XP, X', pixelWeights)
#     P .= (-intensity) .* XP

#     meanlogp = mean(P)
#     P .= exp.(P .- meanlogp)

#     reshape(X2, 2npercircle, :)[1:npercircle, :] .= reshape(X, npercircle, :)
#     reshape(X2, 2npercircle, :)[npercircle+1:end, :] .= reshape(X, npercircle, :)
    
#     # meanX .= 1
#     # meanX[2:end, 1:1] .= mean(X2, dims = 2)
#     # reshaped = reshape(view(meanX, 2:size(meanX, 1), 1), npercircle, :)
#     # reshaped .= mean(reshaped, dims = 1)
#     # X2 ./= meanX[2:end, 1:1]

#     result = zeros(nimg,1)

#     for i in 1:size(R, 2):nrot
#         e = min(nrot, i + size(R, 2) - 1)
#         R .= 0
#         applyKernel!(R, view(X2,:,i:e), view(P,i:e), images, npercircle-1)
#         view(R, :, 1:length(i:e)) .*= view(rotationWeights, :, i:e)
#         result .+= Array(sum(R, dims=2))
#     end

#     logresult = log.(result ./ npercircle)

#     logresult .+= Array(sum(Float64.(log.(meanX[images[:, 2:end] .+ 1])), dims = 2))
#     logresult .+= meanlogp
    
#     dmeanlogp = length(logresult)
#     dmeanX[images[:, 2:end] .+ 1]
#     # logresult .+= Array(sum(Float64.(log.(meanX[images[:, 2:end] .+ 1])), dims = 2))

#     dresult = cu(dlogresult ./ result)

#     dX2 .= 0
#     dP .= 0
#     for i in 1:size(R, 2):nrot
#         e = min(nrot, i + size(R, 2) - 1)
#         dR .= dresult
#         view(dR, :, 1:length(i:e)) .= view(rotationWeights, :, i:e)
        
#         applyKernelPullback!(R, dR, view(X2,:,i:e), view(dX2,:,i:e), view(P,i:e), view(dP,i:e), images, npercircle-1)
#     end
    
#     # dmeanX[2:end, 1:1] .= -dX2 .* X2 ./ meanX[2:end, 1:1].^2
#     # dX2 ./= meanX[2:end, 1:1].^2 # X2 ./= meanX[2:end, 1:1]

#     # dX2 .+= dmeanX[2:end, 1:1] ./ npercircle
#     # meanX[2:end, 1:1] .= mean(X2, dims = 2)


#     reshape(dX, npercircle, :) .= reshape(dX2, 2npercircle, :)[1:npercircle, :]
#     reshape(dX, npercircle, :) .+= reshape(dX2, 2npercircle, :)[npercircle+1:end, :]

#     # dP .= P .* dP # P .= exp.(P .- meanlogp)
#     # dmeanlogp = -mean(dP)
#     # dP .+= dmeanlogp # meanlogp = mean(P)    

#     # dXP .= (-intensity) .* dP
#     # mul!(dX, pixelWeights, dXP', 1, 1)

    
#     reshape(logresult,:), dX
# end

# function gradientClosure(images, rotations, pixels, pixelWeights, npercircle, rotationWeights, N; ncalls = 10, rmask = [], nmask = [], usecuda = true, sorted = true, inner = true)
#     images = if images isa Vector{<:SVector{3, <:Real}}
#         deserializeImages(images)
#     else
#         images
#     end

#     npix = length(pixels); nrot = length(rotations); 
#     nimg = length(images); lmax = maximum(length.(images))

#     if sorted
#         images = sort(images, by = length)
#     end
#     indexImages = if eltype(images[1]) == Int
#         images
#     elseif length(eltype(eltype(images))) == 2
#         roundImagesCircular(pixels, images, npercircle)
#     else
#         roundImages(pixels, images)
#     end
#     indexImages = [image .+ npercircle .* ((image .- 1) .÷ npercircle) for image in indexImages]
#     imageMatrix = toImageMatrix(indexImages)
#     imagelengths = length.(images)


#     I = cu(UInt16.(imageMatrix))
#     meanX = cu(ones(2npix + 1, 1))
#     X2 = cu(zeros(2npix, nrot))
#     XP = cu(zeros(nrot))
#     P = cu(zeros(nrot))
#     R = cu(zeros(nimg, ceil(Int, nrot / ncalls)))
#     pixelWeights = cu(pixelWeights)
#     rotationWeights = cu(reshape(rotationWeights, 1, :))

#     dX2 = similar(X2)
#     dXP = similar(XP)
#     dP = similar(P)
#     dR = similar(R)
#     dmeanX = similar(meanX)

#     logps(X, dX) = computegradient!(1, X, dX, X2, dX2, meanX, dmeanX, XP, dXP, P, dP, R, dR, pixelWeights, rotationWeights, I, N, npercircle)
# end
