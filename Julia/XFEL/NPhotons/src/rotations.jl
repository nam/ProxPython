using Rotations

function randomRotation(rng::AbstractRNG)
    RotMatrix{3}(UnitQuaternion(randn(rng), randn(rng), randn(rng), randn(rng)))
end

function randomRotation(dim::Int = 3)
    return rand(RotMatrix{dim})
    # Q, R = qr(randn(dim, dim))
    # r = sign.(diag(R))
    # L = Diagonal(r)
    # res = Q*L
    # if(det(res) > 0)
    #     return res
    # else
    #     randomRotation(dim)
    # end
end

function randomSmallRotation(σ, rng = Random.GLOBAL_RNG)
    α = randn(rng) * σ
    S = randomRotation(rng)
    R = SA[cos(α) -sin(α) 0; sin(α) cos(α) 0; 0 0 1]
    return S^-1 * R * S
end

function rotationsFromPoles(poles, ncirc; sym = false)
    rotations = QuatRotation{Float64}[]
    e = [0.0, 0.0, 1.0]
    for b in poles
        R = rotation_between(b, e)
        # Re1 = R * [1.0, 0.0, 0.0]
        # Re1 = Re1 .* [1.0, 1.0, 0.0]
        # if norm(Re1) > 0
        #     R = rotation_between(Re1, [1.0, 0.0, 0.0]) * R
        # end
        for a in prange(0, (2 - sym) * pi, ncirc)# .+ 0.00001
            push!(rotations, R * AngleAxis(a, 0, 0, 1))
        end
    end
    rotations
end

function rotationFromPole(pole, angle)
    e = [0.0, 0.0, 1.0]
    R = rotation_between(pole, e)
    Re1 = R * [1.0, 0.0, 0.0]
    Re1 = Re1 .* [1.0, 1.0, 0.0]
    if norm(Re1) > 0
        R = rotation_between(Re1, [1.0, 0.0, 0.0]) * R
    end
    AngleAxis(angle, 0, 0, 1) * R
end


function lebedevGrid(;precision = 41)
    lebedev = readdlm(project_path("data/lebedev/lebedev_$(lpad(precision,3,"0")).txt"))
    sph2xyz.(lebedev[:,1] ./ 180 .* pi, lebedev[:,2] ./ 180 .* pi), lebedev[:,3]
end

function uniformRotationGrid(nsub, ncirc; sym = false)
    rotationsFromPoles(getIcosphereVerts(nsub), ncirc; sym = false)
end

function rotationGrid(; precision, class = :lebedev)
    if class == :lebedev
        lebedev = readdlm(project_path("data/lebedev/lebedev_$(lpad(precision,3,"0")).txt"))
        xyz = sph2xyz.(lebedev[:,1] ./ 180 .* pi, lebedev[:,2] ./ 180 .* pi)
        rotations = rotation_between.(xyz, Ref([0.0, 0.0, 1.0]))
        rotationWeights = lebedev[:,3]
        return rotations, rotationWeights
    elseif class == :design
        files = readdir(project_path("data/designs"))
        file = files[findfirst(x -> occursin(string(precision), x), files)]
        xyz = Array.(eachcol(reshape(readdlm(project_path("data/designs/$(file)")), 3, :)))
        rotations = rotation_between.(xyz, Ref([0.0, 0.0, 1.0]))
        rotationWeights = fill(1/length(rotations), length(rotations))
        return rotations, rotationWeights
    end
    throw(ArgumentError("invalid class"))
end

function uniformRotationGrid(; precision, npercircle, class = :lebedev)
    if class == :lebedev
        lebedev = readdlm(project_path("data/lebedev/lebedev_$(lpad(precision,3,"0")).txt"))
        xyz = sph2xyz.(lebedev[:,1] ./ 180 .* pi, lebedev[:,2] ./ 180 .* pi)
        rotations = rotationsFromPoles(xyz, npercircle, sym = false)
        rotationWeights = repeat(lebedev[:,3] ./ npercircle, inner = (npercircle, 1))[:]
        return rotations, rotationWeights
    elseif class == :design
        files = readdir(project_path("data/designs"))
        file = files[findfirst(x -> occursin(string(precision), x), files)]
        xyz = Array.(eachcol(reshape(readdlm(project_path("data/designs/$(file)")), 3, :)))
        rotations = rotation_between.(xyz, Ref([0.0, 0.0, 1.0]))
        rotationWeights = fill(1/length(rotations), length(rotations))
        rotations, rotationWeights
    end


    throw(ArgumentError("invalid class"))
end




function align(f, g; res::Int = 10, ntries::Int = 1000, radius = 1)
    vs = gridinUnitBall(res) .* radius
    fa = f.(vs)
    
    R = Array(randomRotation(3))
    R, ga = alignGlobal!(R, fa, g, vs, ntries)
    R, ga = alignLocal!(R, fa, g, vs, ntries, 0.5)
    R
end

function alignGlobal!(R, fa, g, vs, M)
    ga = g.(Ref(R) .* vs)
    d = abs(dot(fa, ga))
    
    nga = copy(ga)
    for i in 1:M
        S = Array(randomRotation(3))
        if rand() > 0.5
            S .*= -1
        end
        nga .= g.(Ref(S) .* vs)
        newd = abs(dot(fa, nga))
        if newd > d
            ga .= nga
            d = newd
            R .= S
        end
    end
    R, ga
end

function alignLocal!(R, fa, g, vs, M, σ)
    ga = g.(Ref(R) .* vs)
    d = abs(dot(fa, ga))
    
    nga = copy(ga)
    for i in 1:M
        S = Array(randomSmallRotation(σ))
        SR = S * R
        nga .= g.(Ref(SR) .* vs)
        newd = abs(dot(fa, nga))
        if newd > d
            ga .= nga
            d = newd
            R .= SR
        end
    end
    R, ga
end

function align(fs; res::Int = 10, N::Int = 100, M::Int = 1000, radius = 1)
    vs = gridinUnitBall(res) .* radius
    rots = [Array(randomRotation(3)) for i in 1:length(fs)]
    fas = [fs[i].(Ref(rots[i]) .* vs) for i in 1:length(fs)]

    prog = Progress(N * length(fs), "Global: ")
    for n in 1:N
        for i in shuffle(1:length(fs))
            sfa = sum([fas[k] for k in 1:length(fas) if k != i])
            rots[i], fas[i] = alignGlobal!(rots[i], sfa, fs[i], vs, M)
            next!(prog)
        end
    end

    prog = Progress(N * length(fs), "Local:  ")
    for n in 1:N
        for i in shuffle(1:length(fs))
            sfa = sum([fas[k] for k in 1:length(fas) if k != i])
            rots[i], fas[i] = alignLocal!(rots[i], sfa, fs[i], vs, M, 0.5)
            next!(prog)
        end
    end

    rots
end