### Paper 1: single structure with noise
- Bayesian single structure (crambin at 4A, coliphage)
- hierarchical sampling
- incoherent and background scattering
- polarization + detector shape
- hits v misses
- intensity fluctuations
- scaling noise
- scaling photon count

### Paper 2: ensemble with noise
- Bayesian ensemble (alanine dipeptide, chignolin)
- larger system?
- scaling ensemble vs gaussians
- scaling size

## Partition 1b
### Paper 1: single structure with noise
- Bayesian single structure (crambin at 4A, coliphage)
- hierarchical sampling
- incoherent and background scattering
- polarization + detector shape
- hits v misses
- intensity fluctuations


### Paper 2: ensemble with noise
- Bayesian ensemble (alanine dipeptide, chignolin)
- larger system?

### Paper 3: scaling
- scaling ensemble vs gaussians
- scaling noise
- scaling photon count
- scaling size



## Partition 2a
### Paper 1: ensemble noise-free
- Bayesian single structure (crambin at 4A, coliphage)
- Bayesian ensemble (alanine dipeptide, chignolin)
- hierarchical sampling
- scaling noise
- scaling photon count

### Paper 2: ensemble with noise
- incoherent and background scattering
- polarization + detector shape
- hits v misses
- intensity fluctuations
- scaling ensemble vs gaussians
- scaling size

## Partition 2b
### Paper 1: ensemble noise-free
- Bayesian single structure (crambin at 4A, coliphage)
- Bayesian ensemble (alanine dipeptide, chignolin)
- hierarchical sampling

### Paper 2: ensemble with noise
- incoherent and background scattering
- polarization + detector shape
- hits v misses
- intensity fluctuations

### Paper 3: scaling
- scaling ensemble vs gaussians
- scaling noise
- scaling photon count
- scaling size


## Partition 3
### Paper 1: single-structure with and without noise
- Bayesian single structure (crambin at 4A, coliphage)
- hierarchical sampling
- incoherent and background scattering
- polarization + detector shape
- intensity fluctuations
- scaling noise
- scaling photon count
- scaling size

### Paper 2: hits v misses
- hit selection not possible
- hits v misses
- scaling hitrate

### Paper 3: ensemble with noise
- Bayesian ensemble (alanine dipeptide, chignolin)
- scaling ensemble vs gaussians


## Partition 4
### Paper 1: single-structure only with noise
- Bayesian single structure (crambin at ~6A ?, coliphage)
- incoherent and background scattering
- polarization + detector shape
- intensity fluctuations
- scaling noise
- scaling photon count

### Paper 2: hits v misses
- hit selection not possible
- hits v misses
- scaling hitrate

### Paper 3: sampling method
- hierarchical sampling
- crambin at 4A
- ?

### Paper 4: ensemble with noise
- Bayesian ensemble (alanine dipeptide, chignolin)
- scaling ensemble vs gaussians
- scaling size