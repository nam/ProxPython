import Base.show

mutable struct AdaptiveProposer{T}
    proposer::T
    stepsize::Float64
    steplambda::Float64
    stepmin::Float64
    stepmax::Float64
    acceptancerate::Float64
end

function AdaptiveProposer(proposer; stepsize = 1.0, steplambda = 1.01, acceptancerate = 0.23, stepmin = 1e-10, stepmax = 10.0)
    AdaptiveProposer(proposer, stepsize, steplambda, stepmin, stepmax, acceptancerate)
end

function update!(p::AdaptiveProposer, accepted)
    if accepted
        p.stepsize *= p.steplambda ^ ((1 - p.acceptancerate) / p.acceptancerate)
    else
        p.stepsize /= p.steplambda
    end
    p.stepsize = clamp(p.stepsize, p.stepmin, p.stepmax)
end

function neighbor(proposer::AdaptiveProposer, f)
    neighbor(proposer.proposer, f, proposer.stepsize)
end

function logprop(proposer::AdaptiveProposer, f, g)
    logprop(proposer.proposer, f, g, proposer.stepsize)
end



Base.@kwdef mutable struct Annealer{T1, T2}
    nsteps::Int
    temperature::Float64
    tlambda::Float64
    configuration::T1
    proposers::T2

    energies::Vector{Float64}
    history::Vector{T1}
    histstep::Int

    function Annealer(configuration::T1, proposers::T2; temperature = 1.0, tlambda = 2^(-1/1e3), histstep = 1, thalf = 0.0) where {T1, T2}
        if thalf != 0
            tlambda = 2^(-1/thalf)
        end
        new{T1, T2}(0, temperature, tlambda, configuration, proposers, [], [], histstep)
    end
end

function currentEnergy(state::Annealer)
    !isempty(state.energies) ? state.energies[end] : Inf
end

function step!(state::Annealer, E, configuration; logpropdiff = 0.0)
    currentE = currentEnergy(state)
    deltaE = E - currentE

    if deltaE < 0 || exp(-(deltaE + logpropdiff) / state.temperature) > rand()
        state.configuration = configuration
        push!(state.energies, E)
        return true
    else
        push!(state.energies, currentE)
        return false
    end
end

function anneal!(state::Annealer, energy; output = s -> true, nsteps = 100, showprogress = true, targetTemp = 0.0, minTemp = 0.0, progress = nothing)
    if targetTemp > 0
        nsteps = round(Int, log(state.tlambda, targetTemp / state.temperature))
    end

    prog = if isnothing(progress)
        Progress(nsteps; showspeed = true)
    else
        progress
    end
    start = state.nsteps

    energies = if isa(energy, Tuple)
        energy
    else
        ntuple(x -> energy, length(state.proposers))
    end

    for i in 1:nsteps
        for j in 1:length(state.proposers)
            proposer = state.proposers[j]
            energy = energies[min(length(energies), j)]

            candidate = neighbor(proposer, state.configuration)
            isnew = candidate != state.configuration

            forward = logprop(proposer, candidate, state.configuration)
            backward = logprop(proposer, state.configuration, candidate)
            accepted = step!(state, energy(candidate), candidate, logpropdiff = forward - backward)
            update!(proposer, accepted && isnew)
        end

        state.nsteps += 1    
        state.temperature *= state.tlambda
        state.temperature = clamp(state.temperature, minTemp, Inf)
        
        if state.nsteps % state.histstep == 0
            push!(state.history, state.configuration)
        end

        if showprogress 
            next!(prog, showvalues = [
                (:stepsizes, round.([p.stepsize for p in state.proposers], sigdigits = 2)), 
                (:temperature, state.temperature), 
                (:nsteps, "$(state.nsteps)/$(start + nsteps)"), 
                (:accepted, round.(acceptanceRatio(state), sigdigits = 2)), 
                (:energy, currentEnergy(state))
            ])
        end
        
        if !output(state)
            return false
        end
    end
    return true
end

function replicaexchange!(states, energy; nsteps, output = s -> true, showprogress = true, progress = nothing)
    prog = if isnothing(progress)
        Progress(nsteps; showspeed = true)
    else
        progress
    end
    start = states[1].nsteps

    energies = if energy isa Function
        ntuple(x -> energy, length(states))
    else
        energy
    end

    nswaps = 0
    for n in 1:nsteps
        for (state, energy) in zip(states, energies)
            for j in 1:length(state.proposers)
                proposer = state.proposers[j]

                candidate = neighbor(proposer, state.configuration)
                isnew = candidate != state.configuration

                forward = logprop(proposer, candidate, state.configuration)
                backward = logprop(proposer, state.configuration, candidate)
                accepted = step!(state, energy(candidate), candidate, logpropdiff = forward - backward)
                update!(proposer, accepted && isnew)
            end

            state.nsteps += 1
            
            if state.nsteps % state.histstep == 0
                push!(state.history, state.configuration)
            end
        end

        for i in 1:length(states)-1
            if i % 2 == n % 2
                s1 = states[i]; s2 = states[i+1];
                T1 = s1.temperature; T2 = s2.temperature
                e11 = s1.energies[end]; e22 = s2.energies[end]
                e12, e21 = if allequal(energies)
                    e22, e11
                else
                    (energies[i](s2.configuration), energies[i+1](s1.configuration))
                end
                if rand() < exp((e11 - e12) / T1 + (e22 - e21) / T2)
                    s1.configuration, s2.configuration = s2.configuration, s1.configuration
                    s1.energies[end] = e12; s2.energies[end] = e21
                    nswaps += 1
                end
                # if rand() < exp((e1 - e2) * (1 / s1.temperature - 1 / s2.temperature))
                #     s1.configuration, s2.configuration = s2.configuration, s1.configuration
                #     s1.energies[end] = e2; s2.energies[end] = e1
                # end
            end
        end

        if showprogress 
            next!(prog, showvalues = [
                (:stepsizes, round.([s.proposers[1].stepsize for s in states], sigdigits = 2)), 
                (:temperature, round.(getfield.(states, :temperature), sigdigits = 2)), 
                (:accepted, round.(first.(acceptanceRatio.(states)), sigdigits = 2)), 
                (:nswaps, nswaps), 
            ])  
        end
        
        if !output(states)
            return false
        end
    end
    return true
end


# function show(io::IO, state::Annealer)
#     print(io, "AnnealingState(nsteps = $(state.nsteps), stepsize = $(state.stepsize), temperature = $(state.temperature))")
# end

function acceptanceRatio(state::Annealer; n = 1000)
    np = length(state.proposers)
    ne = length(state.energies)
    if ne < 2np return fill(NaN, np) end
    if n == 0
        n = length(state.energies) - 1
    end
    start = max(np, ne - n - 1)
    start -= start % np
    [mean(state.energies[i] != state.energies[i + 1] for i in start+j:np:ne-1) for j in 0:np-1]
end

function remainingSteps(state::Annealer, targetTemp = 1.0)
    round(Int, log(state.tlambda, targetTemp / state.temperature))
end

function multienergy(logp; offset = 0.0)
    p = Ref(logp([AtomVolume(1, 10.0, 1.0)], nothing))
    function lp1(fsws)
        p[] = logp(fsws[1], nothing)
        -p[](fsws[2]) - offset
    end
    function lp2(fsws)
        -p[](fsws[2]) - offset
    end
    (lp1, lp2)
end