import Base.length, Base.getindex

struct MixtureVolume{T}
    fs::T
end

function MixtureVolume(fs...)
    MixtureVolume(fs)
end

function cuda(V::MixtureVolume)
    MixtureVolume(cuda.(V.fs))
end

function getindex(V::MixtureVolume, i::Int)
    V.fs[i]
end

function Base.setindex(V::MixtureVolume, value, i::Int)
    MixtureVolume(setindex(V.fs, value, i))
end

function evaluate(V::MixtureVolume, P)
    X = evaluate(V[1], P)
    for f in V.fs[2:end]
        X .+= evaluate(f, P)
    end
    X
end

function evaluate!(X, V::MixtureVolume, P)
    evaluate!(X, V[1], P)
    for f in V.fs[2:end]
        X .+= evaluate(f, P)
    end
    X
end

function (V::MixtureVolume)(x)
    sum(f(x) for f in V.fs)
end

function l1(V::MixtureVolume)
    sum(l1(f) for f in V.fs)
end

function LinearAlgebra.normalize(f::MixtureVolume)
    f / l1(f)
end

function LinearAlgebra.normalize!(f::MixtureVolume)
    f.heights ./= l1(f)
    f
end

function length(V::MixtureVolume)
    length(V.fs)
end

Base.broadcastable(f::MixtureVolume) = Ref(f)


function envelope(V::MixtureVolume)
    envs = envelope.(V.fs)
    r -> sum(e(r) for e in envs)
end

Base.:*(a::Real, V::MixtureVolume) = MixtureVolume(a .* V.fs)
Base.:*(V::MixtureVolume, a::Real) = a * f
Base.:/(V::MixtureVolume, a::Real) = a^-1 * f