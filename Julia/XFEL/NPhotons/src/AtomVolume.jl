import Base.length, Base.getindex

struct AtomVolume{T,A <: AbstractVector{<:AbstractArray},B <: AbstractVector}
    positions::A
    heights::B
    widths::B
    radius::T

    function AtomVolume(positions, heights, widths, radius)
        # positions = positions .- Ref(mean(positions))
        new{typeof(radius),typeof(positions),typeof(widths)}(positions, heights, widths, radius)
    end
end

function AtomVolume(positions, heights, widths)
    AtomVolume(positions, heights, widths, maximum(norm, positions, init = 0.0) + 3maximum(widths, init = 0.0))
end

function Base.zero(TF::AtomVolume{T,A,B}) where {T, A, B}
    AtomVolume(A(), B(), B(), zero(T))
end

function emptyAtomVolume(radius = 1.0)
    AtomVolume(SVector{3,Float64}[], Float64[], Float64[], radius)
end

function AtomVolume(N::Int, σ::Real, radius = 1.0)
    positions = [SA[0.0, 0.0, 0.0] for i in 1:N]
    AtomVolume(positions, σ, radius)
end

function AtomVolume(positions::AbstractVector{<:AbstractArray}, σ::Real, radius = nothing)
    if isnothing(radius)
        radius = maximum(norm, positions) + 3σ
    end
    N = length(positions)
    AtomVolume(positions, fill(1 / N, N), fill(Float64(σ), N), radius)
end

function randomAtomVolume(N::Int, σ::Real, radius)
    positions = [SVector{3}(randfromSphere(3)) * radius for i in 1:N]
    positions .-= Ref(mean(positions))
    AtomVolume(positions, σ, radius)
end

function cuda(f::AtomVolume)
    cupositions = cu(SVector{3,Float32}.(f.positions))
    cuheights = cu(f.heights)
    cuwidths = cu(f.widths)
    cuf = AtomVolume(cupositions, cuheights, cuwidths, Float32(f.radius))
end

function CUDA.Adapt.adapt_structure(to::CUDA.CuArrayKernelAdaptor, f::AtomVolume)
    positions = CUDA.Adapt.adapt_structure(to, SVector{3, Float32}.(f.positions))
    heights = CUDA.Adapt.adapt_structure(to, f.heights)
    widths = CUDA.Adapt.adapt_structure(to, f.widths)
    AtomVolume(positions, heights, widths, Float32(f.radius))
end

function CUDA.Adapt.adapt_structure(to, f::AtomVolume)
    positions = CUDA.Adapt.adapt_structure(to, f.positions)
    heights = CUDA.Adapt.adapt_structure(to, f.heights)
    widths = CUDA.Adapt.adapt_structure(to, f.widths)
    AtomVolume(positions, heights, widths, f.radius)
end

function getindex(f::AtomVolume, i::Int)
    f.positions[i]
end

function getindex(f::AtomVolume, what::Symbol)
    x -> f(x, what)
end

function evaluateAtomVolume(positions, heights::AbstractArray{T}, widths, x)::T where T
    result = zero(Complex{T})
    dotxx = dot(x, x)
    if all(w == widths[1] for w in widths)
        for (p, h) in zip(positions, heights)
            result += h * cis(dot(x, p))
        end
        result *= exp(-widths[1]^2 * dotxx / 2)
    else
        for (p, h, σ) in zip(positions, heights, widths)
            result += @fastmath h * exp(complex(-σ^2 * dotxx / 2, dot(x, p)))
        end
    end
    abs2(result)
end

function ftgauss(σ, x, p)
    exp(complex(-σ^2 * dot(x, x) / 2, dot(x, p)))
end

function evaluateAtomVolumeComplex(positions, heights::AbstractArray{T}, widths, x)::Complex{T} where T
    result = zero(Complex{T})
    dotxx = dot(x, x)
    for (p, h, σ) in zip(positions, heights, widths)
        result += h * exp(complex(-σ^2 * dotxx / 2, dot(x, p)))
    end
    result
end

function toMatrix(f::AtomVolume, X)
    ftgauss.(f.widths, reshape(X, 1, size(X)...), f.positions)
end

function evaluate(f::AtomVolume, P)
    evaluateAtomVolume.(Ref(f.positions), Ref(f.heights), Ref(f.widths), P)
end

function evaluate!(X, f::AtomVolume, P)
    X .= evaluateAtomVolume.(Ref(f.positions), Ref(f.heights), Ref(f.widths), P)
end

function evaluate_complex(f::AtomVolume, P)
    evaluateAtomVolumeComplex.(Ref(f.positions), Ref(f.heights), Ref(f.widths), P)
end

function evaluate_complex!(X, f::AtomVolume, P)
    X .= evaluateAtomVolumeComplex.(Ref(f.positions), Ref(f.heights), Ref(f.widths), P)
end

function (f::AtomVolume)(x, y, z, args...)
    f(SA[x, y, z], args...)
end

function (f::AtomVolume)(x)
    evaluateAtomVolume(f.positions, f.heights, f.widths, x)
end

function (f::AtomVolume)(x, what)
    if what == :real
        result = 0.0
        for (p, h, σ) in zip(f.positions, f.heights, f.widths)
            b = 1 / (2σ^2) * sqeuclidean(x, p)
            if b < 5
                result += h * σ^-3 * (exp(-b) - exp(-5))
            end
        end
        return result / (2pi)^(3 / 2)
    elseif what == :fourier
        return f(x)
    elseif what == :complex 
        result = complex(0.0, 0.0)
        dotxx = dot(x, x)
        for (p, h, σ) in zip(f.positions, f.heights, f.widths)
            result += h * exp(complex(-σ^2 * dotxx / 2, dot(x, p)))
        end
        result
    else
        throw(ArgumentError("Expected :real, :fourier or :complex"))
    end
end

function realspace(f::AtomVolume)
    x -> f(x, :real)
end

function gradient(f::AtomVolume, i, k)
    dotkk = dot(k, k)

    yi = f.positions[i]; hi = f.heights[i]; σi = f.widths[i]

    result = zero(Complex{eltype(f.heights)})
    if false#all(w == f.widths[1] for w in f.widths)
        for (yj, hj) in zip(f.positions, f.heights)
            result += hj * cis(dot(k, yi - yj))
        end
        result *= exp(-f.widths[1]^2 * dotkk)
    else
        for (yj, hj, σj) in zip(f.positions, f.heights, f.widths)
            result += hj * exp(complex(-(σi^2+σj^2) * dotkk / 2, dot(k, yi - yj)))
        end
    end
    k * real(2im * hi * result)
end

function l1(f::AtomVolume)
    sum(f.heights)
end

function LinearAlgebra.normalize(f::AtomVolume)
    f / l1(f)
end

function LinearAlgebra.normalize!(f::AtomVolume)
    f.heights ./= l1(f)
    f
end

function scatteringProbability(f::AtomVolume; λ = 2.5, qmax = Inf)
    result = 0.0
    r = min(2pi / λ, qmax / 2)
    for (p1, h1, σ1) in zip(f.positions, f.heights, f.widths)
        for (p2, h2, σ2) in zip(f.positions, f.heights, f.widths)
            k = norm(p1 - p2)
            s = sqrt(σ1^2 + σ2^2)
            if k <= 1e-5
                result += h1 * h2 * (1 - exp(-2*r^2*s^2)) / s^2
            else
                x = 2dawson(k / (√2 * s))
                x -= dawson((k - 2im * s^2 * r) / (√2 * s)) * exp(((k - 2im * s^2 * r)^2 - k^2) / (2s^2))
                x -= dawson((k + 2im * s^2 * r) / (√2 * s)) * exp(((k + 2im * s^2 * r)^2 - k^2) / (2s^2))
                result += real(h1 * h2 * x * sqrt(2) / (2k * s))
            end
        end
    end
    result * 2pi
end

function normalizeScattering(f::AtomVolume; nphotons = 1, λ = 2.5, qmax = Inf)
    f * sqrt(nphotons / scatteringProbability(f, λ = λ, qmax = qmax))
end


function envelope(f::AtomVolume)
    o = SA[0.0,0.0,0.0]
    envf = AtomVolume([o], [1.0], [minimum(f.widths)], f.radius)
    envf.heights .*= sqrt(f(o) / envf(o))
    r -> envf(SA[r,0.0,0.0])
end

function normalEnvelope(f::AtomVolume; dims = 3)
    MvNormal(fill(0.0, dims), 1 / (√2 * minimum(f.widths)))
end

function upperBound(f::AtomVolume, r)
    result = 0.0
    a = r^2 / 2
    for (h, σ) in zip(f.heights, f.widths)
        result += h * exp(-σ^2 * a)
    end
    abs2(result)
end

function smoothen(f::AtomVolume, σ)
    newf = deepcopy(f)
    newf.widths .= sqrt.(σ^2 .+ f.widths.^2);
    newf
end

function sharpen(f::AtomVolume, σ)
    newf = deepcopy(f)
    newf.widths .= sqrt.(f.widths.^2 .- σ^2);
    newf
end

function length(f::AtomVolume)
    return length(f.positions)
end

Base.broadcastable(f::AtomVolume) = Ref(f)

function Base.show(io::IO, f::AtomVolume)
    compact = get(io, :compact, false)

    println(io, "$(length(f))-dim $(typeof(f)) with radius $(f.radius)")
    println(io, "Positions: ", [round.(p, digits = 2) for p in Array(f.positions)])
    println(io, "Heights: ", round.(Array(f.heights), digits = 3))
    print(io, "Widths: ", round.(Array(f.widths), digits = 3))
end

function Base.:+(f::AtomVolume, g::AtomVolume)
    AtomVolume(vcat(f.positions, g.positions), vcat(f.heights, g.heights), vcat(f.widths, g.widths), max(f.radius, g.radius))
end

function rotate(f::AtomVolume, R)
    newf = deepcopy(f)
    for i in eachindex(f.positions)
        newf.positions[i] = R * f.positions[i]
    end
    newf
end

Base.:*(R, f::AtomVolume) = rotate(f, R)
Base.:*(a::Real, f::AtomVolume) = (g = deepcopy(f); g.heights .*= a; g)
Base.:*(f::AtomVolume, a::Real) = a * f
Base.:/(f::AtomVolume, a::Real) = a^-1 * f


function alignN(f::AtomVolume, g::AtomVolume; ntries = 1000, showprogress = false)
    fpos = [p for (h, p) in zip(f.heights, f.positions)]# if h > 0.1 * maximum(f.heights)]
    gpos = [p for (h, p) in zip(g.heights, g.positions)]# if h > 0.1 * maximum(g.heights)]
    ftree = KDTree(fpos)
    gtree = KDTree(gpos)

    cost(R, inds) = sum(norm(R * fpos[i1] .- gpos[i2]) for (i1, i2) in inds)

    R = SMatrix{3, 3, Float64, 9}(randomRotation(3))
    dR = cost(R, pointCloudMatch(Ref(R) .* fpos, gpos))

    for n in 1:1000
        inds = pointCloudMatch(Ref(R) .* fpos, gpos)
        for i in 1:ntries
            S = SMatrix{3, 3, Float64, 9}(randomRotation(3)) * ifelse(rand() < 0.5, 1, -1)
            dS = cost(S, inds)
            if dS < dR
                R = S
                dR = dS
            end
        end
        for i in 1:ntries
            S = randomSmallRotation(0.5)
            dS = cost(R * S, inds)
            if dS < dR
                R = R * S
                dR = dS
            end
        end
    end
    R
end

function align(f::AtomVolume, g::AtomVolume; ntries = 1000, mirror = 0.5, full = true, σ = 0.5, showprogress = false, rng = Random.GLOBAL_RNG)
    fpos = [p for (h, p) in zip(f.heights, f.positions) if h > 0.2 * maximum(f.heights)]
    gpos = [p for (h, p) in zip(g.heights, g.positions) if h > 0.2 * maximum(g.heights)]

    dists = zeros(length(fpos), length(gpos))
    dX = zeros(length(fpos))
    dY = zeros(length(gpos))

    R = SMatrix{3, 3, Float64, 9}(I)
    Rfpos = Ref(R) .* fpos
    dR = pointCloudDistance!(dists, dX, dY, (Rfpos .= Ref(R) .* fpos), gpos)

    prog = Progress(2 * ntries)
    
    if full
        for i in 1:ntries
            S = SMatrix{3, 3, Float64, 9}(randomRotation(rng))
            if rand(rng) < mirror
                S *= -1
            end
            dS = pointCloudDistance!(dists, dX, dY, (Rfpos .= Ref(S) .* fpos), gpos)
            if dS < dR
                R = S
                dR = dS
            end
            showprogress && next!(prog)
        end
    end
    
    for i in 1:ntries
        S = randomSmallRotation(σ, rng)
        dS = pointCloudDistance!(dists, dX, dY, (Rfpos .= Ref(R * S) .* fpos), gpos)
        if dS < dR
            R = R * S
            dR = dS
        end
        showprogress && next!(prog)
    end
    
    R
end

function aligned(f::AtomVolume, g::AtomVolume; kwargs...)
    newf = deepcopy(f)
    newf.positions .-= Ref(mean(newf.positions))
    align(newf, g; kwargs...) * newf
end
