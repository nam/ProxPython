function atomProposer(g, σ = 0.0)
    gnormals = [MvNormal(g.positions[i], sqrt(g.widths[i]^2 + σ^2)) for i in 1:length(g)]
    
    function neighbor(f, d)
        fnormals = [gnormals[ceil(Int, length(g) / length(f) * i)] for i in 1:length(f)]
        normals = [normalprod(fnormals[i], MvNormal(f.positions[i], d)) for i in 1:length(f)]

        newf = deepcopy(f)
        newf.positions .= [rand(n) for n in normals]
        newf.positions .-= Ref(mean(newf.positions))
        newf
    end

    function logprop(f_to, f_from, d)
        fnormals = [gnormals[ceil(Int, length(g) / length(f_from) * i)] for i in 1:length(f_from)]
        normals = [normalprod(fnormals[i], MvNormal(f_from.positions[i], d)) for i in 1:length(f_from)]

        sum(log.(pdf.(normals, f_to.positions)))
    end

    neighbor, logprop
end

function atomProposer(gs::Array, σ = 0.0)
    proposers = atomProposer.(gs, σ);
    neighbors = getindex.(proposers, 1)
    logprops = getindex.(proposers, 2)

    neighbor(fs, d) = [n(f, d) for (n, f) in zip(neighbors, fs)]
    logprop(fs, gs, d) = sum(l(f, g, d) for (l, f, g) in zip(logprops, fs, gs))

    neighbor, logprop
end

function combinedProposer(neighbor, logprop, wd; wprior = x -> 1.0)
    function wneighbor(fws, d)
        fs, ws = fws
        newfs = neighbor(fs, d)
        for i in 1:100000
            news = ws .+ wd .* d .* (x = randn(length(ws)); x .- mean(x))
            if all(news .> 0) && rand() < wprior(news) / wprior(ws)
                ws = news
                break
            end
        end
        normalize!(ws, 1)
        ws = clamp.(ws, 0, 1)
        (newfs, ws)
    end
    function wlogprop(fws, gws, d)
        logprop(fws[1], gws[1], d)
    end
    wneighbor, wlogprop
end

function weightProposer(; wprior = x -> 1.0)
    function neighbor(fws, d)
        fs, ws = fws
        newfs = deepcopy(fs)
        for i in 1:100000
            news = ws .+ d .* (x = randn(length(ws)); x .- mean(x))
            if all(0 .< news .< 1) && rand() < wprior(news) / wprior(ws)
                ws = news
                break
            end
        end
        normalize!(ws, 1)
        ws = clamp.(ws, 0, 1)
        (newfs, ws)
    end
    function logprop(fws, gws, d)
        log(wprior(fws[2])) - log(wprior(gws[2]))
    end
    neighbor, logprop
end



# function atomProposerWD(g, σ)
#     gnormals = [MvNormal(g.positions[i], sqrt(g.widths[i]^2 + σ^2)) for i in 1:length(g)]
    
#     function neighbor(f, d)
#         dists = normalize(minimum(pairwise(Euclidean(), f.positions) + Inf * I, dims = 2)[:], 1) * length(f)
        
#         fnormals = [gnormals[ceil(Int, length(g) / length(f) * i)] for i in 1:length(f)]
#         normals = [normalprod(fnormals[i], MvNormal(f.positions[i], d * dists[i])) for i in 1:length(f)]

#         newf = deepcopy(f)
#         newf.positions .= [rand(n) for n in normals]
#         newf.positions .-= Ref(mean(newf.positions))
#         newf
#     end

#     function logprop(f_to, f_from, d)
#         dists = normalize(minimum(pairwise(Euclidean(), f_from.positions) + Inf * I, dims = 2)[:], 1) * length(f_from)
        
#         fnormals = [gnormals[ceil(Int, length(g) / length(f_from) * i)] for i in 1:length(f_from)]
#         normals = [normalprod(fnormals[i], MvNormal(f_from.positions[i], d * dists[i])) for i in 1:length(f_from)]

#         sum(log.(pdf.(normals, f_to.positions)))
#     end

#     neighbor, logprop
# end

# function atomProposerWT(g, σ)
#     gnormals = [MvNormal(g.positions[i], sqrt(g.widths[i]^2 + σ^2)) for i in 1:length(g)]
    
#     function neighbor(f, d)
#         pt = 0.1/length(f)
#         fnormals = [gnormals[ceil(Int, length(g) / length(f) * i)] for i in 1:length(f)]
#         normals = [normalprod(fnormals[i], MvNormal(f.positions[i], d)) for i in 1:length(f)]
#         mixtures = [MixtureModel([normals[i], fnormals[i]], [1 - pt, pt]) for i in 1:length(f)]

#         newf = deepcopy(f)
#         newf.positions .= [rand(n) for n in mixtures]
#         newf.positions .-= Ref(mean(newf.positions))
#         newf
#     end

#     function logprop(f_to, f_from, d)
#         pt = 0.1/length(f_from)
#         fnormals = [gnormals[ceil(Int, length(g) / length(f_from) * i)] for i in 1:length(f_from)]
#         normals = [normalprod(fnormals[i], MvNormal(f_from.positions[i], d)) for i in 1:length(f_from)]
#         mixtures = [MixtureModel([normals[i], fnormals[i]], [1 - pt, pt]) for i in 1:length(f_from)]

#         sum(log.(pdf.(mixtures, f_to.positions)))
#     end

#     neighbor, logprop
# end

# function atomProposerFew(g, σ)
#     gnormals = [MvNormal(g.positions[i], sqrt(g.widths[i]^2 + σ^2)) for i in 1:length(g)]
    
#     function neighbor(f, d)
#         p = clamp(d, 3 / length(f), 1)
#         d -= p - 1
 
#         fnormals = [gnormals[ceil(Int, length(g) / length(f) * i)] for i in 1:length(f)]
#         normals = [normalprod(fnormals[i], MvNormal(f.positions[i], d)) for i in 1:length(f)]
#         mixtures = [MixtureModel([MvNormal(f.positions[i], 0.0), normals[i]], [1 - p, p]) for i in 1:length(f)]

#         newf = deepcopy(f)
#         newf.positions .= [rand(m) for m in mixtures]
#         newf.positions .-= Ref(mean(newf.positions))
#         newf
#     end

#     function logprop(f_to, f_from, d)
#         p = clamp(d, 3 / length(f_from), 1)
#         d -= p - 1

#         fnormals = [gnormals[ceil(Int, length(g) / length(f_from) * i)] for i in 1:length(f_from)]
#         normals = [normalprod(fnormals[i], MvNormal(f_from.positions[i], d)) for i in 1:length(f_from)]

#         sum(log.(pdf.(normals, f_to.positions)))
#     end

#     neighbor, logprop
# end

# function mcProposer(logp, baseneighbor)
#     function neighbor(f, d)
#         logpf = logp(f)
#         g = baseneighbor(f, d)
#         logpg = logp(g)
#         if logpg > logpf || rand() < exp((logpg - logpf))
#             return g
#         end
#         return f
#     end

#     function logprop(f_to, f_from, d)
#         min(0.0, logp(f_to) - logp(f_from))
#     end

#     neighbor, logprop
# end

# function densityProposer(g, σ; fac = 1.0)
#     sg = smoothen(g, σ)

#     function neighbor(f, d)
#         newf = deepcopy(f)
#         for i in 1:length(f)
#             while true
#                 p = f.positions[i] .+ (d * ifelse(rand() < 0.01, 20, 1)) .* randn.()
#                 if rand() < sg(p, :real) * fac
#                     newf.positions[i] = p
#                     break
#                 end
#             end 
#         end
#         newf
#     end

#     function logprop(f)
#         sum(log, min(1.0, sg(p, :real) * fac) for p in f.positions)
#     end

#     return neighbor, logprop
# end

# function onoffProposer(g, σ)
#     sg = smoothen(g, σ)
#     gmax = maximum(g.(g.radius .* randn(SVector{3,Float64}, 100000), :real))

#     function neighbor(f, d)
#         d = clamp(d, 1 / length(f), 0.5)
#         newf = deepcopy(f)
#         for i in 1:length(f)
#             sgf = clamp(sg(f.positions[i], :real) / gmax, 0, 1)
#             if newf.heights[i] == 0
#                 if rand() < sgf * d
#                     newf.heights[i] = 1
#                 else
#                     newf.heights[i] = 0
#                 end
#             else
#                 if rand() < (1 - sgf) * d
#                     newf.heights[i] = 0
#                 else
#                     newf.heights[i] = 1
#                 end
#             end
#         end
#         newf.heights .*= l1(f) / l1(newf)
#         newf
#     end

#     function logprop(f)
#         result = 0.0
#         for i in 1:length(f)
#             sgf = clamp(sg(f.positions[i], :real) / gmax, 0, 1)
#             if f.heights[i] == 0
#                 result += log(1 - sgf)
#             else
#                 result += log(sgf)
#             end
#         end
#         result
#     end

#     return neighbor, logprop
# end

# function onoffProposer(g, f, σ)
#     sg = smoothen(g, σ)
#     P = sg.(f.positions, :real)
#     P ./= maximum(P)

#     function neighbor(f, d)
#         d = clamp(d, 1 / length(f), 0.5)
#         newf = deepcopy(f)
#         for i in 1:length(f)
#             if newf.heights[i] == 0
#                 newf.heights[i] = ifelse(rand() < P[i] * d, 1, 0)
#             else
#                 newf.heights[i] = ifelse(rand() < (1 - P[i]) * d, 0, 1)
#             end
#         end
#         newf.heights .*= l1(f) / l1(newf)
#         newf
#     end

#     function logprop(f)
#         result = 0.0
#         for i in 1:length(f)
#             if f.heights[i] == 0
#                 result += log(1 - P[i])
#             else
#                 result += log(P[i])
#             end
#         end
#         result
#     end

#     return neighbor, logprop
# end

