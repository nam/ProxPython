function kernel_dense!(X, XI, R, I, N, V, M, rotationWeights)
    # X .*= mean(I) / mean(X)
    V .= X .+ N

    # lp = -0.5f0 .* (reshape(I, size(I, 1), 1, :) .- X).^2 ./ V .- 0.5f0 .* log.(6.283185f0 .* V)
    # XI = dropdims(sum(lp, dims = 1), dims = 1)
    XI .= sum(X.^2 ./ V, dims = 1)'
    mul!(XI, (1 ./ V)', I.^2, 1f0, 1f0) # XI .+= (1 ./ V)' * (I .^ 2)
    mul!(XI, (X ./ V)', I, -2f0, 1f0) # XI .-= 2 .* (X ./ V)' * I
    XI .= -1 .* XI ./ 2 .- sum(log.(V), dims = 1)' ./ 2 .- log(6.283185f0) * size(V, 1) / 2
    
    maximum!(M, XI)
    XI .= exp.(XI .- M)
    mul!(R, XI', rotationWeights)
    R .= log.(R) .+ reshape(M, :)

    return Array(R)
end

function setupDenseKernel(images, pixelWeights, rotationWeights, addvar; usecuda = true)
    npix = length(pixelWeights)
    nrot = length(rotationWeights)
    nimg = length(images)
    
    I = reduce(hcat, images)
    XI = zeros(nrot, nimg)
    R = zeros(nimg)
    if isa(addvar, Real)
        addvar = fill(addvar, npix)
    end
    N = reshape(addvar, :, 1)
    V = zeros(npix, nrot)
    M = zeros(1, nimg)

    if usecuda
        cuXI = cu(XI); cuR = cu(R); cuI = cu(I); cuN = cu(N); 
        cuV = cu(V); cuM = cu(M); curotationWeights = cu(rotationWeights)
        logp(X) = kernel_dense!(X, cuXI, cuR, cuI, cuN, cuV, cuM, curotationWeights)
    else
        logpCPU(X) = kernel_dense!(X, XI, R, I, N, V, M, rotationWeights)
    end
end


function eval!(X, Y, f, points, pixelWeights)
    if size(X) == size(Y)
        evaluate!(X, f, points)
    else
        evaluate!(Y, f, points)
        mean!(reshape(X, size(X, 1), 1, size(X, 2)), Y)
    end
    X .*= pixelWeights
    X
end

function setupEval(rotations, pixels, pixelWeights; usecuda = true)
    X = zeros(size(pixels, 1), length(rotations))
    Y = zeros(size(pixels)..., length(rotations))
    points = SVector{3, Float32}.([R * x for x in pixels, R in rotations])

    if usecuda
        cuX = cu(X); cuY = cu(Y); cupoints = cu(points); cupixelWeights = cu(pixelWeights)
        ev(f) = eval!(cuX, cuY, cuda(f), cupoints, cupixelWeights)
    else
        evCPU(f) = eval!(X, Y, f, points, pixelWeights)
    end
end



function logpsClosureDense(images, rotations, pixels, pixelWeights, rotationWeights, addvar; usecuda = true, normalize = false)
    logpnoeval = setupDenseKernel(images, pixelWeights, rotationWeights, addvar, usecuda = usecuda)
    eval = setupEval(rotations, pixels, pixelWeights, usecuda = usecuda)

    mi = mean(mean, images)
    function logp(f)
        X = eval(f)
        if normalize
            X .*= mi / mean(X)
        end
        logpnoeval(X)
    end
end

function logpClosureDenseLSE(images, rotations, pixels, pixelWeights, rotationWeights, addvar; usecuda = true)
    logpnoeval = setupDenseKernel(images, pixelWeights, rotationWeights, addvar, usecuda = usecuda)
    points = cu(SVector{3, Float32}.([R * x for x in pixels, R in rotations]))

    function logp(center, shells, weights = 1.0)
        Y = evaluate_complex(cuda(center), points)
    
        ps = zeros(length(images), length(shells))
        
        s = VolumeWithShell(Float32, shells[1])
        X = s.height .* ftshell.(norm.(points), s.r1, s.r2) .* exp.(-s.smooth^2/2 .* norm.(points).^2)
        
        for i in 1:length(shells)
            s = VolumeWithShell(Float32, shells[i])
            X .= s.height .* ftshell.(norm.(points), s.r1, s.r2) .* exp.(-s.smooth^2/2 .* norm.(points).^2)
            X .= abs2.(X .+ Y)
            ps[:,i] .= logpnoeval(X)
        end

        ws = if isa(weights, Real)
            fill(1/length(shells), length(shells))
        else
            weights
        end

        ps[isnan.(ps)] .= -Inf
        logsumexp(ws' .+ ps, dims = 2)
    end
end

function logpClosureDense(args...; kwargs...)
    logps = logpsClosureDense(args...; kwargs...)
    logp(f) = sum(logps(f))
end

# function logpClosureDenseLSE(args...; kwargs...)
#     logps = logpsClosureDense(args...; kwargs...)
#     function logp(fs, weights = 1.0)
#         ws = if isa(weights, Real)
#             fill(1/length(fs), length(fs))
#         else
#             weights
#         end
#         ps = reduce(hcat, logps.(fs))
#         ps[isnan.(ps)] .= -Inf
#         sum(logsumexp(ws' .+ ps, dims = 2))
#     end
# end