using Distributed, ClusterManagers, LinearAlgebra, Statistics, Rotations, ProgressMeter, Random, JLD2, StaticArrays, Dates, FileIO, DelimitedFiles, ArgParse, Glob, StatsBase, Printf

const starttime = now()

function printlog(x...)
    println(now(), " ", join(x, " ")...)
    flush(stdout)
end



function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table! s begin
        "--path", "-p"
            default = ""
        "--glob", "-g"
            default = "*"
    end

    return parse_args(s)
end

const args = parse_commandline()
const path = args["path"]
const outpath = joinpath(path, "out")
mkpath(outpath)

files = []
for f in glob(joinpath(path, args["glob"]))
    if occursin(".jld2", f)
        outfile = joinpath(outpath, splitdir(f)[2])
        if !(isfile(outfile) && load(outfile, "finished"))
            push!(files, f)
        end
    end
end

const infiles = [f for f in files]
const outfiles = [joinpath(outpath, splitdir(f)[2]) for f in files]

printlog("Found $(length(infiles)) unfinished input $(length(infiles) > 1 ? "files" : "file")")

if length(infiles) == 0
    exit()
end

using CUDA

if haskey(ENV, "SLURM_NTASKS")
    addprocs_slurm(parse(Int, ENV["SLURM_NTASKS"]) - nprocs() + 1, gpus = 0, exeflags = "--threads=1", job_file_loc = "joboutput", retry_delays = ExponentialBackOff(n=10, first_delay=1, max_delay=10, factor=2));
else
    addprocs(length(devices()))
end

@everywhere push!(LOAD_PATH, "/home/sschult/Documents/xfel")
@everywhere begin
    using CUDA
    CUDA.allowscalar(false)
    import NPhotons as nph
end

asyncmap(workers()) do pid
    remotecall_wait(pid) do
        device!(myid() % length(devices()))
    end
end;
printlog.(fetch.([@spawnat w "$(gethostname()): $(name(device())) $(device().handle)" for w in workers()]))




@everywhere function setlogp(images, data, N; kwargs...)
    global logp = nph.logpClosureConf(images, data, N; kwargs...)
    global multilogp = nph.multienergy(logp)
    return true
end

function setup(images, data, N; kwargs...)
    @sync for worker in workers()
        selected = images[worker-1:nworkers():end]
        @spawnat worker setlogp(selected, data, N; kwargs...)
    end
end

function logpDist(f)
    sum(fetch.([@async @fetchfrom worker logp(f) for worker in workers()]))
end

function logpMDist(f, i)
    sum(fetch.([@async @fetchfrom worker -multilogp[i](f) for worker in workers()]))
end

function perfDist()
    mean(fetch.([@spawnat worker logp.etimes for worker in workers()]))
end



function main()
    for (infile, outfile) in zip(infiles, outfiles)
        try
            printlog("Loading inputs from $(infile)")
            
            @load infile object images N state integrationData rmask nmask targetTemp

            if isfile(outfile)
                printlog("Resuming previous run from $(outfile)")
                @load outfile state
            end

            setup(images, integrationData, N, rmask = rmask, nmask = nmask)

            objectEnergy = -logpDist(object)

            nsteps = nph.remainingSteps(state, targetTemp = targetTemp)
            totalsteps = state.nsteps + nsteps
            try 
                @load infile totalsteps
            catch e
                if !isa(e, KeyError)
                    rethrow(e)
                end
            end
            nsteps = totalsteps - state.nsteps

            printlog("$(nsteps) steps remaining")

            function output_func()
                tsave = now()
                tlog = now()
                t0 = time_ns()

                function output(state)
                    if now() - tlog > Minute(1)
                        t = (time_ns() - t0) / 1e9
                        pd = perfDist()
                        ts = round.([pd; t - sum(pd); t], sigdigits = 2)
                        trem = round(Int, (totalsteps - state.nsteps) * t / 60)
                        printlog("$(state.nsteps) / $(totalsteps)  ts = $(ts)  E = $(@sprintf("%.1e", state.energies[end]))  ETA: $(trem ÷ 60):$(lpad(trem % 60, 2, '0'))")
                        tlog = now()
                    end

                    if now() - tsave > Minute(60)
                        t = @elapsed jldsave(outfile, state = state, finished = false)
                        printlog("Saved to $(outfile) in $(round(t, sigdigits = 3)) seconds.")
                        tsave = now()
                    end

                    t0 = time_ns()
                    
                    if now() - starttime > Minute(24 * 60 - 15)
                        return false
                    end
                    return true
                end
            end

            efuncs = ((f) -> -logpMDist(f, 1) - objectEnergy, (f) -> -logpMDist(f, 2) - objectEnergy)

            finished = nph.anneal!(state, efuncs, output = output_func(), minTemp = targetTemp, nsteps = nsteps, showprogress = false)

            jldsave(outfile, state = state, finished = finished)

            if finished 
                printlog("Finished processing $(infile)")
            else
                printlog("Pausing while processing $(infile)")
            end

            if now() - starttime > Minute(24 * 60 - 15)
                break
            end
        catch e
            printlog("$e while processing $(infile)")
            showerror(stdout, e, catch_backtrace())
        end
    end
end

main()
