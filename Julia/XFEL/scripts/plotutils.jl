using NPhotons
using Colors, StaticArrays, LinearAlgebra, StatsBase

function printM(args...; newline = true, clear = false)
    pargs = collect(Iterators.flatten(zip(args, [", " for i in 1:length(args) - 1])))
    push!(pargs, args[end])
    if newline
        println(pargs...)
    else
        print(pargs...)
    end
    # flush(stdout)
end

function printProgress(args...)
    IJulia.clear_output(true)
    printM(args...; newline = false)
    flush(stdout)
end

function tukey_fences(data; k=1.5)
    q1,q3 = quantile(data,[0.25,0.75])
    iqr = q3-q1
    fence = k*iqr
    q1-fence, q3+fence
end

function monitorPlot(lvol::nph.AtomVolume = nph.AtomVolume(1, 1.0), rvol = nph.AtomVolume(1, 1.0), energies = [0.0]; resolution = (1000, 1000), escale = identity, algorithm = :iso, isovalue = 0.5, isorange = 0.46, trim = false, kwargs...)
    fig = Figure(resolution = (1000, 1000))
    
    eplot = Axis(fig[2,1:2])
    enode = Node(energies)
    scene = lines!(eplot, lift(x -> escale.(x), enode), color=:blue)
    hlines!(eplot, [0.0], color=:orange);
        
    X = collect(range(-1.0, 1.0, length = 50))
    grid = SVector{3, Float64}.(collect(Iterators.product(X, X, X)));
    lf(f) = normalize(f.(grid .* f.radius, :real), Inf)
    
    lscene1 = LScene(fig[1,1])
    lnode = Node(lvol)
    vol1 = volume!(lscene1.scene, X, X, X, lift(lf, lnode); algorithm = algorithm, isovalue = isovalue, isorange = isorange, colorrange = (isovalue - isorange, isovalue + isorange), show_axis=false, kwargs...)
    
    lscene2 = LScene(fig[1,2])
    rnode = Node(rvol)
    vol2 = volume!(lscene2.scene, X, X, X, lift(lf, rnode); algorithm = algorithm, isovalue = isovalue, isorange = isorange, colorrange = (isovalue - isorange, isovalue + isorange), show_axis=false, kwargs...)
    
    linkCameras!(lscene1.scene, lscene2.scene)
    
    cameracontrols(vol1).rotationspeed[] = 0.02
    cameracontrols(vol2).rotationspeed[] = 0.02
    cameracontrols(vol1).fov[] *= 0.7

    display(fig)

    function update(;left = nothing, right = nothing, bottom = nothing)
        !isnothing(left) && (lnode[] = left)
        !isnothing(right) && (rnode[] = right)
        # !isnothing(bottom) && (enode[] = bottom)
        # !isnothing(bottom) && ylims!(eplot, high = partialsort(bottom, ceil(Int, 0.03length(bottom)), rev = true) + 0.05 * (maximum(bottom) - min(0, minimum(bottom))))
        if !isnothing(bottom)
            enode[] = bottom
            reset_limits!(eplot)
            if trim && !isempty(bottom)
                high = maximum(bottom[ceil(Int, 0.2length(bottom)):end])
                low = min(0, minimum(bottom))
                ylims!(eplot, low = low - 0.1 * (high - low), high = high + 0.1 * (high - low))
            end
        end
        nothing
    end
    fig, update
end

function monitorPlot(lvol, rvol = nph.AtomVolume(1, 1.0), energies = [0.0]; resolution = (1000, 1000), escale = identity, algorithm = :iso, isovalue = 0.5, isorange = 0.46, kwargs...)
    fig = Figure(resolution = (1000, 1000))
    
    eplot = Axis(fig[2,1:2])
    enode = Node(energies)
    scene = lines!(eplot, lift(x -> escale.(x), enode), color=:blue)
    hlines!(eplot, [0.0], color=:orange);
        
    X = collect(range(-1.0, 1.0, length = 50))
    grid = SVector{3, Float64}.(collect(Iterators.product(X, X, X)));
    lf(f) = normalize(f.(grid .* maximum(norm.(f.positions))), Inf) .^ 0.2
    
    lscene1 = LScene(fig[1,1])
    lnode = Node(lvol)
    vol1 = volume!(lscene1.scene, X, X, X, lift(lf, lnode); algorithm = algorithm, isovalue = isovalue, isorange = isorange, colorrange = (isovalue - isorange, isovalue + isorange), show_axis=false, kwargs...)
    
    lscene2 = LScene(fig[1,2])
    rnode = Node(rvol)
    vol2 = volume!(lscene2.scene, X, X, X, lift(lf, rnode); algorithm = algorithm, isovalue = isovalue, isorange = isorange, colorrange = (isovalue - isorange, isovalue + isorange), show_axis=false, kwargs...)
    
    linkCameras!(lscene1.scene, lscene2.scene)
    
    cameracontrols(vol1).rotationspeed[] = 0.02
    cameracontrols(vol2).rotationspeed[] = 0.02
    cameracontrols(vol1).fov[] *= 0.7
    display(fig)

    function update(;left = nothing, right = nothing, bottom = nothing)
        !isnothing(left) && (lnode[] = left)
        !isnothing(right) && (rnode[] = right)
        !isnothing(bottom) && (enode[] = bottom)
        autolimits!(eplot)
        # !isnothing(bottom) && ylims!(eplot, high = tukey_fences(bottom,k = 0.75)[2])
        nothing
    end
    fig, update
end

function volCompare(vols...; radius = 1.0, resolution = (1000, 1000), escale = identity, algorithm = :iso, isovalue = 0.5, isorange = 0.46, kwargs...)
    ncols = ceil(Int, sqrt(length(vols)))
    nrows = ceil(Int, length(vols) / ncols)

    fac = round(Int, clamp(1400 / (max(nrows, ncols) * 500), 0, 1) * 500)
    fig = Figure(resolution = (fac * ncols, fac * nrows))
    
    X = collect(range(-1.3, 1.3, length = 50))
    grid = radius .* SVector{3, Float64}.(collect(Iterators.product(X, X, X)));

    lf(f) = normalize(f.(grid), Inf)
    
    lscenes = []
    plts = []
    
    for (i, vol) in enumerate(vols)
        lscene = LScene(fig[(i-1)÷ncols+1, mod1(i, ncols)])
        algorithm == :mip && (lscene.scene.backgroundcolor = cgrad(lscene.scene.colormap[])[1])
        plt = volume!(lscene.scene, X, X, X, lf(vol); algorithm = algorithm, isovalue = -isovalue, isorange = isorange, colorrange = (isovalue - isorange, isovalue + isorange), show_axis=false, kwargs...)
        plt = volume!(lscene.scene, X, X, X, lf(vol); algorithm = algorithm, isovalue = isovalue, isorange = isorange, colorrange = (isovalue - isorange, isovalue + isorange), show_axis=false, kwargs...)
        push!(lscenes, lscene)
        push!(plts, plt)
    end

    algorithm == :mip && (fig.scene.backgroundcolor = cgrad(lscenes[1].scene.colormap[])[1])

    linkCameras!([lscene.scene for lscene in lscenes]...)
    for plt in plts
        cameracontrols(plt).rotationspeed[] = 0.02
    end
    cameracontrols(plts[1]).fov[] *= 0.7
    display(fig)
    fig
end


function toVolume(f; radius = 1.0, gridres = 10, colorscale = identity, kwargs...)
    X = collect(range(-radius, radius, length = gridres))
    grid = collect.(collect(Iterators.product(X, X, X)))
    V = colorscale.(f.(grid))
    V ./= maximum(abs, V)
    X, V
end

function toGridVolume(f; kwargs...)
    X, V = toVolume(f; kwargs...)
    X, X, X, V
end

function volshow!(scene, f; kwargs...)
    X, V = toVolume(f; kwargs...)
    volshow!(scene, X, V; kwargs...)
end

function volshow(f; kwargs...)
    X, V = toVolume(f; kwargs...)
    volshow(X, V; kwargs...)
end

function volshow(X, V; algorithm = :iso, kwargs...)
    scene = Scene(;kwargs...)
    volshow!(scene, X, V; algorithm = algorithm, kwargs...)
end

function volshow!(scene, X, V; algorithm = :iso, isovalue = 0.1, isorange = 0.05, kwargs...)
    if algorithm == :mip
        scene.backgroundcolor = cgrad(scene.colormap[])[1]
    end
    volume!(scene, X, X, X, V; algorithm = algorithm, isorange = isorange, isovalue = isovalue, colorrange = (isovalue - isorange, isovalue + isorange), kwargs...)
    cameracontrols(scene).rotationspeed[] = 0.02

    return scene
end

# function volCompare(volumes...; ncols = 0, kwargs...)
#     scenes = [volshow(volume; kwargs...) for volume in volumes]
#     linkCameras!(scenes...)
#     scene = plotGrid(scenes...;  ncols = ncols, kwargs...)
#     scene
# end

function linkCameras!(scenes...)
    for scene in scenes
        scene.camera = scenes[1].camera
    end
end

function plotGrid!(parent, scenes...; ncols = 0, kwargs...)
    if ncols == 0
        ncols = ceil(Int, sqrt(length(scenes)))
    end
    nrows = ceil(Int, length(scenes) / ncols)

    grid = [[]]
    i = 1
    while i <= length(scenes)
        if length(grid[end]) == ncols
            push!(grid, [])
        end
        push!(grid[end], scenes[i])
        # scenes[i].camera = grid[1][1].camera
        i += 1
    end
    
    rows = [vbox(s...) for s in grid]

    # parent = Scene(;kwargs...)
    # parent.resolution = (parent.resolution[][1], parent.resolution[][1] / ncols * nrows)

    scene = hbox(reverse(rows)...; parent = parent)
end

function plotGrid(scenes...; ncols = 0, kwargs...)
    parent = Scene(;kwargs...)
    parent.resolution = (parent.resolution[][1], parent.resolution[][1] / ncols * nrows)

    plotGrid!(parent, scenes...; ncols = 0, kwargs...)
end

function pointPlot3d(points; ms = 0.1, color = :black, d = 1, res::Int = 800)
    return pointPlot3d(points, (res, res), ms = ms, color = color, d = d)
end

function pointPlot3d(points, res; ms = 0.1, color = :black, d = 1)
    scene = Scene(resolution=res, limits=FRect3D((-d,-d,-d),(2d,2d,2d)), show_axis=true)
    scatter!(scene, Point3f0.(points), markersize = ms, color=color)
    cameracontrols(scene).rotationspeed[] = 0.01
    display(scene)
    return scene
end

function pointPlot3d!(scene, points; ms = 0.1, color = :black)
    scatter!(scene, Point3f0.(points), markersize = ms, color=color)
    display(scene)
    return scene
end