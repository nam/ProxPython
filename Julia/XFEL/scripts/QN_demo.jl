#############################
#  import packages and setup
#############################
using Revise

using LinearAlgebra, Statistics, Rotations, ProgressMeter, Random, JLD2, BenchmarkTools, StaticArrays, Dates, FileIO, Colors, Distributions, Dates, StatsBase, SpecialFunctions, StatsFuns, Glob, IterTools, Suppressor, ColorSchemes, Accessors, Interpolations

# Professional timing:
# using TimerOutputs
# const to = TimerOutput()
# Just-in timing:
using TickTock

using CUDA
CUDA.allowscalar(false)
import NPhotons as nph

# put the following into some utilities file...
function to_real_vect(b)
    if b === nothing
            return nothing
        end
        collect(Iterators.flatten(Iterators.flatten(b)))
end
        
function to_static_vect(b)
    b = transpose(reshape(b,3,:))
        [SVector{3,Float64}(l...) for l in eachrow(b)]
end

#############################
#  synthetic object
#############################

qmax = 3
o = nph.AtomVolume(nph.spiral(10, m = 1.5, r = 3, h = 10), 1.0)
o = nph.normalizeScattering(o, nphotons = 15, qmax = qmax);

#####################################################
#  generate images setup log-likelihood and gradient
#####################################################
images = nph.generateImages(10000, 1, o)# , maxr = qmax)

params = nph.integrationParameters(qstep = 0.1, qmax = qmax, npercircle = 32)
ev = nph.setupEval(params.rotations, params.pixels);
logp = nph.logpClosure(images, params.rotations, params.pixels, params.pixelWeights, params.npercircle, params.rotationWeights, 1, ncalls = 1)
grad = nph.gradientClosure(images, params.rotations, params.pixels, params.pixelWeights, params.npercircle, params.rotationWeights, 1, ndiffs = 3length(o), ncalls = 1);

f = deepcopy(o);
f.positions .= 5 .* randn.(SVector{3, Float64});
f.positions ./= max.(1, norm.(f.positions) ./ 5)
fs = [f]
oe = -logp(o)
ps = Float64[-logp(f)];
gs = Float64[];
# enode=ps;
# gnode = gs;

# initialize from the same initial state as the previous cell:
f_old=deepcopy(f);


# preallocate monitoring arrays:
OUTERMAXIT = 1;
INNERMAXIT = 100;
enode=Vector{Float64}(undef, INNERMAXIT+1);
gnode=Vector{Float64}(undef, INNERMAXIT+1);

fpositions = to_real_vect(f.positions);
fpositions_old = fpositions;
fpositions_sq = fpositions.^2;
fergodic = fpositions;
fergodic_old = deepcopy(fergodic);
fpositions_sq_ergodic = fpositions_sq;
fpositions_sq_ergodic_old = deepcopy(fpositions_sq_ergodic);
f_var = fpositions_sq_ergodic - fergodic.^2;
f_var_old = deepcopy(f_var);
nf_ergodic_diff = Vector{Float64}(undef, OUTERMAXIT+1);
nf_ergodic_diff[1] = norm(fpositions);
nf_var_diff = Vector{Float64}(undef, OUTERMAXIT+1);
nf_var_diff[1] = norm(f_var);
gbarnode=Vector{Float64}(undef, OUTERMAXIT+1);
####################
##  Main iteration:
####################
stepsize = 1e-1
samplesize=10000
# the more samples -> the smaller the step needs to be.  
# For 1000-2000 images stepsize ~ 1e-4  
# For 5000-10000 images stepsize ~ 1e-5

################################
# Import SAMSARA
################################
using PyCall
pushfirst!(PyVector(pyimport("sys")."path"), "")
samsara = pyimport("samsara")

function main()
    ################################
    # Initialize SAMSARA
    ################################
    # have to install the options and other samsara stuff
    ngrad_TOL = 6e-6   # eps**(1/3)
    step_TOL = 4e-11   # eps**(2/3)
    Maxit = 50
    options = Dict()
    options[:verbose] = true
    options[:alpha_0] = 5e-12
    options[:gamma] = .5
    options[:c] = .01
    options[:beta] = .9999999
    options[:eta1] = .995
    options[:eta2] = .8
    options[:eta3] =  0.000001 # .025
    options[:maxmem] = 8
    options[:tr] = 1e+15
    options[:ngrad_TOL] = ngrad_TOL
    options[:step_TOL] = step_TOL
    options[:Maxit] = Maxit
    options[:QN_method] = samsara.BFGS1_product
    options[:Step_finder] = samsara.Explicit_TR
    options[:History] = samsara.cdSY_mat
    options[:update_conditions] = "Trust Region"
    options[:initial_step] = 1e+7*options[:step_TOL]

    s = samsara.Samsara(;options...)

    fpositions = to_real_vect(f.positions);
    fpositions_sq = fpositions.^2;
    fergodic = fpositions;
    fergodic_old = deepcopy(fergodic);
    fpositions_sq_ergodic = fpositions_sq;
    fpositions_sq_ergodic_old = deepcopy(fpositions_sq_ergodic);
    f_var = fpositions_sq_ergodic - fergodic.^2;
    f_var_old = deepcopy(f_var);
    nf_ergodic_diff = Vector{Float64}(undef, OUTERMAXIT+1);
    nf_ergodic_diff[1] = norm(fpositions);
    nf_var_diff = Vector{Float64}(undef, OUTERMAXIT+1);
    nf_var_diff[1] = norm(f_var);
    # gbarnode=Vector{Float64}(undef, OUTERMAXIT+1);
    positions_new = fpositions # nothing
    E_old = -logp(f_old)
    E_new = 1000 # -logp(f_old) # nothing
    f_new = deepcopy(f_old) # nothing
    # fergodic_old = fergodic;
    X = ev(f_old)
    grad = nph.gradientClosure(sample(images, samplesize, replace = false), params.rotations, params.pixels, params.pixelWeights, params.npercircle, params.rotationWeights, 1, ndiffs = 3length(o), ncalls = 1);
    dX = CUDA.zeros(size(X)..., 3length(o))
    for h in 1:length(o)
        gr = nph.gradient.(cu(f), h, ev.cupoints)
        for i in 1:3
            dX[:,:,3(h-1)+i] .= getindex.(gr, i)
        end
    end
    g_old = SVector{3, Float64}.(eachcol(reshape(grad(X, dX), 3, :)))
    g_old .= -1g_old
    g_new =  nothing
    stepsize = 999.
    ngradf_new = 999.
    # end initialize samsara
    # oe = -logp(o)
    # fnode = Observable(f_old)
    # enode = Observable(ps)

    for n in 1:OUTERMAXIT
        X = ev(f)
        grad = nph.gradientClosure(sample(images, samplesize, replace = false), params.rotations, params.pixels, params.pixelWeights, params.npercircle, params.rotationWeights, 1, ndiffs = 3length(o), ncalls = 1);
        dX = CUDA.zeros(size(X)..., 3length(o))

        for t in 1:INNERMAXIT
            #  if we were in python, and samsara has been imported properly, the call to samsara in this context would look like:
            __a, __b, E_old, __c, stepsize = s.run(to_real_vect(f_old.positions), to_real_vect(positions_new), E_old, E_new, to_real_vect(g_old), to_real_vect(g_new))
            positions_new = to_static_vect(__a)
            f_old.positions .= to_static_vect(__b)
            g_old = to_static_vect(__c)
            # when positions_new and g_new are not None, then this overwrites f_old.positions on the left with 
            # positions_new from the right and overwrites g_old on the left with g_new from the right
            # and overwrites E_old on the left with N_new from the right...so none of these variables need to be updated
            # we just need to generate the new E_new and g_new and two replace .positions in f_new with positions_new
        
            # construct f_new: only the positions change, I think
            f_new=deepcopy(f_old)
            # the next line is retained from Steffenś code above, but Iḿ not sure about this:
            # positions_new .-= Ref(mean(positions_new))
            f_new.positions.=positions_new        
            # f = @set f.positions = f.positions .+ (stepsize/samplesize) .* g
            # f.positions .-= Ref(mean(f.positions))

            # the next line evaluates the Energy at the new proposed positions
            E_new = -logp(f_new)
            print(E_old)
            print(" ")
            print(E_new)
            print(" ")
            push!(fs, deepcopy(f_new))
        
            # Now compute the gradient at the new positions        
            X = ev(f_new)
            for h in 1:length(o)
                gr = nph.gradient.(cu(f_new), h, ev.cupoints)
                for i in 1:3
                    dX[:,:,3(h-1)+i] .= getindex.(gr, i)
                end
            end
            g_new = SVector{3, Float64}.(eachcol(reshape(grad(X, dX), 3, :)))
            g_new .= -1g_new
            ngradf_new = norm(g_new)
            push!(ps, ngradf_new)
            # enode[t] = -logp(f_new);
            # gnode[t] = ngradf_new;
            # Makie.notify(gnode); Makie.notify(enode);
            # ispressed(fig, Keyboard.escape) && break
        end
        fpositions = to_real_vect(f.positions);
        fpositions_sq = fpositions.^2;
        fergodic = 1/(n+1).*(fpositions+ n.* fergodic_old);
        fpositions_sq_ergodic = 1/(n+1).*(fpositions_sq+ n.* fpositions_sq_ergodic_old);
        f_var = fpositions_sq_ergodic - fergodic.^2;
        nf_ergodic_diff[n+1] = norm(fergodic-fergodic_old);
        nf_var_diff[n+1] = norm(f_var - f_var_old);
        fergodic_old = fergodic;
        fpositions_sq_ergodic_old = fpositions_sq_ergodic;
        f_var_old = f_var;        
    end
    # Makie.notify(gnode); Makie.notify(enode);
    # ispressed(fig, Keyboard.escape) && break
end
tick()
main()
t=tok()
# Stuff the timing into nf_var_diff
nf_var_diff[1]=t;
# fig

#################################
#  data I/O
################################
using HDF5

h5write("data/step_diff_1x10000x10000_truestart.h5", "gnode", gnode[1:INNERMAXIT])
h5write("data/f_deterministic_truestart.h5", "fergodic", f)

h5write("data/nf_ergodic_diff_5000x10x5000_rand.h5", "nf_ergodic_diff", nf_ergodic_diff)
h5write("data/nf_var_diff_5000x10x5000_rand.h5", "nf_var_diff", nf_var_diff);
h5write("data/fergodic_5000x10x5000_rand.h5", "fergodic", fergodic)

fpositions = h5read("data/fergodic_5000x10x100_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f100=f;
f100 = @set f100.positions = fpositions;
fpositions = h5read("data/fergodic_5000x10x500_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f500=f;
f500 = @set f500.positions = fpositions;
fpositions = h5read("data/fergodic_5000x10x1000_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f1000=f;
f1000 = @set f1000.positions = fpositions;
fpositions = h5read("data/fergodic_5000x10x5000_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f5000=f;
f5000 = @set f5000.positions = fpositions;


fpositions = h5read("data/fergodic_1x10000x10000_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f10000=f;
f10000 = @set f10000.positions = fpositions;


nf_ergodic_diff100 = h5read("data/nf_ergodic_diff_5000x10x100_rand.h5", "nf_ergodic_diff");
nf_var_diff100 = h5read("data/nf_var_diff_5000x10x100_rand.h5", "nf_var_diff");
nf_ergodic_diff500 = h5read("data/nf_ergodic_diff_5000x10x500_rand.h5", "nf_ergodic_diff");
nf_var_diff500 = h5read("data/nf_var_diff_5000x10x500_rand.h5", "nf_var_diff");
nf_ergodic_diff1000 = h5read("data/nf_ergodic_diff_5000x10x1000_rand.h5", "nf_ergodic_diff");
nf_var_diff1000 = h5read("data/nf_var_diff_5000x10x1000_rand.h5", "nf_var_diff");
nf_ergodic_diff5000 = h5read("data/nf_ergodic_diff_5000x10x5000_rand.h5", "nf_ergodic_diff");
nf_var_diff5000 = h5read("data/nf_var_diff_5000x10x5000_rand.h5", "nf_var_diff");

gnode = h5read("data/gnode_determ.h5", "gnode");
nf_var_diff = h5read("data/nf_var_diff_1x10000x10000_rand.h5", "nf_var_diff");

###################
#  Graphics
###################
using GLMakie, CairoMakie
gl(args...; kwargs...) = GLMakie.activate!(args...; kwargs...);
cairo(args...; kwargs...) = CairoMakie.activate!(args...; kwargs...);
set_window_config!(float=false);
ProgressMeter.ijulia_behavior(:clear)

theme = Theme(Figure = (resolution = (1000,1000),), Volume = (algorithm = :iso, isovalue = 1.0, isorange = 0.9, colorrange = (0, 2)))
set_theme!(theme)

import REPL
REPL.REPLCompletions.latex_symbols["\\fig"] = "fig = Figure(); ax = Axis(fig[1,1])";
REPL.REPLCompletions.latex_symbols["\\angstrom"] = "Å";


# includet("utils.jl")
# need to put in an absolute path to utils*.jl
includet("utils2.jl")


###########################
# everything in one window
###########################
gl(); 
fig = Figure(resolution = (1600, 2000), fontsize=32)


ax3 = Axis(fig[2,1], yscale = log10,
    ylabel = L"||x^{k+1}-x^k||",
    xlabel = "Iteration")
lines!(ax3, gnode[1:INNERMAXIT],linewidth=2)
# on(s -> autolimits!(ax3), gnode[1:INNERMAXIT]);

ls = LScene(fig[1,1][1,2], show_axis=false, height = 1300, width = 1300, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f, algorithm = :iso)

ls = LScene(fig[1,1][1,1], show_axis=false, height = 1300, width = 1300, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, o, algorithm = :iso, colormap = :ice)

linkCameras!(contents(fig[1, 1][:,:]))
rowsize!(fig.layout, 1, 800)
save("deterministic_composite.png", fig)


#############################
# Just the recovered density
#############################

gl(); 
fig = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig[1,1], show_axis=false, height = 3000, width = 3000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f, algorithm = :iso, colormap = :ice)

# linkCameras!(contents(fig[1, 1][:,:]))
# rowsize!(fig.layout, 1, 1600)
# window(fig)
save("initialization_rand.png", fig)

########################################
# Recovered density comparison
#######################################

gl(); 
fig100 = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig100[1,1], show_axis=false, height = 2000, width = 2000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f100, algorithm = :iso, colormap = :ice)
save("fergodic_5000x10x100_rand.png", fig100)

gl(); 
fig500 = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig500[1,1], show_axis=false, height = 2000, width = 2000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f500, algorithm = :iso, colormap = :ice)
save("fergodic_5000x10x500_rand.png", fig500)

gl(); 
fig1000 = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig1000[1,1], show_axis=false, height = 2000, width = 2000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f1000, algorithm = :iso, colormap = :ice)
save("fergodic_5000x10x1000_rand.png", fig1000)

gl(); 
fig5000 = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig5000[1,1], show_axis=false, height = 2000, width = 2000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f5000, algorithm = :iso, colormap = :ice)
save("fergodic_5000x10x5000_rand.png", fig5000)

ls = LScene(fig[1,1][2,1], show_axis=false, height = 3000, width = 3000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f1000, algorithm = :iso, colormap = :ice)

ls = LScene(fig[1,1][2,2], show_axis=false, height = 1300, width = 1300, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f5000, algorithm = :iso, colormap = :ice)

linkCameras!(contents(fig[1, 1][:,:]))
rowsize!(fig.layout, 1, 1600)



######################################
# Just the gradient-norm-to iteration
######################################
gl(); 
fig3 = Figure(resolution=(800, 1000), fontsize=32)

ax3 = Axis(fig3[1,1], yscale = log10, 
    ylabel = L"||x^{k+1}-x^k||", 
    xlabel = "Iteration")
lines!(ax3, gnode[1:10000], linewidth=4)
# on(s -> autolimits!(ax3), gnode[1:INNERMAXIT]);

fig3
save("deterministic_gradient.png", fig3)

#######################################################
# Ergodic and variance difference iteration comparison
#######################################################
gl(); 
fig2 = Figure(resolution=(800, 1000), fontsize=32)

ax2 = Axis(fig2[1,1], yscale = log10, 
    ylabel = L"||x_{Ces}^{k+1}-x_{Ces}^k||", 
    xlabel = "Iteration")
lines!(ax2, nf_ergodic_diff100, linewidth=3, label = "m=100")
lines!(ax2, nf_ergodic_diff500, linewidth=3, label = "m=500")
lines!(ax2, nf_ergodic_diff1000, linewidth=3, label = "m=1000")
lines!(ax2, nf_ergodic_diff5000, linewidth=3, label = "m=5000")
axislegend(ax2)
# on(s -> autolimits!(ax2), nf_ergodic_diff);

fig2
save("nf_ergodic_diff_5000x10xXXXX.png", fig2)

fig3 = Figure(resolution=(800, 1000), fontsize=32)

ax3 = Axis(fig3[1,1], yscale = log10, 
    ylabel = L"||var_x^{k+1}-var_x^k||", 
    xlabel = "Iteration")
lines!(ax3, nf_var_diff100, linewidth=3, label = "m=100")
lines!(ax3, nf_var_diff500, linewidth=3, label = "m=500")
lines!(ax3, nf_var_diff1000, linewidth=3, label = "m=1000")
lines!(ax3, nf_var_diff5000, linewidth=3, label = "m=5000")
axislegend(ax3)
# on(s -> autolimits!(ax3), nf_var_diff);

fig3
save("nf_var_diff_5000x10xXXXX.png", fig3)

######################################
# Just the ergodic difference iteration
######################################
gl(); 
fig3 = Figure(resolution=(800, 1000), fontsize=32)

ax3 = Axis(fig3[1,1], yscale = log10, 
    ylabel = L"||x^{k+1}-x^k||", 
    xlabel = "Iteration")
lines!(ax3, nf_ergodic_diff)
on(s -> autolimits!(ax3), nf_ergodic_diff);

fig3
save("nf_ergodic_diff_1000x10x100.png", fig3)


