#############################
#  import packages and setup
#############################
# using Revise

using LinearAlgebra, Random
################################
# Import SAMSARA
################################
using PyCall
pushfirst!(PyVector(pyimport("sys")."path"), "")
samsara = pyimport("samsara")

# Professional timing:
# using TimerOutputs
# const to = TimerOutput()
# Just-in timing:
using TickTock

# put the following into some utilities file...

#############################
#  Rosenbrock's function
#############################

function Objective!(storage, x)
    storage[1] = 100*(x[2]-x[1]^2)^2 + (1-x[1])^2 # f
    storage[2] = -400*(x[2]-x[1]^2)*x[1] - 2*(1-x[1]) # Df[1]
    storage[3] = 200*(x[2]-x[1]^2) # Df[2]
end

################################
# Initialize SAMSARA
################################
# have to install the options and other samsara stuff
ngrad_TOL = 6e-6   # eps**(1/3)
step_TOL = 4e-11   # eps**(2/3)
Maxit = 500
options = Dict()
options[:verbose] = true
options[:alpha_0] = 5e-12
options[:gamma] = .5
options[:c] = .01
options[:beta] = .9999999
options[:eta1] = .995
options[:eta2] = .8
options[:eta3] =  0.25 # .025
options[:maxmem] = 8
options[:tr] = 1e+15
options[:ngrad_TOL] = ngrad_TOL
options[:step_TOL] = step_TOL
options[:Maxit] = Maxit
options[:QN_method] = samsara.BFGS1_product
options[:Step_finder] = samsara.Explicit_TR
options[:History] = samsara.cdSY_mat
options[:update_conditions] = "Trust Region"
options[:initial_step] = 1e+7*options[:step_TOL]

s = samsara.Samsara(;options...)

x_old = 5 .* randn(2);
x_old ./= max.(1, norm.(x_old) ./ 5)

# initialize from the same initial state as the previous cell:
storage = zeros(3)
Objective!(storage, x_old)
f_old=deepcopy(storage[1])
g_old=deepcopy(storage[2:3])

x_new = nothing # x_old 
f_new = nothing # deepcopy(f_old) # nothing
g_new = nothing # deepcopy(g_old) # nothing
stepsize = 999.
ngradf_new = 999.
it=1
# end initialize samsara

####################
##  Main iteration:
####################
tick()
while it < Maxit && ngradf_new > ngrad_TOL && stepsize > step_TOL
    #  if we were in python, and samsara has been imported properly, the call to samsara in this context would look like:
    x_new, x_old, f_old, g_old, stepsize = s.run(x_old, x_new, f_old, f_new, g_old, g_new)
    # when positions_new and g_new are not None, then this overwrites f_old.positions on the left with 
    # positions_new from the right and overwrites g_old on the left with g_new from the right
    # and overwrites E_old on the left with N_new from the right...so none of these variables need to be updated
    # we just need to generate the new E_new and g_new and two replace .positions in f_new with positions_new
        
    # construct f_new and g_new: 
    Objective!(storage,x_new)
    f_new=deepcopy(storage[1])
    g_new=deepcopy(storage[2:3])
    ngradf_new = norm(g_new)
    it +=1
end
t=tok()
###################
#  Graphics
###################
using GLMakie, CairoMakie
gl(args...; kwargs...) = GLMakie.activate!(args...; kwargs...);
cairo(args...; kwargs...) = CairoMakie.activate!(args...; kwargs...);
set_window_config!(float=false);
ProgressMeter.ijulia_behavior(:clear)

theme = Theme(Figure = (resolution = (1000,1000),), Volume = (algorithm = :iso, isovalue = 1.0, isorange = 0.9, colorrange = (0, 2)))
set_theme!(theme)

import REPL
REPL.REPLCompletions.latex_symbols["\\fig"] = "fig = Figure(); ax = Axis(fig[1,1])";
REPL.REPLCompletions.latex_symbols["\\angstrom"] = "Å";


# includet("utils.jl")
# need to put in an absolute path to utils*.jl
includet("utils2.jl")


###########################
# everything in one window
###########################
gl(); 
fig = Figure(resolution = (1600, 2000), fontsize=32)


ax3 = Axis(fig[2,1], yscale = log10,
    ylabel = L"||x^{k+1}-x^k||",
    xlabel = "Iteration")
lines!(ax3, gnode[1:INNERMAXIT],linewidth=2)
# on(s -> autolimits!(ax3), gnode[1:INNERMAXIT]);

ls = LScene(fig[1,1][1,2], show_axis=false, height = 1300, width = 1300, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f, algorithm = :iso)

ls = LScene(fig[1,1][1,1], show_axis=false, height = 1300, width = 1300, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, o, algorithm = :iso, colormap = :ice)

linkCameras!(contents(fig[1, 1][:,:]))
rowsize!(fig.layout, 1, 800)
save("deterministic_composite.png", fig)


######################################
# Just the gradient-norm-to iteration
######################################
gl(); 
fig3 = Figure(resolution=(800, 1000), fontsize=32)

ax3 = Axis(fig3[1,1], yscale = log10, 
    ylabel = L"||x^{k+1}-x^k||", 
    xlabel = "Iteration")
lines!(ax3, gnode[1:10000], linewidth=4)
# on(s -> autolimits!(ax3), gnode[1:INNERMAXIT]);

fig3
save("deterministic_gradient.png", fig3)

