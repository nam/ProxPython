#############################
#  import packages and setup
#############################
using Revise

using LinearAlgebra, Statistics, Rotations, ProgressMeter, Random, JLD2, BenchmarkTools, StaticArrays, Dates, FileIO, Colors, Distributions, Dates, StatsBase, SpecialFunctions, StatsFuns, Glob, IterTools, Suppressor, ColorSchemes, Accessors, Interpolations

# Professional timing:
# using TimerOutputs
# const to = TimerOutput()
# Just-in timing:
using TickTock

using CUDA
CUDA.allowscalar(false)
import NPhotons as nph

# put the following into some utilities file...
function to_real_vect(b)
    if b === nothing
            return nothing
    end
    collect(Iterators.flatten(Iterators.flatten(b)))
end
        
function to_static_vect(b)
    b = transpose(reshape(b,3,:))
        [SVector{3,Float64}(l...) for l in eachrow(b)]
end

#############################
#  synthetic object
#############################

qmax = 3
# o = nph.AtomVolume(nph.spiral(10, m = 1.5, r = 3, h = 10), 1.0)
# o = nph.normalizeScattering(o, nphotons = 15, qmax = qmax);
o = nph.smoothen(nph.loadPDB("1EJG"), 2.0)
# f = deepcopy(o);
# f.positions .= 5 .* randn.(SVector{3, Float64});
# f.positions ./= max.(1, norm.(f.positions) ./ 5)
f = nph.randomAtomVolume(10, 2.5, 10)

#####################################################
#  generate images setup log-likelihood and gradient
#####################################################
images = nph.generateImages(10000, 1, o, maxr = qmax)

params = nph.integrationParameters(qstep = 0.1, qmax = qmax, npercircle = 32)
ev = nph.setupEval(params.rotations, params.pixels);
logp = nph.logpClosure(images, params.rotations, params.pixels, params.pixelWeights, params.npercircle, params.rotationWeights, 1, ncalls = 1)
grad = nph.gradientClosure(images, params.rotations, params.pixels, params.pixelWeights, params.npercircle, params.rotationWeights, 1, ndiffs = 3length(f), ncalls = 1);

fs = [f]
oe = -logp(o)
ps = Float64[-logp(f)];
gs = Float64[];
# enode=ps;
# gnode = gs;

# preallocate monitoring arrays:
OUTERMAXIT = 500;
INNERMAXIT = 10;
INNER_TOL = 2e-3;
enode=Vector{Float64}(undef, INNERMAXIT+1);
gnode=Vector{Float64}(undef, INNERMAXIT+1);

fpositions = to_real_vect(f.positions);
fpositions_old = fpositions;
fpositions_sq = fpositions.^2;
fergodic = fpositions;
fergodic_old = fergodic;
fpositions_sq_ergodic = fpositions_sq;
fpositions_sq_ergodic_old = fpositions_sq_ergodic;
f_var = fpositions_sq_ergodic - fergodic.^2;
f_var_old = f_var;
nf_ergodic_diff = Vector{Float64}(undef, OUTERMAXIT+1);
nf_ergodic_diff[1] = norm(fpositions);
nf_var_diff = Vector{Float64}(undef, OUTERMAXIT+1);
nf_var_diff[1] = norm(f_var);
gbarnode=Vector{Float64}(undef, OUTERMAXIT+1);
####################
##  Main iteration:
####################
stepsize = 1e-1
samplesize=200
# the more samples -> the smaller the step needs to be.  
# For 1000-2000 images stepsize ~ 1e-4  
# For 5000-10000 images stepsize ~ 1e-5

tick()
for n in 1:OUTERMAXIT
    X = ev(f)
    global grad = nph.gradientClosure(sample(images, samplesize, replace = false), params.rotations, params.pixels, params.pixelWeights, params.npercircle, params.rotationWeights, 1, ndiffs = 3length(f), ncalls = 1);
    dX = CUDA.zeros(size(X)..., 3length(o))
    t=1;
    residual=999;
    while t < INNERMAXIT && residual > INNER_TOL
    # for t in 1:INNERMAXIT
        X = ev(f)
        for h in 1:length(o)
            gr = nph.gradient.(cu(f), h, ev.cupoints)
            for i in 1:3
                dX[:,:,3(h-1)+i] .= getindex.(gr, i)
            end
        end
        g = SVector{3, Float64}.(eachcol(reshape(grad(X, dX), 3, :)))
        
        global f = @set f.positions = f.positions .+ (stepsize/samplesize) .* g
        # The following just corrects for a linear shift in the object.
        f.positions .-= Ref(mean(f.positions))

        # fnode[] = f
        push!(fs, f)
        # append!(enode[], -logp(f))
        # append!(gnode[], (stepsize/samplesize)*norm(g))
        global enode[t] = -logp(f);
        residual = (stepsize/samplesize)*norm(g)
        global gnode[t] = residual;
        # Makie.notify(gnode); Makie.notify(enode);
        # ispressed(fig, Keyboard.escape) && break
        t +=1
    end
    global t_end=t-1
    global fpositions = to_real_vect(f.positions);
    global fpositions_sq = fpositions.^2;
    global fergodic = 1/(n+1).*(fpositions+ n.* fergodic_old);
    global fpositions_sq_ergodic = 1/(n+1).*(fpositions_sq+ n.* fpositions_sq_ergodic_old);
    global f_var = fpositions_sq_ergodic - fergodic.^2;
    global nf_ergodic_diff[n+1] = norm(fergodic-fergodic_old);
    global nf_var_diff[n+1] = norm(f_var - f_var_old);
    global fergodic_old = fergodic;
    global fpositions_sq_ergodic_old = fpositions_sq_ergodic;
    global f_var_old = f_var;
    print(string(string(n), " of ", string(OUTERMAXIT), ": ", string(t_end), " inner iterations \n"))   

end
t=tok()
# Stuff the timing into nf_var_diff
nf_var_diff[1]=t;
# fig

#################################
#  data I/O
################################
using HDF5

filename = string("./data/step_diff_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".h5");
h5open(filename, "w") do file
    # Write data to the file
    file[filename] = gnode
end
# h5write(filename, "gnode", gnode)
# filename = string("../data/f_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".h5");
# h5write(filename, "fergodic", f)

filename = string("./data/nf_ergodic_diff_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".h5");
h5write(filename, "nf_ergodic_diff", nf_ergodic_diff);
filename = string("./data/nf_var_diff_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".h5");
h5write(filename, "nf_var_diff", nf_var_diff);

#= filename = string("../data/nf_ergodic_diff_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".h5");
nf_ergodic_diff500 = h5read(filename, "nf_ergodic_diff");
filename = string("../data/step_diff_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".h5");
gnode500 = h5read(filename, "gnode");


# fixed intstances below:
h5write("data/step_diff_1x10000x10000_truestart.h5", "gnode", gnode[1:INNERMAXIT])
h5write("data/f_deterministic_truestart.h5", "fergodic", f)

h5write("data/nf_ergodic_diff_5000x10x5000_rand.h5", "nf_ergodic_diff", nf_ergodic_diff)
h5write("data/nf_var_diff_5000x10x5000_rand.h5", "nf_var_diff", nf_var_diff);
h5write("data/fergodic_5000x10x5000_rand.h5", "fergodic", fergodic)

fpositions = h5read("data/fergodic_5000x10x100_rand.h5", "fergodic");
fpositions = h5read("data/fergodic_5000x10x100_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f100=f;
f100 = @set f100.positions = fpositions;
fpositions = h5read("data/fergodic_5000x10x500_rand.h5", "fergodic");
fpositions = h5read("data/fergodic_5000x10x500_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f500=f;
f500 = @set f500.positions = fpositions;
fpositions = h5read("data/fergodic_5000x10x1000_rand.h5", "fergodic");
fpositions = h5read("data/fergodic_5000x10x1000_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f1000=f;
f1000 = @set f1000.positions = fpositions;
fpositions = h5read("data/fergodic_5000x10x5000_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f5000=f;
f5000 = @set f1000.positions = fpositions;

fpositions = h5read("data/fergodic_5000x10x5000_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f5000=f;
f5000 = @set f5000.positions = fpositions;


fpositions = h5read("data/fergodic_1x10000x10000_rand.h5", "fergodic");
fpositions = to_static_vect(fpositions);
f10000=f;
f10000 = @set f10000.positions = fpositions;

nf_ergodic_diff100 = h5read("data/nf_ergodic_diff_5000x10x100_rand.h5", "nf_ergodic_diff");
nf_var_diff100 = h5read("data/nf_var_diff_5000x10x100_rand.h5", "nf_var_diff");
nf_ergodic_diff500 = h5read("data/nf_ergodic_diff_5000x10x500_rand.h5", "nf_ergodic_diff");
nf_var_diff500 = h5read("data/nf_var_diff_5000x10x500_rand.h5", "nf_var_diff");
nf_ergodic_diff1000 = h5read("data/nf_ergodic_diff_5000x10x1000_rand.h5", "nf_ergodic_diff");
nf_var_diff1000 = h5read("data/nf_var_diff_5000x10x1000_rand.h5", "nf_var_diff");
nf_ergodic_diff5000 = h5read("data/nf_ergodic_diff_5000x10x5000_rand.h5", "nf_ergodic_diff");
nf_var_diff5000 = h5read("data/nf_var_diff_5000x10x5000_rand.h5", "nf_var_diff");
gnode = h5read("data/gnode_determ.h5", "gnode");
nf_var_diff = h5read("data/nf_var_diff_1x10000x10000_rand.h5", "nf_var_diff");
=#
###################
#  Graphics
###################
using GLMakie, CairoMakie
gl(args...; kwargs...) = GLMakie.activate!(args...; kwargs...);
cairo(args...; kwargs...) = CairoMakie.activate!(args...; kwargs...);
set_window_config!(float=false);
ProgressMeter.ijulia_behavior(:clear)

theme = Theme(Figure = (resolution = (1000,1000),), Volume = (algorithm = :iso, isovalue = 1.0, isorange = 0.9, colorrange = (0, 2)))
set_theme!(theme)

import REPL
REPL.REPLCompletions.latex_symbols["\\fig"] = "fig = Figure(); ax = Axis(fig[1,1])";
REPL.REPLCompletions.latex_symbols["\\angstrom"] = "Å";


# includet("utils.jl")
# need to put in an absolute path to utils*.jl
includet("utils2.jl")


#############################
# Just the recovered density
#############################

gl(); 
fig = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig[1,1], show_axis=false, height = 3000, width = 3000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f, algorithm = :iso, colormap = :ice)
fig
# linkCameras!(contents(fig[1, 1][:,:]))
# rowsize!(fig.layout, 1, 1600)
# window(fig)
filename = string("./data/recoverd_density_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".png")
save(filename, fig)

######################################
# Just the gradient-norm-to iteration
######################################
gl(); 
fig3 = Figure(resolution=(800, 1000), fontsize=32)

ax3 = Axis(fig3[1,1], yscale = log10, 
    ylabel = L"||x^{k+1}-x^k||", 
    xlabel = "Iteration")
lines!(ax3, gnode[1:t_end], linewidth=4)
# on(s -> autolimits!(ax3), gnode[1:INNERMAXIT]);

fig3
filename = string("./data/deterministic_gradient_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".png")
save(filename, fig3)

###########################
# everything in one window
###########################
#=gl(); 
fig = Figure(resolution = (1600, 2000), fontsize=32)

ax3 = Axis(fig[2,1], yscale = log10,
    ylabel = L"||x^{k+1}-x^k||",
    xlabel = "Iteration")
lines!(ax3, gnode[1:t_end],linewidth=2)
# on(s -> autolimits!(ax3), gnode[1:INNERMAXIT]);

ls = LScene(fig[1,1][1,2], show_axis=false, height = 1300, width = 1300, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f, algorithm = :iso)

ls = LScene(fig[1,1][1,1], show_axis=false, height = 1300, width = 1300, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, o, algorithm = :iso, colormap = :ice)

# linkCameras!(contents(fig[1, 1][:,:]))
# rowsize!(fig.layout, 1, 800)
filename = string("./data/deterministic_composite_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".png")
save(filename, fig)
=#

######################################
# Just the ergodic difference iteration
######################################
gl(); 
fig3 = Figure(resolution=(800, 1000), fontsize=32)

ax3 = Axis(fig3[1,1], yscale = log10, 
    ylabel = L"||x^{k+1}-x^k||", 
    xlabel = "Iteration")
lines!(ax3, nf_ergodic_diff)
# on(s -> autolimits!(ax3), nf_ergodic_diff);

fig3
filename = string("./data/nf_ergodic_diff_", string(samplesize) , "x", string(OUTERMAXIT), "x", string(INNER_TOL), ".png")
save(filename, fig3)


########################################
# Recovered density comparison
#######################################
#=
gl(); 
fig100 = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig100[1,1], show_axis=false, height = 2000, width = 2000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f100, algorithm = :iso, colormap = :ice)
save("fergodic_5000x10x100_rand.png", fig100)
save("fergodic_5000x10x100_rand.png", fig100)

gl(); 
fig500 = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig500[1,1], show_axis=false, height = 2000, width = 2000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f500, algorithm = :iso, colormap = :ice)
save("fergodic_5000x10x500_rand.png", fig500)
save("fergodic_5000x10x500_rand.png", fig500)

gl(); 
fig1000 = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig1000[1,1], show_axis=false, height = 2000, width = 2000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f1000, algorithm = :iso, colormap = :ice)
save("fergodic_5000x10x1000_rand.png", fig1000)
save("fergodic_5000x10x1000_rand.png", fig1000)

gl(); 
fig5000 = Figure(resolution = (1600, 1600), fontsize=32)
ls = LScene(fig5000[1,1], show_axis=false, height = 2000, width = 2000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f5000, algorithm = :iso, colormap = :ice)
save("fergodic_5000x10x5000_rand.png", fig5000)
save("fergodic_5000x10x5000_rand.png", fig5000)

ls = LScene(fig[1,1][2,1], show_axis=false, height = 3000, width = 3000, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f1000, algorithm = :iso, colormap = :ice)

ls = LScene(fig[1,1][2,2], show_axis=false, height = 1300, width = 1300, tellheight = false, tellwidth = false);
Camera3D(ls.scene);
volume!(ls, f5000, algorithm = :iso, colormap = :ice)

linkCameras!(contents(fig[1, 1][:,:]))
rowsize!(fig.layout, 1, 1600)



#######################################################
# Ergodic and variance difference iteration comparison
#######################################################
gl(); 
fig2 = Figure(resolution=(800, 1000), fontsize=32)

ax2 = Axis(fig2[1,1], yscale = log10, 
    ylabel = L"||x_{Ces}^{k+1}-x_{Ces}^k||", 
    xlabel = "Iteration")
lines!(ax2, nf_ergodic_diff100, linewidth=3, label = "m=100")
lines!(ax2, nf_ergodic_diff500, linewidth=3, label = "m=500")
lines!(ax2, nf_ergodic_diff1000, linewidth=3, label = "m=1000")
lines!(ax2, nf_ergodic_diff5000, linewidth=3, label = "m=5000")
axislegend(ax2)
# on(s -> autolimits!(ax2), nf_ergodic_diff);

fig2
save("nf_ergodic_diff_5000x10xXXXX.png", fig2)
save("nf_ergodic_diff_5000x10xXXXX.png", fig2)

fig3 = Figure(resolution=(800, 1000), fontsize=32)

ax3 = Axis(fig3[1,1], yscale = log10, 
    ylabel = L"||var_x^{k+1}-var_x^k||", 
    xlabel = "Iteration")
lines!(ax3, nf_var_diff100, linewidth=3, label = "m=100")
lines!(ax3, nf_var_diff500, linewidth=3, label = "m=500")
lines!(ax3, nf_var_diff1000, linewidth=3, label = "m=1000")
lines!(ax3, nf_var_diff5000, linewidth=3, label = "m=5000")
axislegend(ax3)
# on(s -> autolimits!(ax3), nf_var_diff);

fig3
save("nf_var_diff_5000x10xXXXX.png", fig3)
=#
