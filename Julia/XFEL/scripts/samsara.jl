using PyCall
using LinearAlgebra

@pyimport samsara

function init_samsara()


	ngrad_TOL = 6e-6   # eps**(1/3)
	step_TOL = 4e-11   # eps**(2/3)
	Maxit = 500
	options = Dict()
	options[:verbose] = true
	options[:alpha_0] = 5e-12
	options[:gamma] = .5
	options[:c] = .01
	options[:beta] = .9999999
	options[:eta1] = .995
	options[:eta2] = .8
	options[:eta3] = .25
	options[:maxmem] = 8
	options[:tr] = 1e+15
	options[:ngrad_TOL] = ngrad_TOL
	options[:step_TOL] = step_TOL
	options[:Maxit] = Maxit
	options[:QN_method] = samsara.BFGS1_product
	options[:Step_finder] = samsara.Explicit_TR
	options[:History] = samsara.cdSY_mat
	options[:update_conditions] = "Trust Region"
	options[:initial_step] = 1e+5*options[:step_TOL]

	s = samsara.Samsara(;options...)
	return s
end

function rosenbrock(x)
	f = 100.0*(x[2]-x[1]^2)^2 + (1-x[1])^2
	Df = [0.0, 0.0]
	Df[1] = -400.0*(x[2]-x[1]^2)*x[1] - 2.0*(1-x[1])
 	Df[2] = 200.0*(x[2]-x[1]^2)
    	return f, Df
end

function run_samsara(s, f, Maxit, ngrad_TOL, step_TOL)
	xold = [.5, 2.0]

	fold, gradfold = f(xold)
	println(fold)
	println(gradfold)
	fnew = 100
	xnew = nothing
	gradfnew = nothing
	stepsize = 999.
	ngradfnew = 999.
	it = 0

	while it < Maxit && ngradfnew > ngrad_TOL && stepsize > step_TOL
		xnew, xold, fold, gradfold, stepsize = s.run(xold, xnew, fold, fnew, gradfold, gradfnew)
    		it += 1
     		fnew, gradfnew = rosenbrock(xnew)
   		ngradfnew = norm(gradfnew)
	end
end

s = init_samsara()
run_samsara(s, rosenbrock, 500, 6e-6, 4e-11)
